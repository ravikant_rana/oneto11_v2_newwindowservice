﻿using Microsoft.WindowsAzure.Storage.Blob;
using AzureStorage.BlobModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AzureStorage.Utilities
{
    public class BlobWrapper
    {
        public async Task<bool> UploadFileToBlob(FileDto file)
        {
            try
            {
                // Get Blob Container.
                CloudBlobContainer container = BlobUtilities.GetBlobClient.GetContainerReference(file.ContainerAccount);
                container.CreateIfNotExists();

                // Set Blob Container Permission.
                BlobContainerPermissions bcp = new BlobContainerPermissions();
                bcp.PublicAccess = BlobContainerPublicAccessType.Container;
                container.SetPermissions(bcp);

                // Get reference to blob (binary content).
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(file.FileName);

                // set its properties.
                blockBlob.Properties.ContentType = file.FileMime;
                blockBlob.Metadata["filename"] = file.FileName;
                blockBlob.Metadata["filemime"] = file.FileMime;

                // Get stream from file bytes.
                Stream stream = new MemoryStream(file.FileBytes);

                // Async upload of stream to Storage.
                AsyncCallback UploadCompleted = new AsyncCallback(OnUploadCompleted);
                blockBlob.BeginUploadFromStream(stream, UploadCompleted, blockBlob);
            }
            catch (Exception ex)
            {


            }
            return true;
        }

        private void OnUploadCompleted(IAsyncResult result)
        {
            CloudBlockBlob blob = (CloudBlockBlob)result.AsyncState;
            blob.SetMetadata();
            blob.EndUploadFromStream(result);
        }
        
        public async Task<bool> DeleteFileFromBlob(FileInfoDto file)
        {
            try
            {
                // Get Blob Container.
                CloudBlobContainer container = BlobUtilities.GetBlobClient.GetContainerReference(file.ContainerAccount);


                // Get reference to blob (binary content).
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(file.FileName);

                // Delete blob from Storage.                
                blockBlob.Delete();

            }
            catch (Exception ex)
            {


            }
            return true;
        }

        public async Task<byte[]> DownloadFileFromBlob(FileInfoDto file)
        {
            // Get Blob Container
            CloudBlobContainer container = BlobUtilities.GetBlobClient.GetContainerReference(file.ContainerAccount);

            // Get reference to blob (binary content)
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(file.FileName);

            // Read content
            using (MemoryStream ms = new MemoryStream())
            {
                blockBlob.DownloadToStream(ms);
                return ms.ToArray();
            }
        }
    }
}
