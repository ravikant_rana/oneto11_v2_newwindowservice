﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using AzureStorage.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureStorage.Utilities
{
    public class BlobUtilities
    {
        public static CloudBlobClient GetBlobClient
        {
            get
            {
                string cloudStorageAccountConnection = AppSettings.StorageAccountConnection;
                //"DefaultEndpointsProtocol=https;AccountName=oneadstorageacc;AccountKey=wuIiUwbk5/E6FkhLAPdULFhWBnriFBIICY/HapP5FxDKv2b/JtjmMbSMLqseTJMlukMlf7S2m/WuYvJIf4m6qg==;EndpointSuffix=core.windows.net"
                // "DefaultEndpointsProtocol=https;AccountName=[Storage Account Name];" +"AccountKey=[Storage Account Primary Key]"
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(cloudStorageAccountConnection);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                return blobClient;
            }
        }
    }
}
