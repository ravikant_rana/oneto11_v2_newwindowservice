﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureStorage.BlobModels
{
  public  class FileDto
    {
        public string FileName { get; set; }
        public byte[] FileBytes { get; set; }
        public string FileMime { get; set; }
        public string ContainerAccount { get; set; }
        public string FolderName { get; set; }
    }
}
