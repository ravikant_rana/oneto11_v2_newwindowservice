﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureStorage.BlobModels
{
    public class FileInfoDto
    {
        public string FileName { get; set; }
        public string ContainerAccount { get; set; }
    }
}
