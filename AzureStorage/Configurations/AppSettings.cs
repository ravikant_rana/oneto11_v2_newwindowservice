﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AzureStorage.Configurations
{
    public static class AppSettings
    {
        public static string StorageAccountConnection { get; set; }
    }
}
