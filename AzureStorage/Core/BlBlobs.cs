﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AzureStorage.Core
{
    public class BlBlobs
    {
        public CloudBlobContainer GetBLOBRef(string ConnectionString)
        {
            CloudStorageAccount storageac = CloudStorageAccount.Parse(ConnectionString);

            CloudBlobClient blobclient = storageac.CreateCloudBlobClient();

            CloudBlobContainer blobcontainer = blobclient.GetContainerReference("testctmstore");

            if (blobcontainer.CreateIfNotExists())
                blobcontainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });


            return blobcontainer;
        }
        public List<string> GetBlobList(string ConnectionString)
        {
            CloudBlobContainer blobcontainer = GetBLOBRef(ConnectionString);
            List<string> blobs = new List<string>();
            foreach (var item in blobcontainer.ListBlobs())
            {
                blobs.Add(item.Uri.ToString());
            }
            return blobs;
        }
        public void AddBlob(Stream InputStream, string FileName, int ContentLength, string ConnectionString)
        {
            if (ContentLength > 0)
            {
                try
                {
                    CloudBlobContainer blobcontainer = GetBLOBRef(ConnectionString);
                    CloudBlockBlob blob = blobcontainer.GetBlockBlobReference(FileName);
                    //blob.UploadFromStream(InputStream);

                    blob.UploadFromStream(InputStream, accessCondition: AccessCondition.GenerateIfNoneMatchCondition("*"));
                }
                catch (StorageException ex)
                {
                    if (ex.RequestInformation.HttpStatusCode == (int)System.Net.HttpStatusCode.Conflict)
                    {
                        // Handle duplicate blob condition
                    }
                    throw;
                }
            }
        }
        internal void DeleteBlob(string name, string ConnectionString)
        {
            Uri ur = new Uri(name);
            string fname = System.IO.Path.GetFileName(ur.LocalPath);
            CloudBlobContainer blobcontainer = GetBLOBRef(ConnectionString);
            CloudBlockBlob blob = blobcontainer.GetBlockBlobReference(fname);
            blob.Delete();
        }




        public void UploadBlobInBlocks(Stream InputStream, string name, long ContentLength, string ConnectionString)
        {
            CloudBlobClient blobClient;

            CloudStorageAccount storageac = CloudStorageAccount.Parse(ConnectionString);
            blobClient = storageac.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("testctmstore");
            container.CreateIfNotExists();

            var permission = container.GetPermissions();
            permission.PublicAccess = BlobContainerPublicAccessType.Container;
            container.SetPermissions(permission);

            CloudBlockBlob blob = container.GetBlockBlobReference(name);

            //blob.UploadFromStream(InputStream);

            int maxSize = 1 * 1024 * 1024; // 4 MB

            try
            {
                if (ContentLength > maxSize)
                {
                    byte[] data = ByteArrayFromStream(InputStream);
                    int id = 0;
                    int byteslength = data.Length;
                    int bytesread = 0;
                    int index = 0;
                    List<string> blocklist = new List<string>();
                    int numBytesPerChunk = 250 * 1024; //250KB per block

                    do
                    {
                        byte[] buffer = new byte[numBytesPerChunk];
                        int limit = index + numBytesPerChunk;
                        for (int loops = 0; index < limit; index++)
                        {
                            buffer[loops] = data[index];
                            loops++;
                        }
                        bytesread = index;
                        string blockIdBase64 = Convert.ToBase64String(System.BitConverter.GetBytes(id));

                        blob.PutBlock(blockIdBase64, new MemoryStream(buffer, true), null);
                        blocklist.Add(blockIdBase64);
                        id++;
                    } while (byteslength - bytesread > numBytesPerChunk);

                    int final = byteslength - bytesread;
                    byte[] finalbuffer = new byte[final];
                    for (int loops = 0; index < byteslength; index++)
                    {
                        finalbuffer[loops] = data[index];
                        loops++;
                    }
                    string blockId = Convert.ToBase64String(System.BitConverter.GetBytes(id));
                    blob.PutBlock(blockId, new MemoryStream(finalbuffer, true), null);
                    blocklist.Add(blockId);

                    blob.PutBlockList(blocklist);
                }
                else
                {
                    blob.UploadFromStream(InputStream);
                }
            }
            catch (StorageException ex)
            {
                if (ex.RequestInformation.HttpStatusCode == (int)System.Net.HttpStatusCode.Conflict)
                {
                    // Handle duplicate blob condition
                }
                throw;
            }

        }



        private byte[] ByteArrayFromStream(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}
