﻿using AzureStorage.BlobModels;
using AzureStorage.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AzureStorage.Core
{
    public class AzureBlobStorage
    {
        public static async Task<bool> UploadFile(FileDto file)
        {

            BlobWrapper _blobWrapper = new BlobWrapper();
            try
            {
                var respose = await _blobWrapper.UploadFileToBlob(file);
                return respose;
            }
            catch (Exception ex)
            { }
            return true;
        }

        public static async Task<bool> DeleteFile(FileInfoDto file)
        {
            BlobWrapper _blobWrapper = new BlobWrapper();
            var respose = await _blobWrapper.DeleteFileFromBlob(file);
            return respose;
        }


        public static async Task<bool> DownloadFile(FileInfoDto file)
        {
            BlobWrapper _blobWrapper = new BlobWrapper();
            Task<byte[]> data = _blobWrapper.DownloadFileFromBlob(file);
            byte[] d = await data;
            return true;
        }
    }
}
