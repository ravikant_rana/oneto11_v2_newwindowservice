﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
namespace OneTo11_MoveOldArchiveToNewArchive
{
    public partial class MoveOldArchiveToNewArchiveService : ServiceBase
    {
        

        private const string exchangeName = "HelloWorld_RabbitMQ";
        private const string queueName = "HelloQueue";
        private Timer timer1 = null;
        public MoveOldArchiveToNewArchiveService()
        {
            InitializeComponent();
        }

        public void StartMoveOldArchiveToNewArchiveService()
        {
            //OnStart(null);
            Execute(null, null);
            //System.Threading.Thread.Sleep(1000000000);
        }

        protected override void OnStart(string[] args)
        {
            timer1 = new Timer();
            this.timer1.Interval = 1000;  //for 1  second
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.Execute);
            timer1.Enabled = true;
            //Execute(null, null);
        }

        public void Execute(object sender, ElapsedEventArgs e)
        {
            
            //timer1.Enabled = false;

            MoveReceiver moveReceiver = new MoveReceiver();
            moveReceiver.GetMatches();

            //timer1.Enabled = true;
            
        }

        protected override void OnStop()
        {
        }

        
    }
}
