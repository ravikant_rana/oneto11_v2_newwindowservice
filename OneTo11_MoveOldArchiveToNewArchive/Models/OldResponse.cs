﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
namespace OneTo11_MoveOldArchiveToNewArchive.Models
{
    public class LiveMatches : MongoDbGenericRepository.Models.Document
    {
        public Int64 dbId { get; set; }
        public string cid { get; set; }
        public string match_id { get; set; }
        public string title { get; set; }
        public Int32 formatId { get; set; }
        public string format_str { get; set; }
        public Int32 status { get; set; }
        public string status_str { get; set; }
        public string status_note { get; set; }
        public string TeamA_Id { get; set; }
        public string TeamA { get; set; }
        public string TeamA_Logo { get; set; }
        public string TeamB_Id { get; set; }
        public string TeamB { get; set; }
        public string TeamB_Logo { get; set; }
        public string ContestName { get; set; }
        public string TeamA_score { get; set; }
        public string TeamA_fullscore { get; set; }
        public string TeamB_score { get; set; }
        public string TeamB_fullscore { get; set; }
        public string TeamA_overs { get; set; }
        public string TeamB_overs { get; set; }
        public string GameTypeId { get; set; }
        public DateTime MatchStartTime { get; set; }
        public DateTime MatchEndTime { get; set; }
        public string seriesname { get; set; }
        public List<TeamPlayer> TeamPlayer { get; set; }

    }

    public class MatchTeamPlayer
    {
        public Int64 MatchTeamPlayerId { get; set; }
        public Int64 MatchId { get; set; }
        public string TeamId { get; set; }
        public string Name { get; set; }
        public bool IsAnnounced { get; set; }
        public decimal SelectedBy { get; set; }
        public decimal CaptainBy { get; set; }
        public decimal ViceCaptainBy { get; set; }
        public decimal FantacyPoints { get; set; }
        public string PlayerImage { get; set; }
        public int PlayerType { get; set; }
        public decimal Score { get; set; }
        public decimal Credit { get; set; }
        public string TeamThumbUrl { get; set; }
        public string TeamLogoUrl { get; set; }

    }

    public class TeamPlayer
    {
        public Int64 MatchTeamPlayerId { get; set; }
        public Int64 MatchId { get; set; }
        public Int64 TeamId { get; set; }
        public string TeamName { get; set; }
        public string Team_Abbr { get; set; }
        public string Name { get; set; }
        public string Playing_Role { get; set; }
        public decimal SelectedBy { get; set; }
        public decimal CaptainBy { get; set; }
        public decimal ViceCaptainBy { get; set; }
        public decimal FantacyPoints { get; set; }
        public string Rating { get; set; }
        public string PlayerImage { get; set; }
        public bool? IsAnnounced { get; set; }
        public string starting11 { get; set; }
        public string run { get; set; }
        public string four { get; set; }
        public string six { get; set; }
        public string sr { get; set; }
        public string fifty { get; set; }
        public string duck { get; set; }
        public string wkts { get; set; }
        public string maidenover { get; set; }
        public string er { get; set; }
        public string _catch { get; set; }
        public string runoutstumping { get; set; }
        public string runoutthrower { get; set; }
        public string runoutcatcher { get; set; }
        public string directrunout { get; set; }
        public string stumping { get; set; }
        public string thirty { get; set; }
        public string bonus { get; set; }
    }

    public class AllLiveGames : MongoDbGenericRepository.Models.Document
    {
        public Int64 dbId { get; set; }
        public string match_id { get; set; }
    }

    public class MatchContest : MongoDbGenericRepository.Models.Document
    {
        public MatchContest()
        {
            //user = new List<MatchLeagueUserTeams>();
        }
        public long dbId { get; set; }
        public long? MatchId { get; set; }
        public string API_MatchId { get; set; }
        public string Title { get; set; }
        public Int32? MaxUsers { get; set; }
        public decimal? WinerPercent { get; set; }
        public string ContestInvitationCode { get; set; }
        public decimal? TotalContestAmount { get; set; }
        public long TotalTeam { get; set; }
        public long? FirstWinner { get; set; }
        public decimal? EntryFee { get; set; }
        public bool IsConfirm { get; set; }
        public bool MultipleEntryAllowed { get; set; }
        public bool IsPractice { get; set; }
        public int? status { get; set; }
        public string status_str { get; set; }
    }

    public class MatchContestUserTeams : MongoDbGenericRepository.Models.Document
    {
        public long MatchContestUserTeamId { get; set; }
        public long? UserTeamId { get; set; }
        public long? UserId { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long MatchContestId { get; set; }
        public long MatchId { get; set; }
        public bool isInWinning_Jone { get; set; }
        public decimal? winning_Amount { get; set; }
        public long? rank { get; set; }
        public string TeamAbbr { get; set; }
        public bool IsWinningAmountAdded { get; set; }
    }

    public class ApproveContestResult
    {
        public long? UserId { get; set; }
        public long? MatchId { get; set; }
        public long? MatchContestId { get; set; }
        public long? UserTeamId { get; set; }
        public string TeamName { get; set; }
        public decimal? FantacyPoints { get; set; }
        public decimal? WinningAmount { get; set; }
        public bool IsInWinning_Jone { get; set; }
        public long? Rank { get; set; }
    }

    public class ProcSaveApproveContestResult
    {
        public int ResultStatus { get; set; }
        public string Result { get; set; }
    }

    public class MatchList
    {
        [Description("match_key")]
        public long? mchk { get; set; }
        

    }

    public class MatchContestPrizeBreakUpData : MongoDbGenericRepository.Models.Document
    {
        public long dbId { get; set; }
        public long? MatchId { get; set; }
        public long MatchContestId { get; set; }
        public Int32? StartRank { get; set; }
        public Int32? EndRank { get; set; }
        public decimal? Prize { get; set; }
    }

    public class UserTeams : MongoDbGenericRepository.Models.Document
    {
        public UserTeams()
        {
            players = new List<UserTeamPlayers>();
        }
        public Int64 dbId { get; set; }
        public Int64 UserId { get; set; }
        public Int64 MatchId { get; set; }
        public string TeamAbbr { get; set; }
        public Nullable<decimal> FantacyPoints { get; set; }
        public string UserName { get; set; }
        public string ProfileImage { get; set; }
        public int ProfileAvtarId { get; set; }
        public List<UserTeamPlayers> players { get; set; }
    }

    public class UserTeamPlayers
    {
        public Int64 UserTeamPlayerId { get; set; }
        public Int64 UserTeamId { get; set; }
        public Int64 MatchTeamPlayerId { get; set; }
        public Nullable<decimal> FantacyPoints { get; set; }
        public Nullable<Int32> IsCaptain { get; set; }
        public Nullable<Int32> IsViceCaptain { get; set; }
        public string Name { get; set; }
        public Nullable<decimal> Score { get; set; }
        public Nullable<decimal> Credit { get; set; }
        public string PlayerImage { get; set; }
        public string PlayerType { get; set; }
        public decimal? selectdBy { get; set; }
        public decimal? selectdAsCaption { get; set; }
        public decimal? selectdViceCaption { get; set; }
        public bool is_playing { get; set; }
        public Int64? TeamId { get; set; }
        public string TeamName { get; set; }
        public string Team_Abbr { get; set; }
        //public bool is_substitute { get; set; }
    }
}
