﻿using MongoDB.Driver;
using OneTo11_MoveOldArchiveToNewArchive.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneTo11_MoveOldArchiveToNewArchive.Helpers;

namespace OneTo11_MoveOldArchiveToNewArchive
{
    public class MoveReceiver
    {
        private DBAccess dBAccess;
        private DBAccess transactiondBAccess;
        string oldArchiveconnection = ConfigurationManager.AppSettings["OldArchiveMongoConnection"];
        string newArchiveconnection = ConfigurationManager.AppSettings["NewArchiveMongoConnection"];//"mongodb://oneto11admin:Intigate123@104.211.183.244:27017/oneto11live?authSource=admin";

        string connectionGameString = ConfigurationManager.AppSettings["OneTo11_Game_Connection"];
        string connectionTransactionString = ConfigurationManager.AppSettings["OneTo11_Transaction_Connection"];

        MongoClient oldDbClient;
        MongoClient newDbClient;
        public MoveReceiver()
        {
            dBAccess = new DBAccess(connectionGameString);
            transactiondBAccess = new DBAccess(connectionTransactionString);
            oldDbClient = new MongoClient(oldArchiveconnection);
            newDbClient = new MongoClient(newArchiveconnection);
        }

        public void GetMatches()
        {
            string[] notificationParam = {  };
            var data = transactiondBAccess.GetDataFromStoredProcedure<ArchiveMatchMovement>("[dbo].[ProcGetMatchesForOldArchiveToNewArchiveMovement]", notificationParam).ToList();
            foreach(var m in data)
            {
                SaveMatchesToArchive(m.Match_Fk_Id);
                SaveContestToArchive(m.Match_Fk_Id);
                SaveContestUserTeamToArchive(m.Match_Fk_Id);
                SaveMatchContestPrizeBreakUpToArchive(m.Match_Fk_Id);
                SaveUserTeamPointsAndPlayers(m.Match_Fk_Id);
                SaveUserMatchTeamCount(m.Match_Fk_Id);
            }
        }
        public void SaveMatchesToArchive(long match_id)
        {
            try
            {

                var olddatabase = oldDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var newdatabase = newDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);

                var oldArchiveMatchCollection = olddatabase.GetCollection<LiveMatches>("ArchiveMatches");
                var newArchiveMatchCollection = newdatabase.GetCollection<NewLiveMatchInfo>("ArchiveMatches");
                var newMatchTeamPlayerCollection = newdatabase.GetCollection<NewTeamPlayer>("MatchTeamPlayerInfo");

                var filter = Builders<LiveMatches>.Filter.Eq("dbId", match_id);
                var filter1 = Builders<NewLiveMatchInfo>.Filter.Eq("dbId", match_id);
                var filter2 = Builders<NewTeamPlayer>.Filter.Eq("MatchId", match_id);
                var _matchList = oldArchiveMatchCollection.Find(filter).ToList();
                var _matchList1 = newArchiveMatchCollection.Find(filter1).ToList();


                if (_matchList.Count > 0)
                {
                    newArchiveMatchCollection.DeleteMany(filter1);
                    newMatchTeamPlayerCollection.DeleteMany(filter2);

                    List<NewLiveMatchInfo> liveMatchInfo = new List<NewLiveMatchInfo>();
                    foreach (var m in _matchList)
                    {
                        NewLiveMatchInfo mchInfo = new NewLiveMatchInfo();
                        mchInfo._id = Convert.ToInt64(m.dbId);
                        mchInfo.dbId = Convert.ToInt64(m.dbId);
                        mchInfo.cid = Convert.ToString(m.cid);
                        mchInfo.match_id = Convert.ToString(m.match_id);
                        mchInfo.title = Convert.ToString(m.title);
                        mchInfo.formatId = m.formatId;
                        mchInfo.format_str = m.format_str;
                        mchInfo.status = m.status;
                        mchInfo.status_str = m.status_str;
                        mchInfo.status_note = m.status_note;
                        mchInfo.TeamA_Id = m.TeamA_Id;
                        mchInfo.TeamA = m.TeamA;
                        mchInfo.TeamA_Logo = m.TeamA_Logo;
                        mchInfo.TeamB_Id = m.TeamB_Id;
                        mchInfo.TeamB = m.TeamB;
                        mchInfo.TeamB_Logo = m.TeamB_Logo;
                        mchInfo.ContestName = m.ContestName;
                        mchInfo.GameTypeId = m.GameTypeId;
                        mchInfo.MatchStartTime = m.MatchStartTime;
                        mchInfo.MatchEndTime = m.MatchEndTime;
                        mchInfo.seriesname = m.seriesname;
                        mchInfo.IsVerified = true;

                        liveMatchInfo.Add(mchInfo);
                        List<NewTeamPlayer> teamPlayerList = new List<NewTeamPlayer>();
                        foreach (var mtp in m.TeamPlayer)
                        {
                            NewTeamPlayer teamPlayer = new NewTeamPlayer
                            {
                                _id = mtp.MatchTeamPlayerId,
                                MatchTeamPlayerId = mtp.MatchTeamPlayerId,
                                MatchId = mtp.MatchId,
                                TeamId = mtp.TeamId,
                                TeamName = mtp.TeamName,
                                Team_Abbr = mtp.Team_Abbr,
                                Name = mtp.Name,
                                Playing_Role = mtp.Playing_Role,
                                SelectedBy = mtp.SelectedBy,
                                CaptainBy = mtp.CaptainBy,
                                ViceCaptainBy = mtp.ViceCaptainBy,
                                FantacyPoints = mtp.FantacyPoints,
                                PlayerImage = mtp.PlayerImage,
                                IsAnnounced = mtp.IsAnnounced,
                                Rating = mtp.Rating,
                                Credit = (!string.IsNullOrEmpty(mtp.Rating)) ? Convert.ToDecimal(mtp.Rating) : 0,
                                starting11 = mtp.starting11,
                                run = mtp.run,
                                four = mtp.four,
                                six = mtp.six,
                                sr = mtp.sr,
                                fifty = mtp.fifty,
                                duck = mtp.duck,
                                wkts = mtp.wkts,
                                maidenover = mtp.maidenover,
                                er = mtp.er,
                                _catch = mtp._catch,
                                runoutstumping = mtp.runoutstumping,
                                runoutthrower = mtp.runoutthrower,
                                runoutcatcher = mtp.runoutcatcher,
                                directrunout = mtp.directrunout,
                                stumping = mtp.stumping,
                                thirty = mtp.thirty,
                                bonus = mtp.bonus,


                            };

                            teamPlayerList.Add(teamPlayer);
                        }

                        if (teamPlayerList.Count > 0)
                        {
                            newMatchTeamPlayerCollection.InsertMany(teamPlayerList);
                        }


                    }

                    if (liveMatchInfo.Count > 0)
                    {
                        newArchiveMatchCollection.InsertMany(liveMatchInfo);
                    }

                }

            }
            catch (Exception ex)
            {

            }
        }

        public void SaveContestToArchive(long match_id)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();
                var olddatabase = oldDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var newdatabaseArchive = newDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var oldContestCollection = olddatabase.GetCollection<MatchContest>("ArchiveContest");
                var newContestCollection = newdatabaseArchive.GetCollection<NewMatchContest>("ArchiveContest");

                var filter = Builders<MatchContest>.Filter.Eq("MatchId", match_id);
                var filter1 = Builders<NewMatchContest>.Filter.Eq("MatchId", match_id);
                var _contestList = oldContestCollection.Find(filter).ToList();


                if (_contestList.Count > 0)
                {
                    newContestCollection.DeleteMany(filter1);
                    List<NewMatchContest> matchContestList = new List<NewMatchContest>();
                    foreach (var contest in _contestList)
                    {
                        NewMatchContest matchContest = new NewMatchContest();
                        matchContest._id = contest.dbId;
                        matchContest.dbId = contest.dbId;
                        matchContest.MatchId = contest.MatchId;
                        matchContest.API_MatchId = contest.API_MatchId;
                        matchContest.Title = contest.Title;
                        matchContest.MaxUsers = contest.MaxUsers;
                        matchContest.WinerPercent = contest.WinerPercent;
                        matchContest.ContestInvitationCode = contest.ContestInvitationCode;
                        matchContest.TotalContestAmount = contest.TotalContestAmount;
                        matchContest.TotalTeam = contest.TotalTeam;
                        matchContest.FirstWinner = contest.FirstWinner;
                        matchContest.EntryFee = contest.EntryFee;
                        matchContest.IsConfirm = contest.IsConfirm;
                        matchContest.MultipleEntryAllowed = contest.MultipleEntryAllowed;
                        matchContest.IsPractice = contest.IsPractice;
                        matchContest.status = contest.status;
                        matchContest.status_str = contest.status_str;

                        matchContestList.Add(matchContest);


                    }
                    if (matchContestList.Count > 0)
                    {
                        newContestCollection.InsertMany(matchContestList);
                    }

                }
            }
            catch (Exception ex)
            {

            }
        }

        public void SaveContestUserTeamToArchive(long match_id)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();p-
                var olddatabase = oldDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var newdatabaseArchive = newDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveContestUserTeamCollection = olddatabase.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");
                var archiveContestUserTeamCollection = newdatabaseArchive.GetCollection<NewMatchContestUserTeams>("ArchiveContestUserTeam");

                var filter = Builders<MatchContestUserTeams>.Filter.Eq("MatchId", match_id);
                var filter1 = Builders<NewMatchContestUserTeams>.Filter.Eq("MatchId", match_id);
                var _contestUserTeamList = liveContestUserTeamCollection.Find(filter).ToList();



                if (_contestUserTeamList.Count > 0)
                {
                    archiveContestUserTeamCollection.DeleteMany(filter1);
                    List<NewMatchContestUserTeams> matchContestUserTeamsList = new List<NewMatchContestUserTeams>();

                    foreach (var cut in _contestUserTeamList)
                    {
                        NewMatchContestUserTeams matchContestUserTeams = new NewMatchContestUserTeams();
                        matchContestUserTeams._id = cut.MatchContestUserTeamId;
                        matchContestUserTeams.MatchContestUserTeamId = cut.MatchContestUserTeamId;
                        matchContestUserTeams.UserTeamId = cut.UserTeamId;
                        matchContestUserTeams.UserId = cut.UserId;
                        matchContestUserTeams.FantacyPoints = cut.FantacyPoints;
                        matchContestUserTeams.MatchContestId = cut.MatchContestId;
                        matchContestUserTeams.MatchId = cut.MatchId;
                        matchContestUserTeams.isInWinning_Jone = cut.isInWinning_Jone;
                        //matchContestUserTeams.winning_Amount = (((cut.winning_Amount ?? 0) > 0 && (rankCount > 0)) ? (cut.winning_Amount / rankCount) : cut.winning_Amount);
                        matchContestUserTeams.winning_Amount = cut.winning_Amount;
                        matchContestUserTeams.rank = cut.rank;
                        matchContestUserTeams.TeamAbbr = cut.TeamAbbr;
                        matchContestUserTeams.IsWinningAmountAdded = cut.IsWinningAmountAdded;
                        matchContestUserTeamsList.Add(matchContestUserTeams);

                    }


                    if (matchContestUserTeamsList.Count > 0)
                    {
                        archiveContestUserTeamCollection.InsertMany(matchContestUserTeamsList);
                    }
                }





            }
            catch (Exception ex)
            {

            }
        }

        public void SaveMatchContestPrizeBreakUpToArchive(long match_id)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();
                var olddatabase = oldDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var newdatabaseArchive = newDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveLeaguesPrizeBreakupCollection = olddatabase.GetCollection<MatchContestPrizeBreakUpData>("ArchiveMatchContestPrizeBreakup");
                var archiveLeaguesPrizeBreakupCollection = newdatabaseArchive.GetCollection<NewMatchContestPrizeBreakUpDataMongoLive>("ArchiveMatchContestPrizeBreakup");

                var filter = Builders<MatchContestPrizeBreakUpData>.Filter.Eq("MatchId", match_id);
                var filter1 = Builders<NewMatchContestPrizeBreakUpDataMongoLive>.Filter.Eq("MatchId", match_id);
                var _leaguePrizeBreakupList = liveLeaguesPrizeBreakupCollection.Find(filter).ToList();


                if (_leaguePrizeBreakupList.Count > 0)
                {
                    archiveLeaguesPrizeBreakupCollection.DeleteMany(filter1);
                    List<NewMatchContestPrizeBreakUpDataMongoLive> _list = new List<NewMatchContestPrizeBreakUpDataMongoLive>();
                    foreach(var p in _leaguePrizeBreakupList)
                    {
                        NewMatchContestPrizeBreakUpDataMongoLive pb = new NewMatchContestPrizeBreakUpDataMongoLive
                        {
                            _id = p.dbId,
                            dbId = p.dbId,
                            MatchId = p.MatchId,
                            MatchContestId = p.MatchContestId,
                            StartRank = p.StartRank,
                            EndRank = p.EndRank,
                            Prize = p.Prize

                        };

                        _list.Add(pb);
                    }

                    if(_list.Count > 0)
                    {
                        archiveLeaguesPrizeBreakupCollection.InsertMany(_list);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        public void SaveUserTeamPointsAndPlayers(long match_id)
        {
            var olddatabase = oldDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
            var newdatabaseArchive = newDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
            var liveLeaguesPrizeBreakupCollection = olddatabase.GetCollection<UserTeams>("ArchiveUserTeam");
            var archiveLeaguesPrizeBreakupCollection = newdatabaseArchive.GetCollection<NewUserTeamPoints>("ArchiveUserTeamPoints");
            var liveUserTeamPlayerCollection = newdatabaseArchive.GetCollection<NewUserTeamPlayersLive>("ArchiveUserTeamPlayers");

            var filter = Builders<UserTeams>.Filter.Eq("MatchId", match_id);
            var filter1 = Builders<NewUserTeamPoints>.Filter.Eq("MatchId", match_id);
            var filter2 = Builders<NewUserTeamPlayersLive>.Filter.Eq("MatchId", match_id);
            var _leaguePrizeBreakupList = liveLeaguesPrizeBreakupCollection.Find(filter).ToList();

            if(_leaguePrizeBreakupList.Count > 0)
            {
                archiveLeaguesPrizeBreakupCollection.DeleteMany(filter1);
                liveUserTeamPlayerCollection.DeleteMany(filter2);
                List<NewUserTeamPoints> pList = new List<NewUserTeamPoints>();
                foreach(var up in _leaguePrizeBreakupList)
                {
                    NewUserTeamPoints nutp = new NewUserTeamPoints
                    {
                        _id = up.dbId,
                        UserTeamId = up.dbId,
                        FantacyPoints = up.FantacyPoints,
                        MatchId = up.MatchId,
                        TeamAbbr = up.TeamAbbr,
                        UserId = up.UserId,
                        UserAvatarId = up.ProfileAvtarId
                    };

                    pList.Add(nutp);
                    List<NewUserTeamPlayersLive> newplayer = new List<NewUserTeamPlayersLive>();
                    foreach (var u in up.players)
                    {
                        NewUserTeamPlayersLive player = new NewUserTeamPlayersLive
                        {
                            _id = u.UserTeamPlayerId,
                            UserTeamPlayerId = u.UserTeamPlayerId,
                            MatchTeamPlayerId = u.MatchTeamPlayerId,
                            FantacyPoints = u.FantacyPoints,
                            IsCaptain = u.IsCaptain ?? 0,
                            IsViceCaptain = u.IsViceCaptain ?? 0,
                            MatchId = up.MatchId,
                            UserTeamId = u.UserTeamId

                        };

                        newplayer.Add(player);

                    }

                    if(newplayer.Count > 0)
                    {
                        liveUserTeamPlayerCollection.InsertMany(newplayer);
                    }
                }

                if(pList.Count > 0)
                {
                    archiveLeaguesPrizeBreakupCollection.InsertMany(pList);
                }
            }
        }

        public void SaveUserMatchTeamCount(long match_id)
        {
            try
            {
                string[] notificationParam = { "@MatchId" };
                var data = dBAccess.GetDataFromStoredProcedure<ArchiveUserMatchTeamCount>("[dbo].[uspGetUserContestAndTeamCount]", notificationParam, match_id).ToList();
                if (data.Count > 0)
                {
                    var databaseArchive = newDbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                    var archiveArchiveUserMatchTeamCountCollection = databaseArchive.GetCollection<ArchiveUserMatchTeamCount>("ArchiveUserMatchTeamCount");
                    var filter = Builders<ArchiveUserMatchTeamCount>.Filter.Eq("MatchId", match_id);
                    var filter1 = Builders<ArchiveUserMatchTeamCount>.Filter.Eq("MatchId", match_id);
                    var _List = archiveArchiveUserMatchTeamCountCollection.Find(filter).ToList();
                    if (_List.Count == 0)
                    {
                        archiveArchiveUserMatchTeamCountCollection.DeleteMany(filter1);
                        archiveArchiveUserMatchTeamCountCollection.InsertMany(data);
                    }
                }
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}
