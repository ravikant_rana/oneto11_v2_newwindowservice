﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_MoveOldArchiveToNewArchive
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
#if DEBUG

            new MoveOldArchiveToNewArchiveService().StartMoveOldArchiveToNewArchiveService();
#else
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new MoveOldArchiveToNewArchiveService()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
