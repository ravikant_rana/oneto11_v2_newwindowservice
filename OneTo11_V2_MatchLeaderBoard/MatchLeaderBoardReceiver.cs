﻿using MongoDB.Driver;
using Newtonsoft.Json;
using OneTo11_V2_MatchLeaderBoard.Helpers;
using OneTo11_V2_MatchLeaderBoard.Models;
using OneTo11_V2_MatchLeaderBoard.QueryExtensions;
using OneTo11_V2_Redis;
using OneTo11_V2_Redis.Helpers;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MatchLeaderBoard
{
    public partial class MatchLeaderBoardReceiver : DefaultBasicConsumer
    {
        private readonly IModel _channel;
        private DBAccess dBAccess;
        private DBAccess dBAuthAccess;
        private DBAccess dBTransactionAccess;
        string connectionGameString = ConfigurationManager.AppSettings["OneTo11_Game_Connection"];
        string connectionAuthString = ConfigurationManager.AppSettings["OneTo11_Auth_Connection"];
        string connectionTransactionString = ConfigurationManager.AppSettings["OneTo11_Transaction_Connection"];
        string connection = ConfigurationManager.AppSettings["ArchiveMongoConnection"]; //"mongodb+srv://oneto11rootuser:Wve8BYfHtexpmOSG@cluster1.t98az.mongodb.net/test";
        MongoClient dbClient;

        public MatchLeaderBoardReceiver(IModel channel)
        {
            _channel = channel;
            dBAccess = new DBAccess(connectionGameString);
            dBAuthAccess = new DBAccess(connectionAuthString);
            dBTransactionAccess = new DBAccess(connectionTransactionString);
            dbClient = new MongoClient(connection);

        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            //Library.WriteErrorLog("Live updates receiver started at: " + DateTime.Now);
            try
            {
                CompleteMatch jsonCompleteMatch = new CompleteMatch();

                string jsonData = Encoding.UTF8.GetString(body);


                jsonCompleteMatch = JsonConvert.DeserializeObject<CompleteMatch>(jsonData);

                //GetMegaContestId(jsonCompleteMatch.match_id);
                AddWorldCupMegaContestWinners(jsonCompleteMatch.match_id);

                _channel.BasicAck(deliveryTag, false);
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void GetMegaContestId(long match_id)
        {
            try
            {
                string cid = Convert.ToString(ConfigurationManager.AppSettings["cid"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);                
                var archiveMatchCollection = databaseArchive.GetCollection<LiveMatchInfo>("ArchiveMatches");

                var filter = Builders<LiveMatchInfo>.Filter.Eq("dbId", match_id);
                if (!string.IsNullOrEmpty(cid))
                {
                    filter = filter & Builders<LiveMatchInfo>.Filter.Eq("cid", cid);

                    var _matchList = archiveMatchCollection.Find(filter).ToList();
                    foreach (var mch in _matchList)
                    {
                        string matchName = mch.TeamA + " Vs " + mch.TeamB;
                        string[] paramScheduleName = { "@MatchId", "@SeasonId" };
                        var _megaResult = dBAccess.GetDataFromStoredProcedure<MegaContest>("[dbo].[PROC_GET_MatchLeaderBoardContest]", paramScheduleName, match_id, mch.cid).ToList();
                        foreach (var item in _megaResult)
                        {                            
                            SaveMegaContestLeaderBoard(match_id, item.cid, matchName, mch.MatchStartTime);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {

                Library.WriteErrorLog(ex);
            }

        }

        public void SaveMegaContestLeaderBoard(long match_id, long ContestId, string MatchName, DateTime? MatchStartDate)
        {
            try
            {
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");

                var filter = Builders<MatchContestUserTeams>.Filter.Eq("MatchContestId", ContestId);
                var _contestUserTeamList = archiveContestUserTeamCollection.Find(filter).ToList();
                if (_contestUserTeamList.Count > 0)
                {
                    List<MegaLeaderBoardResult> megaLeaderBoardResultList = new List<MegaLeaderBoardResult>();
                    foreach (var item in _contestUserTeamList)
                    {
                        MegaLeaderBoardResult megaLeaderBoardResult = new MegaLeaderBoardResult();
                        megaLeaderBoardResult.UserId = item.UserId;
                        megaLeaderBoardResult.FantacyPoints = item.FantacyPoints;
                        megaLeaderBoardResult.UserTeamId = item.UserTeamId;
                        megaLeaderBoardResult.TeamAbbr = item.TeamAbbr;
                        megaLeaderBoardResultList.Add(megaLeaderBoardResult);
                    }

                    if (megaLeaderBoardResultList.Count > 0)
                    {
                        string[] paramScheduleName = { "@match_id", "@match_name", "@date_start", "@dtMatchLeaderBoard" };
                        var _megaResult = dBAuthAccess.SaveDataStoredProcedure("[dbo].[PROC_SAVE_MATCH_LEADERBOARD]", paramScheduleName, match_id, MatchName, MatchStartDate, DataTableExtension.ConvertListToDataTable(megaLeaderBoardResultList));

                        string[] paramName = { "@MatchId" };
                        var _result = dBAuthAccess.SaveDataStoredProcedure("[dbo].[PROC_DECLARE_IPL_BatWinner]", paramName, match_id);

                        string[] paramContestWinner = { "@math_id" };
                        DataTable dataTable = dBAuthAccess.GetDataFromStoredProcedure("[dbo].[PROC_SAVE_DAILY_MATCH_CONTEST_WINNERS]", paramContestWinner, match_id);
                        if(dataTable.Rows.Count > 0)
                        {
                            string[] paramTransaction = { "@math_id","@contest_id","@dtMatchContestWinners" };
                            var _res = dBTransactionAccess.SaveDataStoredProcedure("[dbo].[PROC_ADDED_MEGA_CONTEST_OFFER_TO_ADDED_BALANCE]", paramTransaction, match_id, ContestId, dataTable);
                        }


                        DeleteLeaderboardCache();
                    }

                }
            }
            catch (Exception ex)
            {

                Library.WriteErrorLog(ex);
            }

        }

        public void AddWorldCupMegaContestWinners(long match_id)
        {
            try
            {
                string cid = Convert.ToString(ConfigurationManager.AppSettings["cid"]);
                string[] paramName = { "@MatchId", "@Cid" };
                DataTable dataTable = dBAccess.GetDataFromStoredProcedure("[dbo].[PROC_GET_WORLD_CUP_CONTEST_WINNERS]", paramName, match_id, cid);
                if (dataTable.Rows.Count > 0)
                {
                    string[] paramWinnersName = { "@MatchId", "@dtYesterdayConestWinners" };
                    DataTable dataTable1 = dBAuthAccess.GetDataFromStoredProcedure("[dbo].[PROC_SAVE_WORLD_CUP_MEGA_CONTEST_WINNERS]", paramWinnersName, match_id, dataTable);
                    if (dataTable1.Rows.Count > 0)
                    {
                        string[] paramTransaction = { "@MatchId", "@dtMatchContestWinners" };
                        var _res = dBTransactionAccess.SaveDataStoredProcedure("[dbo].[PROC_ADDED_WORLD_CUP_MEGA_CONTEST_OFFER_TO_ADDED_BALANCE]", paramTransaction, match_id, dataTable1);

                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void DeleteLeaderboardCache()
        {
            try
            {
                using (var cache = new CacheService())
                {
                    string iplBatWinnerCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ":" + ConfigurationManager.AppSettings["redis:IplBatWinner"];
                    string iplLeaderboardCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ":" + ConfigurationManager.AppSettings["redis:IplLeaderboard"];
                    string iplDailyWinnerCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ":" + ConfigurationManager.AppSettings["redis:DailyWinner"]+ ":IPL";
                    string dailyWinnerCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ":" + ConfigurationManager.AppSettings["redis:DailyWinner"]+ ":Daily";

                    IntigateCache.DeleteCache(iplBatWinnerCache);
                    IntigateCache.DeleteCache(iplLeaderboardCache);
                    IntigateCache.DeleteCache(iplDailyWinnerCache);
                    IntigateCache.DeleteCache(dailyWinnerCache);
                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }
    }
}
