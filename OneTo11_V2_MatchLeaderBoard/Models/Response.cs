﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MatchLeaderBoard.Models
{
    public class CompleteMatch
    {
        public long match_id { get; set; }
    }
    public class MegaContest
    {
        public long cid { get; set; }
    }

    public class MatchContestUserTeams
    {
        public MatchContestUserTeams()
        {

        }
        public long _id { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long MatchContestId { get; set; }
        public long MatchContestUserTeamId { get; set; }
        public long MatchId { get; set; }
        public long? UserId { get; set; }
        public long? UserTeamId { get; set; }
        public string TeamAbbr { get; set; }
        public bool isInWinning_Jone { get; set; }
        public long? rank { get; set; }
        public decimal? winning_Amount { get; set; }
        public bool IsWinningAmountAdded { get; set; }
    }

    public class MegaLeaderBoardResult
    {        
        public long? UserId { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long? UserTeamId { get; set; }
        public string TeamAbbr { get; set; }
    }

    public class LiveMatchInfo
    {
        public Int64 _id { get; set; }
        public Int64 dbId { get; set; }
        public string cid { get; set; }
        public string match_id { get; set; }
        public string title { get; set; }
        public Int32 formatId { get; set; }
        public string format_str { get; set; }
        public Int32 status { get; set; }
        public string status_str { get; set; }
        public string status_note { get; set; }
        public string TeamA_Id { get; set; }
        public string TeamA { get; set; }
        public string TeamA_Logo { get; set; }
        public string TeamB_Id { get; set; }
        public string TeamB { get; set; }
        public string TeamB_Logo { get; set; }
        public string ContestName { get; set; }
        public string TeamA_score { get; set; }
        public string TeamA_fullscore { get; set; }
        public string TeamB_score { get; set; }
        public string TeamB_fullscore { get; set; }
        public string TeamA_overs { get; set; }
        public string TeamB_overs { get; set; }
        public string GameTypeId { get; set; }
        public DateTime MatchStartTime { get; set; }
        public DateTime MatchEndTime { get; set; }
        public string seriesname { get; set; }

        public bool IsVerified { get; set; }
    }
}
