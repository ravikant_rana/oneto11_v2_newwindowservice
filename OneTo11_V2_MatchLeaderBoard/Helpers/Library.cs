﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MatchLeaderBoard.Helpers
{
    public static class Library
    {

        public static void WriteErrorLog(Exception Ex, string ListenerName = "")
        {
            StreamWriter sw = null;

            try
            {
                if (!string.IsNullOrEmpty(ListenerName))
                    sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Log_" + ListenerName + ".txt", true);
                else
                    sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Log.txt", true);
                sw.WriteLine(DateTime.Now + " : " + Ex.Source.ToString().Trim() + ";" + Ex.Message.Trim() + ":" + Ex.StackTrace + ":" + Ex.InnerException);
                sw.Flush();
                sw.Close();

            }
            catch (Exception ex)
            {


            }
        }

        public static void WriteErrorLog(string Ex, string ListenerName = "")
        {
            StreamWriter sw = null;

            try
            {
                if (!string.IsNullOrEmpty(ListenerName))
                    sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Log_" + ListenerName + ".txt", true);
                else
                    sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\Log.txt", true);
                sw.WriteLine(DateTime.Now + " : " + Ex.Trim());
                sw.Flush();
                sw.Close();

            }
            catch (Exception ex)
            {


            }
        }

        public static void WriteErrorLog(string Ex)
        {
            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now + " : " + Ex.Trim());
                sw.Flush();
                sw.Close();

            }
            catch (Exception ex)
            {


            }
        }
    }
}
