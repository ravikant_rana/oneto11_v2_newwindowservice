﻿using Newtonsoft.Json;
using OneTo11_V2_Redis.Helpers;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_Redis
{
    public class IntigateLeaderboardCache
    {
        public IntigateLeaderboardCache()
        {

        }

        public static void DeleteCache(string cachename)
        {
            //string cachename = "Common.FruitList";
            try
            {

                var cache = new LeaderboardCacheService();

                if (cache.IsAlive())
                {
                    cache.Remove(cachename);

                    #region purpose: quick checking if the cache is deleted
                    //if (cache.Exists(cachename))
                    //{
                    //    Console.Write("still exist");
                    //}
                    //else
                    //{
                    //    Console.Write("deleted");
                    //}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                //TO DO if delete failed
                Console.Write(ex.Message);
            }
        }




        #region cache operations




        /// <summary>
        /// Retrieve Data from Cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cachename"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static List<T> RetrieveCacheByCacheName<T>(string cachename, LeaderboardCacheService cache)
        {
            var genList = new List<T>();

            if (cache.IsAlive())
            {
                var deserializeObj = cache.Get(cachename);
                if (deserializeObj != null)
                    genList = JsonConvert.DeserializeObject<List<T>>(deserializeObj);

            }
            return genList;
        }


        /// <summary>
        /// Retrieve Data from Cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cachename"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static async Task<List<T>> RetrieveCacheByCacheNameAsync<T>(string cachename, LeaderboardCacheService cache)
        {
            var genList = new List<T>();


            var deserializeObj = await cache.GetAsync(cachename);
            if (deserializeObj != null)
                genList = JsonConvert.DeserializeObject<List<T>>(deserializeObj);

            return genList;
        }

        public static Dictionary<string, string> RetrieveDictionaryCacheByCacheName(string cachename, LeaderboardCacheService cache)
        {
            var genList = new Dictionary<string, string>();

            if (cache.IsAlive())
            {

                var deserializeObj = cache.Get(cachename);
                if (deserializeObj != null)
                    genList = JsonConvert.DeserializeObject<Dictionary<string, string>>(deserializeObj);

            }
            return genList;
        }

        public static async Task<Dictionary<string, string>> RetrieveDictionaryCacheByCacheNameAsync(string cachename, LeaderboardCacheService cache)
        {
            var genList = new Dictionary<string, string>();

            if (cache.IsAlive())
            {

                var deserializeObj = await cache.GetAsync(cachename);
                if (deserializeObj != null)
                    genList = JsonConvert.DeserializeObject<Dictionary<string, string>>(deserializeObj);

            }
            return genList;
        }


        /// <summary>
        /// Save Class Object to Cache.
        /// </summary>
        /// <typeparam name="T">Class Type</typeparam>
        /// <param name="cache">Cache Class Object</param>
        /// <param name="cachename">Redis Cache Key Name</param>
        /// <param name="t">List Object of Class</param>
        public static void SaveToCache<T>(LeaderboardCacheService cache, string cachename, List<T> t)
        {
            string serializeObj = Serialize(t);
            //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            if (cache.IsAlive())
                cache.Save(cachename, serializeObj);
        }

        public static void SaveToCacheDictionary(LeaderboardCacheService cache, string cachename, Dictionary<string, object> t)
        {
            string serializeObj = Serialize(t);
            //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            if (cache.IsAlive())
                cache.Save(cachename, serializeObj);
        }

        /// <summary>
        /// Save Class Object to Cache.
        /// </summary>
        /// <typeparam name="T">Class Type</typeparam>
        /// <param name="cache">Cache Class Object</param>
        /// <param name="cachename">Redis Cache Key Name</param>
        /// <param name="t">List Object of Class</param>
        //static int retryCount = 3;
        public static void SaveToCacheWithoutAsync<T>(LeaderboardCacheService cache, string cachename, List<T> t)
        {
            string serializeObj = Serialize(t);
            //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));


            if (cache.IsAlive())
                cache.SaveWithoutAsync(cachename, serializeObj);

            //if (cache.IsAlive())
            //{
            //    cache.SaveWithoutAsync(cachename, serializeObj);
            //}
            //else {
            //    WriteErrorLog("Cache is not alive");
            //}
        }


        ///// <summary>
        ///// Save Class Object to Cache.
        ///// </summary>
        ///// <typeparam name="T">Class Type</typeparam>
        ///// <param name="cache">Cache Class Object</param>
        ///// <param name="cachename">Redis Cache Key Name</param>
        ///// <param name="t">Single Object of Class</param>
        //public static void SaveToCache<T>(CacheService cache, string cachename, T t)
        //{
        //    string serializeObj = Serialize(t);
        //    cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
        //}




        /// <summary>
        /// Update List Object Details
        /// </summary>
        /// <typeparam name="T">Class Type</typeparam>
        /// <param name="cache">Cache Class Object</param>
        /// <param name="cachename">Redis Cache Key Name</param>
        /// <param name="t">List Object of Class</param>
        public static void UpdateOrSaveCache<T>(LeaderboardCacheService cache, string cachename, List<T> t)
        {
            string serializeObj = Serialize(t);

            //if (cache.Exists(cachename))
            //{
            //    cache.Remove(cachename);
            //    cache.Save(cachename, serializeObj);
            //    //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            //}
            //else
            //{
            if (cache.IsAlive())
                cache.Save(cachename, serializeObj);
            //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            //}
        }

        public static void UpdateOrSaveDictionaryCache(LeaderboardCacheService cache, string cachename, Dictionary<string, string> t)
        {
            string serializeObj = Serialize(t);
            if (cache.IsAlive())
                cache.Save(cachename, serializeObj);
            //if (cache.Exists(cachename))
            //{
            //    cache.Remove(cachename);
            //    cache.Save(cachename, serializeObj);
            //    //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            //}
            //else
            //{
            //    cache.Save(cachename, serializeObj);
            //    //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            //}
        }

        public static RedisValue[] RetrieveHsetByCacheName(string cachename, LeaderboardCacheService cache, List<Int64> values)
        {
            RedisValue[] val = new RedisValue[values.Count];
            int i = 0;
            foreach (var item in values)
            {
                val.SetValue(new RedisValue(item.ToString()), i);
                i++;
            }
            return cache.HashGet(cachename, val);

        }

        public static RedisValue RetrieveSingleHsetByCacheName(string cachename, LeaderboardCacheService cache, Int64 values)
        {
            return cache.SingleHashGet(cachename, new RedisValue(values.ToString()));

        }

        public static HashEntry[] RetrieveHgetAllByCacheName(string cachename, LeaderboardCacheService cache)
        {
            return cache.HashGetAll(cachename);

        }

        public static void SaveToCacheHash(LeaderboardCacheService cache, string cachename, HashEntry[] t)
        {
            string serializeObj = Serialize(t);
            if (cache.IsAlive())
                cache.SaveHash(cachename, t);
        }





        ///// <summary>
        ///// Update Single Object Details
        ///// </summary>
        ///// <typeparam name="T">Class Type</typeparam>
        ///// <param name="cache">Cache Class Object</param>
        ///// <param name="cachename">Redis Cache Key Name</param>
        ///// <param name="t">Single Object of Class</param>
        //public static void UpdateOrSaveCache<T>(CacheService cache, string cachename, T t)
        //{
        //    string serializeObj = Serialize(t);

        //    if (cache.Exists(cachename))
        //    {
        //        cache.Remove(cachename);
        //        cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
        //    }
        //    else
        //    {
        //        cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
        //    }
        //}


        #endregion


        #region "Remove All Keys"
        public static void RemoveAllContainerKeys()
        {

            try
            {
                var cache = new LeaderboardCacheService();

                if (cache.IsAlive())
                {

                    cache.RemoveAllContainerKeys();

                }
            }
            catch (Exception e)
            {


            }
        }



        #endregion "End Remove All Keys"


        #region "Remove All Keys"
        public static void RemoveAllContainerKeys(string KeyStart)
        {

            try
            {
                var cache = new LeaderboardCacheService();

                if (cache.IsAlive())
                {

                    cache.RemoveAllContainerKeys(KeyStart);

                }
            }
            catch (Exception e)
            {


            }
        }



        #endregion "End Remove All Keys"




        #region supports

        private static string Serialize(object o)
        {
            string s = string.Empty;

            s = JsonConvert.SerializeObject(o);

            return s;
        }

        #endregion

        public static void WriteErrorLog(string Ex)
        {
            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\CacheLogFile.txt", true);
                sw.WriteLine(DateTime.Now + " : " + Ex.Trim());
                sw.Flush();
                sw.Close();

            }
            catch (Exception ex)
            {


            }
        }
    }
}
