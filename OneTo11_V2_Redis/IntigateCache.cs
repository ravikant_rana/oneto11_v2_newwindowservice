﻿using Newtonsoft.Json;
using OneTo11_V2_Redis.Helpers;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_Redis
{
    public class IntigateCache
    {
        public IntigateCache()
        {

        }


        public static void DeleteCache(string cachename)
        {
            //string cachename = "Common.FruitList";
            try
            {

                var cache = new CacheService();

                if (cache.IsAlive())
                {
                    cache.Remove(cachename);

                    #region purpose: quick checking if the cache is deleted
                    //if (cache.Exists(cachename))
                    //{
                    //    Console.Write("still exist");
                    //}
                    //else
                    //{
                    //    Console.Write("deleted");
                    //}
                    #endregion
                }
            }
            catch (Exception ex)
            {
                //TO DO if delete failed
                Console.Write(ex.Message);
            }
        }

        public static void DeleteToCacheHash(CacheService cache, string cachename, RedisValue t)
        {
            //string serializeObj = Serialize(t);
            //if (cache.IsAlive())
            try
            {
                cache.DeleteHashAsync(cachename, t);
            }
            catch (Exception ex)
            {

            }
        }


        #region cache operations




        /// <summary>
        /// Retrieve Data from Cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cachename"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static List<T> RetrieveCacheByCacheName<T>(string cachename, CacheService cache)
        {
            var genList = new List<T>();

            if (cache.IsAlive())
            {
                var deserializeObj = cache.Get(cachename);
                if (deserializeObj != null)
                    genList = JsonConvert.DeserializeObject<List<T>>(deserializeObj);

            }
            return genList;
        }


        /// <summary>
        /// Retrieve Data from Cache.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cachename"></param>
        /// <param name="cache"></param>
        /// <returns></returns>
        public static async Task<List<T>> RetrieveCacheByCacheNameAsync<T>(string cachename, CacheService cache)
        {
            var genList = new List<T>();


            var deserializeObj = await cache.GetAsync(cachename);
            if (deserializeObj != null)
                genList = JsonConvert.DeserializeObject<List<T>>(deserializeObj);

            return genList;
        }

        public static Dictionary<string, string> RetrieveDictionaryCacheByCacheName(string cachename, CacheService cache)
        {
            var genList = new Dictionary<string, string>();

            if (cache.IsAlive())
            {

                var deserializeObj = cache.Get(cachename);
                if (deserializeObj != null)
                    genList = JsonConvert.DeserializeObject<Dictionary<string, string>>(deserializeObj);

            }
            return genList;
        }

        public static async Task<Dictionary<string, string>> RetrieveDictionaryCacheByCacheNameAsync(string cachename, CacheService cache)
        {
            var genList = new Dictionary<string, string>();

            if (cache.IsAlive())
            {

                var deserializeObj = await cache.GetAsync(cachename);
                if (deserializeObj != null)
                    genList = JsonConvert.DeserializeObject<Dictionary<string, string>>(deserializeObj);

            }
            return genList;
        }


        /// <summary>
        /// Save Class Object to Cache.
        /// </summary>
        /// <typeparam name="T">Class Type</typeparam>
        /// <param name="cache">Cache Class Object</param>
        /// <param name="cachename">Redis Cache Key Name</param>
        /// <param name="t">List Object of Class</param>
        public static void SaveToCache<T>(CacheService cache, string cachename, List<T> t)
        {
            string serializeObj = Serialize(t);
            //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            if (cache.IsAlive())
                cache.Save(cachename, serializeObj);
        }

        public static void SaveToCacheDictionary(CacheService cache, string cachename, Dictionary<string, object> t)
        {
            string serializeObj = Serialize(t);
            //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            if (cache.IsAlive())
                cache.Save(cachename, serializeObj);
        }

        /// <summary>
        /// Save Class Object to Cache.
        /// </summary>
        /// <typeparam name="T">Class Type</typeparam>
        /// <param name="cache">Cache Class Object</param>
        /// <param name="cachename">Redis Cache Key Name</param>
        /// <param name="t">List Object of Class</param>
        //static int retryCount = 3;
        public static void SaveToCacheWithoutAsync<T>(CacheService cache, string cachename, List<T> t)
        {
            string serializeObj = Serialize(t);
            //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));


            if (cache.IsAlive())            
                cache.SaveWithoutAsync(cachename, serializeObj);

            //if (cache.IsAlive())
            //{
            //    cache.SaveWithoutAsync(cachename, serializeObj);
            //}
            //else {
            //    WriteErrorLog("Cache is not alive");
            //}
        }


        ///// <summary>
        ///// Save Class Object to Cache.
        ///// </summary>
        ///// <typeparam name="T">Class Type</typeparam>
        ///// <param name="cache">Cache Class Object</param>
        ///// <param name="cachename">Redis Cache Key Name</param>
        ///// <param name="t">Single Object of Class</param>
        //public static void SaveToCache<T>(CacheService cache, string cachename, T t)
        //{
        //    string serializeObj = Serialize(t);
        //    cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
        //}




        /// <summary>
        /// Update List Object Details
        /// </summary>
        /// <typeparam name="T">Class Type</typeparam>
        /// <param name="cache">Cache Class Object</param>
        /// <param name="cachename">Redis Cache Key Name</param>
        /// <param name="t">List Object of Class</param>
        public static void UpdateOrSaveCache<T>(CacheService cache, string cachename, List<T> t)
        {
            string serializeObj = Serialize(t);

            //if (cache.Exists(cachename))
            //{
            //    cache.Remove(cachename);
            //    cache.Save(cachename, serializeObj);
            //    //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            //}
            //else
            //{
            if (cache.IsAlive())
                cache.Save(cachename, serializeObj);
            //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            //}
        }

        public static void UpdateOrSaveDictionaryCache(CacheService cache, string cachename, Dictionary<string, string> t)
        {
            string serializeObj = Serialize(t);
            if (cache.IsAlive())
                cache.Save(cachename, serializeObj);
            //if (cache.Exists(cachename))
            //{
            //    cache.Remove(cachename);
            //    cache.Save(cachename, serializeObj);
            //    //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            //}
            //else
            //{
            //    cache.Save(cachename, serializeObj);
            //    //cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
            //}
        }



        ///// <summary>
        ///// Update Single Object Details
        ///// </summary>
        ///// <typeparam name="T">Class Type</typeparam>
        ///// <param name="cache">Cache Class Object</param>
        ///// <param name="cachename">Redis Cache Key Name</param>
        ///// <param name="t">Single Object of Class</param>
        //public static void UpdateOrSaveCache<T>(CacheService cache, string cachename, T t)
        //{
        //    string serializeObj = Serialize(t);

        //    if (cache.Exists(cachename))
        //    {
        //        cache.Remove(cachename);
        //        cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
        //    }
        //    else
        //    {
        //        cache.Save(cachename, serializeObj, new TimeSpan(23, 55, 0));
        //    }
        //}

        public static void SaveToCacheHash(CacheService cache, string cachename, HashEntry[] t)
        {
            string serializeObj = Serialize(t);
            if (cache.IsAlive())
                cache.SaveHash(cachename, t);
        }


        #endregion


        #region "Remove All Keys"
        public static void RemoveAllContainerKeys()
        {

            try
            {
                var cache = new CacheService();

                if (cache.IsAlive())
                {

                    cache.RemoveAllContainerKeys();

                }
            }
            catch (Exception e)
            {


            }
        }



        #endregion "End Remove All Keys"


        #region "Remove All Keys"
        public static void RemoveAllContainerKeys(string KeyStart)
        {

            try
            {
                var cache = new CacheService();

                if (cache.IsAlive())
                {

                    cache.RemoveAllContainerKeys(KeyStart);

                }
            }
            catch (Exception e)
            {


            }
        }



        #endregion "End Remove All Keys"




        #region supports

        private static string Serialize(object o)
        {
            string s = string.Empty;

            s = JsonConvert.SerializeObject(o);

            return s;
        }

        #endregion

        public static void WriteErrorLog(string Ex)
        {
            StreamWriter sw = null;

            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\CacheLogFile.txt", true);
                sw.WriteLine(DateTime.Now + " : " + Ex.Trim());
                sw.Flush();
                sw.Close();

            }
            catch (Exception ex)
            {


            }
        }

        //public static HashEntry[] RetrieveHashCacheByCacheName(string cachename, CacheService cache)
        //{



        //    var deserializeObj = cache.HashGetAll(cachename);



        //    return deserializeObj;
        //}
    }
}
