﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_Redis.Helpers
{
    public static class Configurations
    {
        public static string CacheEndpoint { get; set; }
        public static string CachePassword { get; set; }
        public static Int32 CacheSyncTimeout { get; set; }
        public static Int32 CacheConnectTimeout { get; set; }

        public static string ContestCacheEndpoint { get; set; }
        public static string ContestCachePassword { get; set; }

        public static string LeaderboardCacheEndpoint { get; set; }
        public static string LeaderboardCachePassword { get; set; }

        public static string NewCacheEndpoint { get; set; }
        public static string NewCachePassword { get; set; }
       

    }
}
