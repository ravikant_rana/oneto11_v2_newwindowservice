﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_Redis.Helpers.Enums
{
    public enum CacheObjectKeys
    {
        Announcement,
        Banners,
        Game,
        Contest,
        Matches,
        Analytics,
        GameRules,
        GamePointsRules,
        TeamPlayer,
        LiveMatch,
        League,
        User,
        GameType,
        QuizTopUser,
        Quiz,
        QuizResponse,
        QuizLifeLine,
        Network,
        Refferal,
        OurHeroes,
        ResponseMessage
    }
}
