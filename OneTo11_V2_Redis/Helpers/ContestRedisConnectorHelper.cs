﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_Redis.Helpers
{
    public interface IContestCacheService
    {
        bool IsAlive();
        bool Exists(string key);
        void Save(string key, string value, TimeSpan? expiry = null);
        void Save(string key, int value, TimeSpan? expiry = null);
        string Get(string key);
        List<string> Get(List<string> keys);
        void Remove(string key);
        bool DeleteHash(string key, RedisValue value, TimeSpan? expiry = null);
        Task<bool> DeleteHashAsync(string key, RedisValue value, TimeSpan? expiry = null);
        bool FlushDatabase();

    }

    public class ContestCacheService : IContestCacheService, IDisposable
    {
        private readonly IContestRedisCache _cache;
        private readonly TimeSpan _duration = TimeSpan.FromMilliseconds(5000);

        public ContestCacheService(int CasheId = 0)
        {
            var cacheEndpoint = Configurations.ContestCacheEndpoint;
            var cachePassword = Configurations.ContestCachePassword;
            var cacheRetry = 3;// 5;
            _cache = new ContestRedisCache(cacheEndpoint, cachePassword, cacheRetry, CasheId);
        }

        public bool IsAlive()
        {
            return _cache != null && _cache.IsAlive();
        }

        public void RunScript(string script)
        {
            _cache.DBInstance.ScriptEvaluate(script);
        }

        public bool Exists(string key)
        {
            return _cache.DBInstance.KeyExists(key);
        }

        public void Save(string key, string value, TimeSpan? expiry = null)
        {
            _cache.DBInstance.StringSetAsync(key, value, expiry);
        }

        public void SaveWithoutAsync(string key, string value, TimeSpan? expiry = null)
        {
            _cache.DBInstance.StringSet(key, value, expiry);
        }

        public void Save(string key, int value, TimeSpan? expiry = null)
        {
            _cache.DBInstance.StringSetAsync(key, value.ToString(), expiry);
        }

        public string Get(string key)
        {
            return _cache.DBInstance.StringGet(key);
        }
        public async Task<string> GetAsync(string key)
        {
            return await _cache.DBInstance.StringGetAsync(key);
        }

        public List<string> Get(List<string> keys)
        {
            List<RedisKey> hashedKeys = new List<RedisKey>();

            if (keys == null || keys.Count == 0)
                return null;

            foreach (string key in keys)
                hashedKeys.Add(key);

            return Get(hashedKeys);
        }

        public void Remove(string key)
        {
            _cache.DBInstance.KeyDelete(key);
        }

        public bool DeleteHash(string key, RedisValue value, TimeSpan? expiry = null)
        {
            return _cache.DBInstance.HashDelete(key, value);
        }

        public async Task<bool> DeleteHashAsync(string key, RedisValue value, TimeSpan? expiry = null)
        {
            return await _cache.DBInstance.HashDeleteAsync(key, value);
        }

        public bool FlushDatabase()
        {
            var cacheEndpoint = Configurations.CacheEndpoint;
            var cachePassword = Configurations.CachePassword;

            using (var redisConnection = ConnectionMultiplexer.Connect(cacheEndpoint + ",abortConnect=false,ssl=true,password=" + cachePassword))
            {
                var server = redisConnection.GetServer(cacheEndpoint);

                if (server != null)
                {

                    server.FlushDatabaseAsync();

                }
            }

            return true;
        }

        public RedisValue[] HashGet(string key, RedisValue[] values)
        {
            return _cache.DBInstance.HashGet(key, values);
        }

        public RedisValue SingleHashGet(string key, RedisValue values)
        {
            return _cache.DBInstance.HashGet(key, values);
        }

        public void SaveHash(string key, HashEntry[] value, TimeSpan? expiry = null)
        {
            _cache.DBInstance.HashSet(key, value);
        }

        #region Private Methods

        private List<string> Get(List<RedisKey> hashedRedisKeys)
        {
            var values = _cache.DBInstance.StringGet(hashedRedisKeys.ToArray()).Select(rv => rv.ToString()).ToList();

            // decrypt values
            int valuesCount = values.Count();
            for (int i = 0; i < valuesCount; i++)
                values[i] = values[i];

            return values;
        }

        //public HashEntry[] HashGetAll(string key)
        //{
        //    return _cache.DBInstance.HashGetAll(key);
        //}

        //public RedisValue HashGetValue(string key, RedisValue redisValues)
        //{
        //    return _cache.DBInstance.HashGet(key, redisValues);
        //}

        #endregion



        #region "Remove All Keys"
        public void RemoveAllContainerKeys()
        {
            Task.Run(() =>
            {
                var endpoints = _cache.Conn.GetEndPoints();
                var server = _cache.Conn.GetServer(endpoints[0]);
                //FlushDatabase didn't work for me: got error admin mode not enabled error
                //server.FlushDatabase();

                if (server != null)
                {

                    server.FlushDatabaseAsync();

                }
            });
        }


        #endregion

        #region "Remove All Keys"
        public void RemoveAllContainerKeys(string Keystart)
        {

            var endpoints = _cache.Conn.GetEndPoints();
            var server = _cache.Conn.GetServer(endpoints[0]);
            //FlushDatabase didn't work for me: got error admin mode not enabled error
            //server.FlushDatabase();

            if (server != null)
            {

                var keys = server.Keys();
                foreach (var key in keys)
                {
                    if (key.ToString().Contains(Keystart))
                        _cache.DBInstance.KeyDelete(key);
                }

            }

        }


        #endregion

        public void Dispose()
        { }
    }
}
