﻿using OneTo11_V2_MoveToArchive.Helpers;
using OneTo11_V2_MoveToArchive.Models;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace OneTo11_V2_MoveToArchive
{
    public partial class MoveToArchiveService : ServiceBase
    {
        private const string exchangeName = "HelloWorld_RabbitMQ";
        private const string queueName = "HelloQueue";
        private Timer timer1 = null;
        public MoveToArchiveService()
        {
            InitializeComponent();
        }

        public void StartMoveToArchiveService()
        {
            //OnStart(null);
            Execute(null, null);
            //System.Threading.Thread.Sleep(1000000000);
        }

        protected override void OnStart(string[] args)
        {
            timer1 = new Timer();
            this.timer1.Interval = 1000;  //for 1  second
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.Execute);
            timer1.Enabled = true;
        }

        public void Execute(object sender, ElapsedEventArgs e)
        {
            Library.WriteErrorLog("Execute started at: " + DateTime.Now);
            //timer1.Enabled = false;

            GetConnectionFactory();

            //timer1.Enabled = true;
            Library.WriteErrorLog("Excute Completed at: " + DateTime.Now);
        }

        protected override void OnStop()
        {
        }

        public void GetConnectionFactory()
        {

            ConnectionFactory connectionFactory = new ConnectionFactory
            {

                HostName = ConfigurationManager.AppSettings["MQHostName"],
                UserName = ConfigurationManager.AppSettings["MQUserName"],
                Password = ConfigurationManager.AppSettings["MQPassword"],
                Port = Convert.ToInt32(ConfigurationManager.AppSettings["MQPort"]),
                VirtualHost = ConfigurationManager.AppSettings["MQVirtualHost"]
            };

            var Rconnection = connectionFactory.CreateConnection();

            var Rchannel = Rconnection.CreateModel();

            // accept only one unack-ed message at a time

            // uint prefetchSize, ushort prefetchCount, bool global

            Rchannel.BasicQos(0, 1, false);


            ArchiveMessageReceiver archiveMessageReceiver = new ArchiveMessageReceiver(Rchannel);
            Rchannel.BasicConsume("oneto11-queue-MoveToArchiveSpark", false, archiveMessageReceiver);

            //JsonMoveToArchive jsonMoveToArchive = new JsonMoveToArchive();
            //jsonMoveToArchive.match_id = 3444;
            //archiveMessageReceiver.GetIplMatchContestCashbackUser(jsonMoveToArchive);
            //DataCleanUp dataCleanUp = new DataCleanUp();
            //JsonMoveToArchive jsonMoveToLive = new JsonMoveToArchive();
            //jsonMoveToLive.match_id = 3461;
            ////jsonMoveToLive.status_id = 2;
            ////jsonMoveToLive.status_str = "Completed";
            //////archiveMessageReceiver.SaveUserWinningAmount(jsonMoveToLive);
            //dataCleanUp.DeleteUnnecessaryMatchFromLive(jsonMoveToLive);

            //MoveToArchiveReceiver moveToArchiveReceiver = new MoveToArchiveReceiver(Rchannel);
            //moveToArchiveReceiver.CompareContestUserTeamToArchive(2060);

            Library.WriteErrorLog("Execute Completed at: " + DateTime.Now);

            System.Threading.Thread.Sleep(86400000); // For 24 hours
        }
    }
}
