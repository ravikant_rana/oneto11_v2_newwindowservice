﻿using MongoDB.Driver;
using Newtonsoft.Json;
using OneTo11_V2_MoveToArchive.Helpers;
using OneTo11_V2_MoveToArchive.Models;
using OneTo11_V2_MoveToArchive.QueryExtensions;
using OneTo11_V2_Redis;
using OneTo11_V2_Redis.Helpers;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToArchive
{
    public partial class MoveToArchiveReceiver : DefaultBasicConsumer
    {
        private DBAccess authDBAccess;
        private DBAccess dBAccess;
        private DBAccess transactionDBAccess;
        private readonly IModel _channel;
        string liveconnection = ConfigurationManager.AppSettings["LiveMongoConnection"]; //"mongodb://oneto11admin:Intigate123@104.211.183.244:27017/oneto11live?authSource=admin";
        string connection = ConfigurationManager.AppSettings["ArchiveMongoConnection"]; //"mongodb+srv://oneto11rootuser:Wve8BYfHtexpmOSG@cluster1.t98az.mongodb.net/test";

        string connectionGameString = ConfigurationManager.AppSettings["OneTo11_Game_Connection"];
        string connectionAuthString = ConfigurationManager.AppSettings["OneTo11_Auth_Connection"];
        string connectionTransactionString = ConfigurationManager.AppSettings["OneTo11_Transaction_Connection"];

        MongoClient livedbClient;
        MongoClient dbClient;
        public MoveToArchiveReceiver(IModel channel)
        {
            _channel = channel;
            livedbClient = new MongoClient(liveconnection);
            dbClient = new MongoClient(connection);
            authDBAccess = new DBAccess(connectionAuthString);
            dBAccess = new DBAccess(connectionGameString);
            transactionDBAccess = new DBAccess(connectionTransactionString);
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            Library.WriteErrorLog("Archive receiver started");
            JsonMoveToArchive jsonMoveToArchive = new JsonMoveToArchive();

            string jsonData = Encoding.UTF8.GetString(body);
            jsonMoveToArchive = JsonConvert.DeserializeObject<JsonMoveToArchive>(jsonData);

            SaveMatchesToArchive(jsonMoveToArchive);
            SaveUserTeamToArchive(jsonMoveToArchive);
            SaveContestToArchive(jsonMoveToArchive);
            SaveContestUserTeamToArchive(jsonMoveToArchive);
            SaveMatchContestPrizeBreakUpToArchive(jsonMoveToArchive);

            if (jsonMoveToArchive.status_id == 2)
            {
                GetCompletedMatchTransaction(jsonMoveToArchive);
            }

            //SaveMatchScoreCardToArchive(jsonMoveToArchive);

            DeleteMatchContestNotFilled(jsonMoveToArchive);
            //SaveAdminNotificationRequest saveAdminNotificationRequest = new SaveAdminNotificationRequest();
            //saveAdminNotificationRequest.MatchId = jsonMoveToArchive.match_id;
            //if (jsonMoveToArchive.status_id == 2)
            //{
            //    saveAdminNotificationRequest.AdminNotificationTypeId = 1; /* Match completed */
            //    SaveAdminNotification(saveAdminNotificationRequest);
            //    AddWinningAmountToWallet(jsonMoveToArchive.match_id);
            //}
            //else if (jsonMoveToArchive.status_id == 4)
            //{
            //    saveAdminNotificationRequest.AdminNotificationTypeId = 4; /* Match cancelled or abandoned */
            //    SaveAdminNotification(saveAdminNotificationRequest);
            //}

            Library.WriteErrorLog("Archive receiver completed");

            _channel.BasicAck(deliveryTag, false);



        }

        public void Test(JsonMoveToArchive jsonMoveToArchive)
        {
            //SaveMatchesToArchive(jsonMoveToArchive);
            //SaveUserTeamToArchive(jsonMoveToArchive);
            //SaveContestToArchive(jsonMoveToArchive);
            SaveContestUserTeamToArchive(jsonMoveToArchive);
            SaveMatchContestPrizeBreakUpToArchive(jsonMoveToArchive);

            if (jsonMoveToArchive.status_id == 2)
            {
                GetCompletedMatchTransaction(jsonMoveToArchive);
            }

            //SaveMatchScoreCardToArchive(jsonMoveToArchive);

            //DeleteMatchContestNotFilled(jsonMoveToArchive);
            //SaveAdminNotificationRequest saveAdminNotificationRequest = new SaveAdminNotificationRequest();
            //saveAdminNotificationRequest.MatchId = jsonMoveToArchive.match_id;
            //if (jsonMoveToArchive.status_id == 2)
            //{
            //    saveAdminNotificationRequest.AdminNotificationTypeId = 1; /* Match completed */
            //    SaveAdminNotification(saveAdminNotificationRequest);
            //    AddWinningAmountToWallet(jsonMoveToArchive.match_id);
            //}
            //else if (jsonMoveToArchive.status_id == 4)
            //{
            //    saveAdminNotificationRequest.AdminNotificationTypeId = 4; /* Match cancelled or abandoned */
            //    SaveAdminNotification(saveAdminNotificationRequest);
            //}
        }

        public void SaveMatchesToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();

                var cache = new CacheService();
                string livematchescache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatches"];
                string livematchinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchInfo"];  //"Test_LiveMatchInfo";
                string livematchplayerinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(objMoveToArchive.match_id);


                //var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                //var liveMatchCollection = database.GetCollection<LiveMatches>("Matches");
                var archiveMatchCollection = databaseArchive.GetCollection<LiveMatches>("ArchiveMatches");


                List<LiveMatchInfo> liveMatchInfo = new List<LiveMatchInfo>();
                List<TeamPlayer> teamPlayerList = new List<TeamPlayer>();

                var liveMatchInfoCache = IntigateCache.RetrieveCacheByCacheName<LiveMatchInfo>(livematchinfocache, cache);
                liveMatchInfo = liveMatchInfoCache.Where(x => x.dbId == objMoveToArchive.match_id).ToList();

                var liveMatchCache = IntigateCache.RetrieveCacheByCacheName<LiveMatchCache>(livematchinfocache, cache);

                if (liveMatchInfo.Count > 0)
                {
                    LiveMatchInfo lmi = liveMatchInfo.FirstOrDefault();
                    LiveMatches liveMatch = new LiveMatches();
                    liveMatch.dbId = lmi.dbId;
                    liveMatch.cid = lmi.cid;
                    liveMatch.match_id = lmi.match_id;
                    liveMatch.title = lmi.title;
                    liveMatch.formatId = lmi.formatId;
                    liveMatch.format_str = lmi.format_str;
                    //liveMatch.status = lmi.status;
                    //liveMatch.status_str = lmi.status_str;
                    if (objMoveToArchive.status_id == 2)
                    {
                        liveMatch.status = 5;
                        liveMatch.status_str = "Result peding";
                    }
                    else
                    {
                        liveMatch.status = objMoveToArchive.status_id;
                        liveMatch.status_str = objMoveToArchive.status_str;
                    }
                    liveMatch.status_note = lmi.status_note;
                    liveMatch.TeamA_Id = lmi.TeamA_Id;
                    liveMatch.TeamA = lmi.TeamA;
                    liveMatch.TeamA_Logo = lmi.TeamA_Logo;
                    liveMatch.TeamB_Id = lmi.TeamB_Id;
                    liveMatch.TeamB = lmi.TeamB;
                    liveMatch.TeamB_Logo = lmi.TeamB_Logo;
                    liveMatch.ContestName = lmi.ContestName;
                    liveMatch.TeamA_score = lmi.TeamA_score;
                    liveMatch.TeamA_fullscore = lmi.TeamA_fullscore;
                    liveMatch.TeamB_score = lmi.TeamB_score;
                    liveMatch.TeamB_fullscore = lmi.TeamB_fullscore;
                    liveMatch.TeamA_overs = lmi.TeamA_overs;
                    liveMatch.TeamB_overs = lmi.TeamB_overs;
                    liveMatch.GameTypeId = lmi.GameTypeId;
                    liveMatch.MatchStartTime = lmi.MatchStartTime;
                    liveMatch.MatchEndTime = lmi.MatchEndTime;
                    liveMatch.seriesname = lmi.seriesname;

                    teamPlayerList = IntigateCache.RetrieveCacheByCacheName<TeamPlayer>(livematchplayerinfocache, cache);
                    teamPlayerList = teamPlayerList.Where(x => x.MatchId == objMoveToArchive.match_id).ToList();

                    if (teamPlayerList.Count > 0)
                    {
                        liveMatch.TeamPlayer = teamPlayerList.ToList();
                    }

                    archiveMatchCollection.InsertOne(liveMatch);

                    try
                    {
                        //UpdateMatchContestStatus(Convert.ToInt64(liveMatch.dbId), null, Convert.ToInt32(liveMatch.status), Convert.ToString(liveMatch.status_str), "MATCHSTATUS");
                    }
                    catch (Exception ex)
                    {
                        Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
                    }

                    ///* Delete from live match cache */
                    //liveMatchCache = liveMatchCache.Where(x => x.dbId != objMoveToArchive.match_id).ToList();
                    //IntigateCache.SaveToCache<LiveMatchCache>(cache, livematchescache, liveMatchCache);

                    ///* Delete from live match info cache */
                    //liveMatchInfoCache = liveMatchInfoCache.Where(x => x.dbId != objMoveToArchive.match_id).ToList();
                    //IntigateCache.SaveToCache<LiveMatchInfo>(cache, livematchinfocache, liveMatchInfoCache);

                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveUserTeamToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                var cache = new CacheService();
                string livematchplayerinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(objMoveToArchive.match_id);

                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var liveUserTeamPointCollection = database.GetCollection<UserTeamPoints>("UserTeamPoints");

                var liveUserTeamPlayerCollection = database.GetCollection<UserTeamPlayersLive>("UserTeamPlayers");
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var archiveUserTeamCollection = databaseArchive.GetCollection<UserTeams>("ArchiveUserTeam");

                var filter = Builders<UserTeamPoints>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                var _userTeamPointsList = liveUserTeamPointCollection.Find(filter).ToList();
                List<TeamPlayer> teamPlayerList = new List<TeamPlayer>();
                teamPlayerList = IntigateCache.RetrieveCacheByCacheName<TeamPlayer>(livematchplayerinfocache, cache);
                var teamPlayerListFilter = teamPlayerList.Where(x => x.MatchId == objMoveToArchive.match_id).ToList();
                List<UserTeams> userTeamsList = new List<UserTeams>();
                foreach (var utp in _userTeamPointsList)
                {
                    UserTeams ut = new UserTeams();
                    ut.dbId = utp.UserTeamId;
                    ut.UserId = utp.UserId;
                    ut.MatchId = utp.MatchId;
                    ut.TeamAbbr = utp.TeamAbbr;
                    ut.FantacyPoints = utp.FantacyPoints;
                    ut.UserName = utp.TeamAbbr;
                    ut.ProfileImage = "";
                    ut.ProfileAvtarId = utp.UserAvatarId;

                    var utpfilter = Builders<UserTeamPlayersLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    utpfilter = utpfilter & Builders<UserTeamPlayersLive>.Filter.Eq("UserTeamId", utp.UserTeamId);
                    var _userTeamPlayersList = liveUserTeamPlayerCollection.Find(utpfilter).ToList();
                    if (_userTeamPlayersList.Count > 0)
                    {
                        List<UserTeamPlayers> userTeamPlayersList = new List<UserTeamPlayers>();
                        foreach (var utpl in _userTeamPlayersList)
                        {
                            TeamPlayer teamPlayer = teamPlayerListFilter.Where(y => y.MatchTeamPlayerId == utpl.MatchTeamPlayerId).FirstOrDefault();
                            UserTeamPlayers userTeamPlayers = new UserTeamPlayers();
                            userTeamPlayers.UserTeamPlayerId = utpl.UserTeamPlayerId;
                            userTeamPlayers.UserTeamId = utpl.UserTeamId;
                            userTeamPlayers.MatchTeamPlayerId = utpl.MatchTeamPlayerId;
                            userTeamPlayers.FantacyPoints = utpl.FantacyPoints;
                            userTeamPlayers.IsCaptain = utpl.IsCaptain;
                            userTeamPlayers.IsViceCaptain = utpl.IsViceCaptain;
                            if (teamPlayer != null)
                            {
                                userTeamPlayers.Name = teamPlayer.Name;
                                userTeamPlayers.TeamId = teamPlayer.TeamId;
                                userTeamPlayers.TeamName = teamPlayer.TeamName;
                                userTeamPlayers.Team_Abbr = teamPlayer.Team_Abbr;
                                userTeamPlayers.Score = null;
                                userTeamPlayers.Credit = teamPlayer.Credit;
                                userTeamPlayers.PlayerImage = teamPlayer.PlayerImage;
                                userTeamPlayers.PlayerType = teamPlayer.Playing_Role;
                                userTeamPlayers.selectdBy = teamPlayer.SelectedBy;
                                userTeamPlayers.selectdAsCaption = teamPlayer.CaptainBy;
                                userTeamPlayers.selectdViceCaption = teamPlayer.ViceCaptainBy;
                                userTeamPlayers.is_playing = teamPlayer.IsAnnounced ?? false;
                            }
                            userTeamPlayersList.Add(userTeamPlayers);
                        }
                        ut.players = userTeamPlayersList;

                        userTeamsList.Add(ut);


                    }
                }

                if (userTeamsList.Count > 0)
                {

                    archiveUserTeamCollection.InsertMany(userTeamsList);
                    //liveUserTeamPointCollection.DeleteMany(filter);
                }

                //long totNum = 0;
                ////var dbList = dbClient.ListDatabases().ToList();
                //var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                //var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                //var liveUserTeamCollection = database.GetCollection<UserTeams>("UserTeam");
                //var archiveUserTeamCollection = databaseArchive.GetCollection<UserTeams>("ArchiveUserTeam");

                //var filter = Builders<UserTeams>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                ////var _userTeamList = liveUserTeamCollection.Find(filter).ToList();
                //totNum = liveUserTeamCollection.Count(filter);
                //if (totNum > 0)
                //{
                //    for (int _i = 0; _i < totNum / 1000 + 1; _i++)
                //    {
                //        var _userTeamList = liveUserTeamCollection.Find(filter).Limit(1000).Skip(_i * 1000).ToList();
                //        if (_userTeamList.Count > 0)
                //        {

                //            archiveUserTeamCollection.InsertMany(_userTeamList);
                //        }
                //    }
                //    liveUserTeamCollection.DeleteMany(filter);
                //}

            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveContestToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                var cache = new CacheService();
                string livematchcontest = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContest"] + Convert.ToString(objMoveToArchive.match_id);
                //var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                //var liveContestCollection = database.GetCollection<MatchContest>("Contest");
                var archiveContestCollection = databaseArchive.GetCollection<MatchContest>("ArchiveContest");

                var filter = Builders<MatchContest>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                var _contestList = IntigateCache.RetrieveCacheByCacheName<MatchContest>(livematchcontest, cache);


                if (_contestList.Count > 0)
                {
                    List<MatchContest> matchContestList = new List<MatchContest>();
                    foreach (var contest in _contestList)
                    {
                        MatchContest matchContest = new MatchContest();
                        matchContest.dbId = contest.dbId;
                        matchContest.MatchId = contest.MatchId;
                        matchContest.API_MatchId = contest.API_MatchId;
                        matchContest.Title = contest.Title;
                        matchContest.MaxUsers = contest.MaxUsers;
                        matchContest.WinerPercent = contest.WinerPercent;
                        matchContest.ContestInvitationCode = contest.ContestInvitationCode;
                        matchContest.TotalContestAmount = contest.TotalContestAmount;
                        matchContest.TotalTeam = contest.TotalTeam;
                        matchContest.FirstWinner = contest.FirstWinner;
                        matchContest.EntryFee = contest.EntryFee;
                        matchContest.IsConfirm = contest.IsConfirm;
                        matchContest.MultipleEntryAllowed = contest.MultipleEntryAllowed;
                        matchContest.IsPractice = contest.IsPractice;
                        if (objMoveToArchive.status_id == 2)
                        {
                            matchContest.status = 5;
                            matchContest.status_str = "Result pending";
                        }
                        else if (objMoveToArchive.status_id == 4)
                        {
                            matchContest.status = 4;
                            matchContest.status_str = "Cancelled";
                        }
                        else
                        {
                            matchContest.status = contest.status;
                            matchContest.status_str = contest.status_str;
                        }

                        matchContestList.Add(matchContest);

                        //UpdateMatchContestStatus(matchContest.MatchId, matchContest.dbId, matchContest.status, matchContest.status_str, "MATCHCONTESTSTATUS");
                    }
                    archiveContestCollection.InsertMany(matchContestList);

                    //liveContestCollection.DeleteMany(filter);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveContestUserTeamToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();p-
                var cache = new CacheService();
                string livematchcontest = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContest"] + Convert.ToString(objMoveToArchive.match_id);

                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");

                var archiveMatchCollection = databaseArchive.GetCollection<LiveMatches>("ArchiveMatches");

                var matchfilter = Builders<LiveMatches>.Filter.Eq("dbId", objMoveToArchive.match_id);
                var _matchList = archiveMatchCollection.Find(matchfilter).ToList();

                var contestCollection = databaseArchive.GetCollection<MatchContest>("ArchiveContest");
                var contestfilter = Builders<MatchContest>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                var _matchContestList = contestCollection.Find(contestfilter).ToList();


                var filter = Builders<MatchContestUserTeamsMongoLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                var _contestUserTeamList = liveContestUserTeamCollection.Find(filter).ToList();
                var distinctRank = _contestUserTeamList.Select(x => new
                {
                    x.rank,
                    x.MatchContestId
                }).Distinct().OrderByDescending(x => x.MatchContestId).ThenByDescending(x => x.rank);


                foreach (var mch in _matchContestList)
                {
                    try
                    {
                        List<MatchContestUserTeams> matchContestUserTeamsList = new List<MatchContestUserTeams>();
                        foreach (var r in distinctRank.Where(y => y.MatchContestId == mch.dbId))
                        {
                            int rankCount = _contestUserTeamList.Where(x => x.rank == r.rank && x.MatchContestId == r.MatchContestId).Count();
                            foreach (var cut in _contestUserTeamList.Where(x => x.rank == r.rank && x.MatchContestId == r.MatchContestId))
                            {
                                MatchContestUserTeams matchContestUserTeams = new MatchContestUserTeams();
                                matchContestUserTeams.MatchContestUserTeamId = cut.MatchContestUserTeamId;
                                matchContestUserTeams.UserTeamId = cut.UserTeamId;
                                matchContestUserTeams.UserId = cut.UserId;
                                matchContestUserTeams.FantacyPoints = cut.FantacyPoints;
                                matchContestUserTeams.MatchContestId = cut.MatchContestId;
                                matchContestUserTeams.MatchId = cut.MatchId;
                                matchContestUserTeams.isInWinning_Jone = objMoveToArchive.status_id == 4 ? false : cut.isInWinning_Jone;
                                //matchContestUserTeams.winning_Amount = (((cut.winning_Amount ?? 0) > 0 && (rankCount > 0)) ? (cut.winning_Amount / rankCount) : cut.winning_Amount);
                                matchContestUserTeams.winning_Amount = objMoveToArchive.status_id == 4 ? 0 : GetWinnigPrize(cut.MatchContestId, cut.rank, rankCount);
                                matchContestUserTeams.rank = cut.rank;
                                matchContestUserTeams.TeamAbbr = cut.TeamAbbr;
                                matchContestUserTeams.IsWinningAmountAdded = false;
                                matchContestUserTeamsList.Add(matchContestUserTeams);


                                //if (matchContestUserTeams.winning_Amount > 0 && objMoveToArchive.status_id == 2) /* Amount greater than 0 and status completed */
                                //{
                                //    SaveNotificationRequest saveNotificationRequest = new SaveNotificationRequest();
                                //    saveNotificationRequest.LoggedInUserId = cut.UserId;
                                //    saveNotificationRequest.NotificationTypeId = 5; /* CongratulationsYouWon */
                                //    saveNotificationRequest.Team1VsTeam2 = mch.TeamA + " Vs " + mch.TeamB;
                                //    saveNotificationRequest.Points = cut.FantacyPoints;
                                //    saveNotificationRequest.Rank = Convert.ToInt32(cut.rank);
                                //    saveNotificationRequest.Amount = matchContestUserTeams.winning_Amount;
                                //    saveNotificationRequest.Date = objMoveToArchive.date_start;
                                //    int notificationResponse = SaveNotification(saveNotificationRequest);
                                //}
                            }
                        }

                        if (matchContestUserTeamsList.Count > 0)
                        {
                            //archiveContestUserTeamCollection.InsertMany(_contestUserTeamList);
                            archiveContestUserTeamCollection.InsertMany(matchContestUserTeamsList);
                            //liveContestUserTeamCollection.DeleteMany(filter);


                        }
                    }
                    catch (Exception ex)
                    {
                        Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
                    }
                }

                /* When match status is completed */
                if (objMoveToArchive.status_id == 2)
                {
                    //SaveApproveContestResultInSqlDb(objMoveToArchive);
                }


            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }


        public void GetContestUserTeamToArchive(long MatchContestId)
        {
            try
            {
                 
                //var dbList = dbClient.ListDatabases().ToList();p-
                //var cache = new CacheService();
                //string livematchcontest = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContest"] + Convert.ToString(objMoveToArchive.match_id);

                //var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                //var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");
                var archiveContestUserTeamCollectionBackup = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam_"+MatchContestId);
                var archiveLeaguesPrizeBreakupCollection = databaseArchive.GetCollection<MatchContestPrizeBreakUpData>("ArchiveMatchContestPrizeBreakup");
                var filter = Builders<MatchContestPrizeBreakUpData>.Filter.Eq("MatchContestId", MatchContestId);
                var priceBreakupList = archiveLeaguesPrizeBreakupCollection.Find(filter).ToList();

                

                //var archiveMatchCollection = databaseArchive.GetCollection<LiveMatches>("ArchiveMatches");

                //var matchfilter = Builders<LiveMatches>.Filter.Eq("dbId", objMoveToArchive.match_id);
                //var _matchList = archiveMatchCollection.Find(matchfilter).ToList();

                //var contestCollection = databaseArchive.GetCollection<MatchContest>("ArchiveContest");
                //var contestfilter = Builders<MatchContest>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                //var _matchContestList = contestCollection.Find(contestfilter).ToList();


                var filter1 = Builders<MatchContestUserTeams>.Filter.Eq("MatchContestId", MatchContestId);
                var _contestUserTeamList = archiveContestUserTeamCollection.Find(filter1).ToList();
                if(_contestUserTeamList.Count > 0)
                {
                    archiveContestUserTeamCollectionBackup.InsertMany(_contestUserTeamList);
                }
                //var ranking = _contestUserTeamList.GroupBy(d => d.MatchContestId)
                //                  .SelectMany(g => g.OrderByDescending(y => y.FantacyPoints)
                //                     .Select((x, i) => new { points = x.FantacyPoints, rank = i + 1 })).ToList();
                //var _result = _contestUserTeamList.Select(x=> new 
                //{
                //    x.FantacyPoints
                //}).GroupBy(x=>x.FantacyPoints).OrderByDescending(x => x.FantacyPoints)

                //var priceBreakupList = contestPriceBreakupupdate.Where(x => x.MatchContestId == 181900).ToList();
                foreach (var contestTeam in _contestUserTeamList)
                {
                    if (priceBreakupList.Count > 0)
                    {
                        var ranking = _contestUserTeamList.GroupBy(d => d.MatchContestId)
                                  .SelectMany(g => g.OrderByDescending(y => y.FantacyPoints)
                                     .Select((x, i) => new { points = x.FantacyPoints, rank = i + 1 })).ToList();

                        long? rank = ranking.Where(x => x.points == contestTeam.FantacyPoints).FirstOrDefault().rank;
                        contestTeam.rank = rank;
                        contestTeam.isInWinning_Jone = priceBreakupList.Where(x => x.EndRank >= rank).ToList().Count > 0 ? true : false;
                        //contestTeam.winning_Amount = priceBreakupList.Where(x => x.EndRank >= rank).ToList().Count > 0 ? Convert.ToDecimal(priceBreakupList.Where(x => x.EndRank >= rank).FirstOrDefault().Prize) : 0;
                        //contestTeam.IsWinningAmountAdded = false;
                        ///update LeaguesUserTeam data
                        var contestUserTeamUpdateDefinition = Builders<MatchContestUserTeams>.Filter.Eq(x => x.MatchContestUserTeamId, contestTeam.MatchContestUserTeamId);
                        //var contestUserTeamUpdateFilter = Builders<MatchContestUserTeams>.Update.Set(q => q.isInWinning_Jone, contestTeam.isInWinning_Jone).Set(q => q.winning_Amount, contestTeam.winning_Amount).Set(q => q.rank, contestTeam.rank);
                        var contestUserTeamUpdateFilter = Builders<MatchContestUserTeams>.Update.Set(q => q.isInWinning_Jone, contestTeam.isInWinning_Jone).Set(q => q.rank, contestTeam.rank);
                        archiveContestUserTeamCollection.UpdateOne(contestUserTeamUpdateDefinition, contestUserTeamUpdateFilter);
                    }

                }
                //var distinctRank = _contestUserTeamList.Select(x => new
                //{
                //    x.rank,
                //    x.MatchContestId
                //}).Distinct().OrderByDescending(x => x.MatchContestId).ThenByDescending(x => x.rank);


                


            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveMatchContestPrizeBreakUpToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();
                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveLeaguesPrizeBreakupCollection = database.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("MatchContestPrizeBreakup");
                var archiveLeaguesPrizeBreakupCollection = databaseArchive.GetCollection<MatchContestPrizeBreakUpData>("ArchiveMatchContestPrizeBreakup");

                var filter = Builders<MatchContestPrizeBreakUpDataMongoLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                var _leaguePrizeBreakupList = liveLeaguesPrizeBreakupCollection.Find(filter).ToList();

                List<MatchContestPrizeBreakUpData> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpData>();
                if (_leaguePrizeBreakupList.Count > 0)
                {
                    foreach (var obj in _leaguePrizeBreakupList)
                    {
                        MatchContestPrizeBreakUpData matchContestPrizeBreakUpData = new MatchContestPrizeBreakUpData();
                        matchContestPrizeBreakUpData.dbId = obj.dbId;
                        matchContestPrizeBreakUpData.MatchId = obj.MatchId;
                        matchContestPrizeBreakUpData.MatchContestId = obj.MatchContestId;
                        matchContestPrizeBreakUpData.StartRank = obj.StartRank;
                        matchContestPrizeBreakUpData.EndRank = obj.EndRank;
                        matchContestPrizeBreakUpData.Prize = obj.Prize;
                        matchContestPrizeBreakUpDataList.Add(matchContestPrizeBreakUpData);
                    }
                    archiveLeaguesPrizeBreakupCollection.InsertMany(matchContestPrizeBreakUpDataList);
                    //liveLeaguesPrizeBreakupCollection.DeleteMany(filter);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void UpdateMatchContestStatus(long? match_id, long? contest_id, int? status_id, string status_str, string flag)
        {
            int result = 0;
            try
            {
                string[] matchParam = { "@match_id", "@contest_id", "@status_id", "@status_str", "@flag" };
                result = dBAccess.SaveDataStoredProcedure("[dbo].[PROC_UPDATE_MATCH_CONTEST_STATUS]", matchParam
                                             , match_id, contest_id, status_id, status_str, flag);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void GetCompletedMatchTransaction(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                string[] notificationParam = { "@MatchId" };
                DataTable data = dBAccess.GetDataFromStoredProcedure("[dbo].[PROC_GET_COMPLETED_MATCH_TRANSACTION]", notificationParam, objMoveToArchive.match_id);

                if (data.Rows.Count > 0)
                {
                    UpdateMatchCompletedDateInWalletTransaction(data);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void UpdateMatchCompletedDateInWalletTransaction(DataTable dataTable)
        {
            try
            {
                string[] paramName = { "@dtWalletTransaction" };
                int result = transactionDBAccess.SaveDataStoredProcedure("[dbo].[PROC_UPDATE_MATCH_COMPLETE_DATE_IN_TRANSACTION]", paramName, dataTable);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        //public void SaveMatchScoreCardToArchive(JsonMoveToArchive objMoveToArchive)
        //{
        //    try
        //    {
        //        //var dbList = dbClient.ListDatabases().ToList();
        //        var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
        //        var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
        //        var liveMatchScoreCardCollection = database.GetCollection<PlayerScorecardResponse>("MatchScoreCard");
        //        var archiveMatchScoreCardCollection = databaseArchive.GetCollection<PlayerScorecardResponse>("ArchiveMatchScoreCard");

        //        var filter = Builders<PlayerScorecardResponse>.Filter.Eq("dbmchid", objMoveToArchive.match_id);
        //        var _matchScoreCardList = liveMatchScoreCardCollection.Find(filter).ToList();


        //        if (_matchScoreCardList.Count > 0)
        //        {
        //            archiveMatchScoreCardCollection.InsertMany(_matchScoreCardList);
        //            //liveMatchScoreCardCollection.DeleteMany(filter);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
        //    }
        //}

        public void DeleteMatchContestNotFilled(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                string[] paramName = { "@MatchId" };
                int result = dBAccess.SaveDataStoredProcedure("[dbo].[PROC_DELETE_MATCHCONTESTNOTFILLED]", paramName, objMoveToArchive.match_id);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public int SaveAdminNotification(SaveAdminNotificationRequest notificationRequest)
        {
            int result = 0;
            try
            {
                string[] notificationParam = { "@MatchId", "@ContestId", "@AdminNotificationType_Fk_Id" };
                DataTable data = dBAccess.GetDataFromStoredProcedure("[dbo].[PROC_SAVE_ADMIN_NOTIFICATION_MESSAGE]", notificationParam
                                             , notificationRequest.MatchId, notificationRequest.ContestId, notificationRequest.AdminNotificationTypeId
                                             );

                if (data.Rows.Count > 0)
                {
                    result = Convert.ToInt32(data.Rows[0]["ResultCode"]);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
            return result;
        }

        public void AddWinningAmountToWallet(long match_id)
        {
            try
            {
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);

                string[] param1 = { "@MatchId" };
                DataTable data = transactionDBAccess.GetDataFromStoredProcedure("[dbo].[PROC_GET_RESULT_PENDING_CONTEST]", param1, match_id);
                if (data.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in data.Rows)
                    {
                        int result = 0;
                        string[] param2 = { "@MatchId", "@MatchContestId" };
                        result = transactionDBAccess.SaveDataStoredProcedure("[dbo].[PROC_ADD_WINNIG_AMOUNT_TO_WALLET]", param2, match_id, Convert.ToInt64(dataRow["MatchContestId"]));

                        int result1 = 0;
                        string[] param3 = { "@MatchId", "@ContestId" };
                        result1 = dBAccess.SaveDataStoredProcedure("[dbo].[ADMIN_PROC_UPDATE_APPROVAL_PROCESS_CONTEST_STATUS]", param3, match_id, Convert.ToInt64(dataRow["MatchContestId"]));

                        var archiveContestCollection = databaseArchive.GetCollection<MatchContest>("ArchiveContest");

                        var filter = Builders<MatchContest>.Filter.Eq("dbId", Convert.ToInt64(dataRow["MatchContestId"]));
                        var _contestList = archiveContestCollection.Find(filter).ToList();

                        if (_contestList.Count > 0)
                        {
                            ///update contest status 
                            var updatecontestdefinition = Builders<MatchContest>.Update
                                .Set(p => p.status, 2)
                                .Set(q => q.status_str, "Completed");
                            archiveContestCollection.UpdateOne(filter, updatecontestdefinition);
                        }

                        var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");
                        var _ContestUserTeamFilter = Builders<MatchContestUserTeams>.Filter.Eq("MatchContestId", Convert.ToInt64(dataRow["MatchContestId"]));
                        var _contestUserTeamList = archiveContestUserTeamCollection.Find(_ContestUserTeamFilter).ToList();

                        if (_contestUserTeamList.Count > 0)
                        {
                            ///update contest user team winning amount added 
                            var updatecontestuserteamdefinition = Builders<MatchContestUserTeams>.Update
                                .Set(p => p.IsWinningAmountAdded, true);
                            archiveContestUserTeamCollection.UpdateMany(_ContestUserTeamFilter, updatecontestuserteamdefinition);
                        }
                    }

                    /* This section is processed from admin */

                    //var archiveMatchCollection = databaseArchive.GetCollection<LiveMatches>("ArchiveMatches");
                    //var matchFilter = Builders<LiveMatches>.Filter.Eq("dbId", match_id);
                    //var _matchList = archiveMatchCollection.Find(matchFilter).ToList();

                    //if (_matchList.Count > 0)
                    //{
                    //    ///update match status 
                    //    var updatematchdefinition = Builders<LiveMatches>.Update
                    //        .Set(p => p.status, 2)
                    //        .Set(q => q.status_str, "Completed");
                    //    archiveMatchCollection.UpdateOne(matchFilter, updatematchdefinition);
                    //}

                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }

        }

        private decimal GetWinnigPrize(Int64 cid, Int64? rank, int rankcount)
        {
            Int64 startrank = rank ?? 0;
            Int64 endrank = startrank + rankcount - 1;
            var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
            var contestPriceBreakupcollection = database.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("MatchContestPrizeBreakup");
            //List<MatchContestUserTeams> matchContestUserTeamsList = new List<MatchContestUserTeams>();
            var contestPriceBreakupfilter = Builders<MatchContestPrizeBreakUpDataMongoLive>.Filter.Eq(x => x.MatchContestId, cid);
            var contestPriceBreakupupdate = contestPriceBreakupcollection.Find(contestPriceBreakupfilter).ToList();
            decimal amount = 0;
            for (var i = rank; i <= endrank; i++)
            {
                var obj = contestPriceBreakupupdate.Where(x => x.EndRank >= i && x.StartRank <= i).FirstOrDefault();
                if (obj != null)
                    amount = amount + (obj.Prize ?? 0);
            }
            amount = Math.Round((amount / rankcount), 2);
            return amount;
        }

        public void SaveApproveContestResultInSqlDb(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");
                var filter = Builders<MatchContestUserTeams>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                filter = filter & Builders<MatchContestUserTeams>.Filter.Eq("isInWinning_Jone", true);
                filter = filter & Builders<MatchContestUserTeams>.Filter.Where(x => x.winning_Amount > 0);
                var _matchContestUserTeamsList = archiveContestUserTeamCollection.Find(filter).ToList();
                if (_matchContestUserTeamsList.Count > 0)
                {
                    List<ApproveContestResult> approveContestResultList = new List<ApproveContestResult>();
                    foreach (var cut in _matchContestUserTeamsList)
                    {
                        ApproveContestResult approveContestResult = new ApproveContestResult();
                        approveContestResult.UserId = cut.UserId;
                        approveContestResult.MatchId = cut.MatchId;
                        approveContestResult.MatchContestId = cut.MatchContestId;
                        approveContestResult.UserTeamId = cut.UserTeamId;
                        approveContestResult.TeamName = cut.TeamAbbr;
                        approveContestResult.FantacyPoints = cut.FantacyPoints;
                        approveContestResult.WinningAmount = cut.winning_Amount;
                        approveContestResult.IsInWinning_Jone = cut.isInWinning_Jone;
                        approveContestResult.Rank = cut.rank;
                        approveContestResultList.Add(approveContestResult);
                    }

                    if (approveContestResultList.Count > 0)
                    {
                        try
                        {
                            string[] paramName = { "@dtApproveContestResult" };
                            var result = transactionDBAccess.GetDataFromStoredProcedure<ProcSaveApproveContestResult>("[dbo].[PROC_SAVE_APPROVE_CONTEST_RESULT]", paramName, DataTableExtension.ConvertListToDataTable(approveContestResultList));
                        }
                        catch (Exception ex)
                        {
                            Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }

        }

        public void CorrectionInMatchTeamPlayerInfo()
        {
            var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
            var archiveMatchCollection = databaseArchive.GetCollection<LiveMatchInfo>("ArchiveMatches");

            var filter = Builders<LiveMatchInfo>.Filter.Empty;
            //var filter = Builders<LiveMatchInfo>.Filter.Eq("dbId", match_id);
            var _matchList = archiveMatchCollection.Find(filter).ToList();
            var archiveMatchTeamPlayerCollection2 = databaseArchive.GetCollection<TeamPlayer>("ArchiveMatchTeamPlayerInfo");

            var teamfilter2 = Builders<TeamPlayer>.Filter.Empty;
            var _matchTeamPlayerList2 = archiveMatchTeamPlayerCollection2.Find(teamfilter2).ToList();
            if (_matchTeamPlayerList2.Count > 0)
            {
                var archiveMatchTeamPlayerCollection1 = databaseArchive.GetCollection<TeamPlayer>("ArchiveMatchTeamPlayerInfo22032021");
                archiveMatchTeamPlayerCollection1.InsertMany(_matchTeamPlayerList2);
            }

            foreach (var mch in _matchList)
            {
                var archiveMatchTeamPlayerCollection = databaseArchive.GetCollection<TeamPlayer>("ArchiveMatchTeamPlayerInfo");
                
                var teamfilter = Builders<TeamPlayer>.Filter.Eq("MatchId", mch.dbId);
                var _matchTeamPlayerList = archiveMatchTeamPlayerCollection.Find(teamfilter).ToList();
                string[] paramScheduleName = { "@match_id" };
                DataSet dbSet = dBAccess.GetDatasetFromStoredProcedure("[dbo].[PROC_GET_ScheluedMatchesByMatchId]", paramScheduleName, mch.dbId);
                DataTable dataPlayers = dbSet.Tables[1];
                if(_matchTeamPlayerList.Count > 0 && dataPlayers.Rows.Count > 0)
                {
                    
                    List<TeamPlayer> teamPlayerList = new List<TeamPlayer>();
                    foreach (DataRow dataRow in dataPlayers.Rows)
                    {
                        TeamPlayer teamPlayer = new TeamPlayer
                        {
                            _id = (DBNull.Value == dataRow["Id"]) ? 0 : Convert.ToInt64(dataRow["Id"]),
                            MatchTeamPlayerId = (DBNull.Value == dataRow["MatchTeamPlayerId"]) ? 0 : Convert.ToInt64(dataRow["MatchTeamPlayerId"]),
                            MatchId = (DBNull.Value == dataRow["MatchId"]) ? 0 : Convert.ToInt64(dataRow["MatchId"]),
                            TeamId = (DBNull.Value == dataRow["TeamId"]) ? 0 : Convert.ToInt64(dataRow["TeamId"]),
                            TeamName = (DBNull.Value == dataRow["TeamName"]) ? "" : Convert.ToString(dataRow["TeamName"]),
                            Team_Abbr = (DBNull.Value == dataRow["Team_Abbr"]) ? "" : Convert.ToString(dataRow["Team_Abbr"]),
                            Name = (DBNull.Value == dataRow["Name"]) ? "" : Convert.ToString(dataRow["Name"]),
                            Playing_Role = (DBNull.Value == dataRow["PlayerType"]) ? "" : Convert.ToString(dataRow["PlayerType"]),
                            SelectedBy = (DBNull.Value == dataRow["SelectedBy"]) ? 0 : Convert.ToDecimal(dataRow["SelectedBy"]),
                            CaptainBy = (DBNull.Value == dataRow["CaptainBy"]) ? 0 : Convert.ToDecimal(dataRow["CaptainBy"]),
                            ViceCaptainBy = (DBNull.Value == dataRow["ViceCaptainBy"]) ? 0 : Convert.ToDecimal(dataRow["ViceCaptainBy"]),
                            FantacyPoints = (DBNull.Value == dataRow["FantacyPoints"]) ? 0 : Convert.ToDecimal(dataRow["FantacyPoints"]),
                            PlayerImage = (DBNull.Value == dataRow["PlayerImage"]) ? "" : Convert.ToString(dataRow["PlayerImage"]),
                            IsAnnounced = (DBNull.Value == dataRow["IsAnnounced"]) ? false : Convert.ToBoolean(dataRow["IsAnnounced"]),
                            Rating = (dataRow["Credit"] != DBNull.Value) ? Convert.ToString(dataRow["Credit"]) : "0",
                            Credit = (dataRow["Credit"] != DBNull.Value) ? Convert.ToDecimal(dataRow["Credit"]) : 0,
                            starting11 = "0",
                            run = "0",
                            four = "0",
                            six = "0",
                            sr = "0",
                            fifty = "0",
                            duck = "0",
                            wkts = "0",
                            maidenover = "0",
                            er = "0",
                            _catch = "0",
                            runoutstumping = "0",
                            runoutthrower = "0",
                            runoutcatcher = "0",
                            directrunout = "0",
                            stumping = "0",
                            thirty = "0",
                            bonus = "0",


                        };

                        teamPlayerList.Add(teamPlayer);
                    }

                    if(teamPlayerList.Count > 0)
                    {
                        var teamObj = from sql in teamPlayerList
                                      join mongo in _matchTeamPlayerList on sql.MatchTeamPlayerId equals mongo.MatchTeamPlayerId
                                      into gj
                                      from mongo in gj.DefaultIfEmpty()
                                      where mongo == null
                                      select new TeamPlayer
                                      {
                                          _id = sql._id,
                                          MatchTeamPlayerId = sql.MatchTeamPlayerId,
                                          MatchId = sql.MatchId,
                                          TeamId = sql.TeamId,
                                          TeamName = sql.TeamName,
                                          Team_Abbr = sql.Team_Abbr,
                                          Name = sql.Name,
                                          Playing_Role = sql.Playing_Role,
                                          SelectedBy = sql.SelectedBy,
                                          CaptainBy = sql.CaptainBy,
                                          ViceCaptainBy = sql.ViceCaptainBy,
                                          PlayerImage = sql.PlayerImage,
                                          Rating = sql.Rating,
                                          IsAnnounced = false,
                                          FantacyPoints = sql.FantacyPoints,
                                          starting11 = sql.starting11,
                                          run = sql.run,
                                          four = sql.four,
                                          six = sql.six,
                                          sr = sql.sr,
                                          fifty = sql.fifty,
                                          duck = sql.duck,
                                          wkts = sql.wkts,
                                          maidenover = sql.maidenover,
                                          er = sql.er,
                                          _catch = sql._catch,
                                          runoutstumping = sql.runoutstumping,
                                          runoutthrower = sql.runoutthrower,
                                          runoutcatcher = sql.runoutcatcher,
                                          directrunout = sql.directrunout,
                                          stumping = sql.stumping,
                                          thirty = sql.thirty,
                                          bonus = sql.bonus,
                                          Credit = sql.Credit
                                      };

                        if(teamObj.Count() > 0)
                        {
                            Console.WriteLine("MatchId: " + mch.dbId);
                            archiveMatchTeamPlayerCollection.InsertMany(teamObj.ToList());
                        }
                    }
                }


            }


        }

        public void SaveNewMatchContestPrizeBreakUpToArchive()
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var archiveMatchCollection = databaseArchive.GetCollection<LiveMatchInfo>("ArchiveMatches");

                //var archiveLeaguesPrizeBreakupCollection = databaseArchive.GetCollection<MatchContestPrizeBreakUpData>("ArchiveMatchContestPrizeBreakup");
                var filter = Builders<LiveMatchInfo>.Filter.Empty;
                var _matchList = archiveMatchCollection.Find(filter).ToList();

                var collection = databaseArchive.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("ArchiveMatchContestPrizeBreakup");
                var filter1 = Builders<MatchContestPrizeBreakUpDataMongoLive>.Filter.Empty;

                var _prizeList = collection.Find(filter1).ToList();
                if (_prizeList.Count > 0)
                {
                    //var collection1 = databaseArchive.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("ArchiveMatchContestPrizeBreakup10041988");
                    //collection1.InsertMany(_prizeList);
                    collection.DeleteMany(filter1);
                }
                var collection2 = databaseArchive.GetCollection<MatchContestPrizeBreakUpData>("ArchiveMatchContestPrizeBreakup");
                foreach (var item in _matchList)
                {
                    string[] paramScheduleName = { "@match_id" };
                    var _prizeBreakup = dBAccess.GetDataFromStoredProcedure<MatchContestPrizeBreakUpData>("[dbo].[PROC_GET_LiveResizeMatchContestPrizeBreakup]", paramScheduleName, item.dbId).ToList();

                    if (_prizeBreakup.Count > 0)
                    {
                        List<MatchContestPrizeBreakUpData> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpData>();
                        

                        foreach (var obj in _prizeBreakup)
                        {
                            MatchContestPrizeBreakUpData matchContestPrizeBreakUpData = new MatchContestPrizeBreakUpData();
                            matchContestPrizeBreakUpData._id = obj.dbId;
                            matchContestPrizeBreakUpData.dbId = obj.dbId;
                            matchContestPrizeBreakUpData.MatchId = obj.MatchId;
                            matchContestPrizeBreakUpData.MatchContestId = obj.MatchContestId;
                            matchContestPrizeBreakUpData.StartRank = obj.StartRank;
                            matchContestPrizeBreakUpData.EndRank = obj.EndRank;
                            matchContestPrizeBreakUpData.Prize = obj.Prize;
                            matchContestPrizeBreakUpDataList.Add(matchContestPrizeBreakUpData);
                        }
                        collection2.InsertMany(matchContestPrizeBreakUpDataList);

                    }

                    //List<MatchContestPrizeBreakUpData> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpData>();
                    //if (_leaguePrizeBreakupList.Count > 0)
                    //{
                    //    foreach (var obj in _leaguePrizeBreakupList)
                    //    {
                    //        MatchContestPrizeBreakUpData matchContestPrizeBreakUpData = new MatchContestPrizeBreakUpData();
                    //        matchContestPrizeBreakUpData.dbId = obj.dbId;
                    //        matchContestPrizeBreakUpData.MatchId = obj.MatchId;
                    //        matchContestPrizeBreakUpData.MatchContestId = obj.MatchContestId;
                    //        matchContestPrizeBreakUpData.StartRank = obj.StartRank;
                    //        matchContestPrizeBreakUpData.EndRank = obj.EndRank;
                    //        matchContestPrizeBreakUpData.Prize = obj.Prize;
                    //        matchContestPrizeBreakUpDataList.Add(matchContestPrizeBreakUpData);
                    //    }
                    //    archiveLeaguesPrizeBreakupCollection.InsertMany(matchContestPrizeBreakUpDataList);
                    //    //liveLeaguesPrizeBreakupCollection.DeleteMany(filter);
                    //}
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }


        public void SaveNewLiveMatchContestPrizeBreakUpToArchive(long matchid)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                
                var collection = databaseArchive.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("MatchContestPrizeBreakup");
                var filter1 = Builders<MatchContestPrizeBreakUpDataMongoLive>.Filter.Eq("MatchId",matchid);

                var _prizeList = collection.Find(filter1).ToList();
                if (_prizeList.Count > 0)
                {
                    //var collection1 = databaseArchive.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("ArchiveMatchContestPrizeBreakup10041988");
                    //collection1.InsertMany(_prizeList);
                    collection.DeleteMany(filter1);

                    string[] paramScheduleName = { "@match_id" };
                    var _prizeBreakup = dBAccess.GetDataFromStoredProcedure<MatchContestPrizeBreakUpDataMongoLive>("[dbo].[PROC_GET_LiveResizeMatchContestPrizeBreakup]", paramScheduleName, matchid).ToList();

                    if (_prizeBreakup.Count > 0)
                    {
                        List<MatchContestPrizeBreakUpDataMongoLive> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpDataMongoLive>();


                        foreach (var obj in _prizeBreakup)
                        {
                            MatchContestPrizeBreakUpDataMongoLive matchContestPrizeBreakUpData = new MatchContestPrizeBreakUpDataMongoLive();
                            matchContestPrizeBreakUpData._id = obj.dbId;
                            matchContestPrizeBreakUpData.dbId = obj.dbId;
                            matchContestPrizeBreakUpData.MatchId = obj.MatchId;
                            matchContestPrizeBreakUpData.MatchContestId = obj.MatchContestId;
                            matchContestPrizeBreakUpData.StartRank = obj.StartRank;
                            matchContestPrizeBreakUpData.EndRank = obj.EndRank;
                            matchContestPrizeBreakUpData.Prize = obj.Prize;
                            matchContestPrizeBreakUpData.TotalPrize = obj.TotalPrize;
                            matchContestPrizeBreakUpDataList.Add(matchContestPrizeBreakUpData);
                        }
                        collection.InsertMany(matchContestPrizeBreakUpDataList);

                    }



                }
                //var collection2 = databaseArchive.GetCollection<MatchContestPrizeBreakUpData>("ArchiveMatchContestPrizeBreakup");
                //foreach (var item in _matchList)
                //{
                //    string[] paramScheduleName = { "@match_id" };
                //    var _prizeBreakup = dBAccess.GetDataFromStoredProcedure<MatchContestPrizeBreakUpData>("[dbo].[PROC_GET_LiveResizeMatchContestPrizeBreakup]", paramScheduleName, item.dbId).ToList();

                //    if (_prizeBreakup.Count > 0)
                //    {
                //        List<MatchContestPrizeBreakUpData> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpData>();


                //        foreach (var obj in _prizeBreakup)
                //        {
                //            MatchContestPrizeBreakUpData matchContestPrizeBreakUpData = new MatchContestPrizeBreakUpData();
                //            matchContestPrizeBreakUpData._id = obj.dbId;
                //            matchContestPrizeBreakUpData.dbId = obj.dbId;
                //            matchContestPrizeBreakUpData.MatchId = obj.MatchId;
                //            matchContestPrizeBreakUpData.MatchContestId = obj.MatchContestId;
                //            matchContestPrizeBreakUpData.StartRank = obj.StartRank;
                //            matchContestPrizeBreakUpData.EndRank = obj.EndRank;
                //            matchContestPrizeBreakUpData.Prize = obj.Prize;
                //            matchContestPrizeBreakUpDataList.Add(matchContestPrizeBreakUpData);
                //        }
                //        collection2.InsertMany(matchContestPrizeBreakUpDataList);

                //    }

                //    //List<MatchContestPrizeBreakUpData> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpData>();
                //    //if (_leaguePrizeBreakupList.Count > 0)
                //    //{
                //    //    foreach (var obj in _leaguePrizeBreakupList)
                //    //    {
                //    //        MatchContestPrizeBreakUpData matchContestPrizeBreakUpData = new MatchContestPrizeBreakUpData();
                //    //        matchContestPrizeBreakUpData.dbId = obj.dbId;
                //    //        matchContestPrizeBreakUpData.MatchId = obj.MatchId;
                //    //        matchContestPrizeBreakUpData.MatchContestId = obj.MatchContestId;
                //    //        matchContestPrizeBreakUpData.StartRank = obj.StartRank;
                //    //        matchContestPrizeBreakUpData.EndRank = obj.EndRank;
                //    //        matchContestPrizeBreakUpData.Prize = obj.Prize;
                //    //        matchContestPrizeBreakUpDataList.Add(matchContestPrizeBreakUpData);
                //    //    }
                //    //    archiveLeaguesPrizeBreakupCollection.InsertMany(matchContestPrizeBreakUpDataList);
                //    //    //liveLeaguesPrizeBreakupCollection.DeleteMany(filter);
                //    //}
                //}
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void CompareContestUserTeamToArchive(long match_id)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();p-
               
                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");

                var filter = Builders<MatchContestUserTeamsMongoLive>.Filter.Eq("MatchId", match_id);
                var _contestUserTeamList = liveContestUserTeamCollection.Find(filter).ToList();

                var archivefilter = Builders<MatchContestUserTeams>.Filter.Eq("MatchId", match_id);
                var _archiveContestUserTeamList = archiveContestUserTeamCollection.Find(archivefilter).ToList();

                if(_contestUserTeamList.Count > 0 && _archiveContestUserTeamList.Count > 0)
                {
                    var _notExistList = _contestUserTeamList.Where(p => _archiveContestUserTeamList.All(p2 => p2._id != p._id)).ToList();
                }



            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }


    }
}
