﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToArchive.Models
{
    public class MatchContest
    {
        public MatchContest()
        {
            //user = new List<MatchLeagueUserTeams>();
        }
        public Int64 _id { get; set; }
        public long dbId { get; set; }
        public long? MatchId { get; set; }
        public string API_MatchId { get; set; }
        public string Title { get; set; }
        public Int32? MaxUsers { get; set; }
        public decimal? WinerPercent { get; set; }
        public string ContestInvitationCode { get; set; }
        public decimal? TotalContestAmount { get; set; }
        public long TotalTeam { get; set; }
        public long? FirstWinner { get; set; }
        public decimal? EntryFee { get; set; }
        public bool IsConfirm { get; set; }
        public bool MultipleEntryAllowed { get; set; }
        public bool IsPractice { get; set; }
        public int? status { get; set; }
        public string status_str { get; set; }
    }

    public class MatchContestUserTeams
    {
        public MatchContestUserTeams()
        {

        }
        public long _id { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long MatchContestId { get; set; }
        public long MatchContestUserTeamId { get; set; }
        public long MatchId { get; set; }
        public long? UserId { get; set; }
        public long? UserTeamId { get; set; }
        public string TeamAbbr { get; set; }
        public bool isInWinning_Jone { get; set; }
        public long? rank { get; set; }
        public decimal? winning_Amount { get; set; }  
        public bool IsWinningAmountAdded { get; set; }
    }

    public class MatchContestUserTeamsMongoLive
    {
        public long _id { get; set; }
        public long MatchContestUserTeamId { get; set; }
        public long? UserTeamId { get; set; }
        public long? UserId { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long MatchContestId { get; set; }
        public long MatchId { get; set; }
        public bool isInWinning_Jone { get; set; }
        public decimal? winning_Amount { get; set; }
        public long? rank { get; set; }
        public string TeamAbbr { get; set; }
    }

    public class ApproveContestResult
    {
        public long? UserId { get; set; }
        public long? MatchId { get; set; }
        public long? MatchContestId { get; set; }
        public long? UserTeamId { get; set; }
        public string TeamName { get; set; }
        public decimal? FantacyPoints { get; set; }
        public decimal? WinningAmount { get; set; }
        public bool IsInWinning_Jone { get; set; }
        public long? Rank { get; set; }
    }

    public class ProcSaveApproveContestResult
    {
        public int ResultStatus { get; set; }
        public string Result { get; set; }
    }

    public class MatchContestPrizeBreakUpDataMongoLive
    {
        //[BsonIgnore]
        //public long _id { get; set; }
        public MatchContestPrizeBreakUpDataMongoLive()
        {

        }
        
        public long _id { get; set; }
        public Int64 dbId { get; set; }
        public long? MatchId { get; set; }
        public long MatchContestId { get; set; }
        public Int32? StartRank { get; set; }
        public Int32? EndRank { get; set; }
        public decimal? Prize { get; set; }
        public decimal? TotalPrize { get; set; }
    }
}
