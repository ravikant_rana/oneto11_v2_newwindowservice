﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToArchive.Models
{
    public class UserTeams : MongoDbGenericRepository.Models.Document
    {
        public UserTeams()
        {
            players = new List<UserTeamPlayers>();
        }
        public Int64 dbId { get; set; }
        public Int64 UserId { get; set; }
        public Int64 MatchId { get; set; }
        public string TeamAbbr { get; set; }
        public Nullable<decimal> FantacyPoints { get; set; }
        public string UserName { get; set; }
        public string ProfileImage { get; set; }
        public int ProfileAvtarId { get; set; }
        public List<UserTeamPlayers> players { get; set; }
    }

    public class UserTeamPlayers
    {
        public Int64 UserTeamPlayerId { get; set; }
        public Int64 UserTeamId { get; set; }
        public Int64 MatchTeamPlayerId { get; set; }
        public Nullable<decimal> FantacyPoints { get; set; }
        public Nullable<Int32> IsCaptain { get; set; }
        public Nullable<Int32> IsViceCaptain { get; set; }
        public string Name { get; set; }
        public Nullable<decimal> Score { get; set; }
        public Nullable<decimal> Credit { get; set; }
        public string PlayerImage { get; set; }
        public string PlayerType { get; set; }
        public decimal? selectdBy { get; set; }
        public decimal? selectdAsCaption { get; set; }
        public decimal? selectdViceCaption { get; set; }
        public bool is_playing { get; set; }
        public Int64? TeamId { get; set; }
        public string TeamName { get; set; }
        public string Team_Abbr { get; set; }
        //public bool is_substitute { get; set; }
    }
}
