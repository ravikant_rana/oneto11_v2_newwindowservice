﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToArchive.Models
{
    public class JsonMoveToArchive
    {
        public Int64 match_id { get; set; }
        public int status_id { get; set; }
        public string status_str { get; set; }
        public string date_start { get; set; }
    }

    public class UserTeamPoints
    {
        public long _id { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long MatchId { get; set; }
        public string TeamAbbr { get; set; }
        public long UserId { get; set; }
        public long UserTeamId { get; set; }
        public Int32 UserAvatarId { get; set; }

    }

    public class UserTeamPlayersLive
    {
        public long _id { get; set; }
        public long UserTeamPlayerId { get; set; }
        public long MatchTeamPlayerId { get; set; }
        public decimal? FantacyPoints { get; set; }
        public int IsCaptain { get; set; }
        public int IsViceCaptain { get; set; }
        public long MatchId { get; set; }
        public long UserTeamId { get; set; }
    }

    public class MatchContestPrizeBreakUpData
    {
        public long _id { get; set; }
        public long dbId { get; set; }
        public long? MatchId { get; set; }
        public long MatchContestId { get; set; }
        public Int32? StartRank { get; set; }
        public Int32? EndRank { get; set; }
        public decimal? Prize { get; set; }
    }


    public class SaveAdminNotificationRequest
    {
        public long? MatchId { get; set; }
        public long? ContestId { get; set; }
        public Nullable<int> AdminNotificationTypeId { get; set; }
    }


    public class ArchiveUserMatchTeamCount
    {
        public long _id { get; set; }
        public long MatchId { get; set; }
        public long UserId { get; set; }
        public Nullable<int> Team { get; set; }
    }

    public class ArchiveUserMatchContestTeamCount
    {
        public long _id { get; set; }
        public long MatchId { get; set; }
        public long UserId { get; set; }
        public Nullable<int> Team { get; set; }
        public Nullable<int> TotalContest { get; set; }
        public Nullable<decimal> TotalWinningAmount { get; set; }
    }

    public class UserWinningAmountResult
    {
        public long MatchId { get; set; }
        public long UserId { get; set; }
        public decimal TotalWinningAmount { get; set; }
    }

    public class UpdatePlayerPointsToSql
    {
        public Int64 MatchTeamPlayerId { get; set; }
        public Int64 MatchId { get; set; }
        public decimal FantacyPoints { get; set; }
    }

    public class UpdateId
    {
        public long Id { get; set; }
        public long MatchTeamPlayerId { get; set; }
        public long MatchId { get; set; }
    }

    public class ProcResultStatus
    {
        public Int32 ResultStatus { get; set; }
    }

    public class ContestForCancel
    {
        public long Id { get; set; }
    }
    public class UserScratchCard
    {
        public Int64 UserId { get; set; }
    }

    public class UserToken
    {
        public string token { get; set; }
    }
}
