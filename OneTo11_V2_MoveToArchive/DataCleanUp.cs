﻿using MongoDB.Driver;
using OneTo11_V2_MoveToArchive.Helpers;
using OneTo11_V2_MoveToArchive.Models;
using OneTo11_V2_Redis;
using OneTo11_V2_Redis.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToArchive
{
    public class DataCleanUp
    {
        //private DBAccess authDBAccess;
        //private DBAccess dBAccess;
        //private DBAccess transactionDBAccess;

        string liveconnection = ConfigurationManager.AppSettings["LiveMongoConnection"]; //"mongodb://oneto11admin:Intigate123@104.211.183.244:27017/oneto11live?authSource=admin";
        string connection = ConfigurationManager.AppSettings["ArchiveMongoConnection"]; //"mongodb+srv://oneto11rootuser:Wve8BYfHtexpmOSG@cluster1.t98az.mongodb.net/test";

        //string connectionGameString = ConfigurationManager.AppSettings["OneTo11_Game_Connection"];
        //string connectionAuthString = ConfigurationManager.AppSettings["OneTo11_Auth_Connection"];
        //string connectionTransactionString = ConfigurationManager.AppSettings["OneTo11_Transaction_Connection"];

        MongoClient livedbClient;
        MongoClient dbClient;
        public DataCleanUp()
        {

            livedbClient = new MongoClient(liveconnection);
            dbClient = new MongoClient(connection);
            //authDBAccess = new DBAccess(connectionAuthString);
            //dBAccess = new DBAccess(connectionGameString);ik
            //transactionDBAccess = new DBAccess(connectionTransactionString);
        }

        #region "Delete match details from live database"

        public async Task DeleteMatchFromLive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                using (var cache = new CacheService())
                {

                    /* Matches Information */

                    string livematchescache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatches"];
                    string livematchinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchInfo"];  //"Test_LiveMatchInfo";

                    var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                    var liveMatchCollection = database.GetCollection<LiveMatchInfo>("Matches");

                    long matchCount = 0;
                    var filter = Builders<LiveMatchInfo>.Filter.Eq("dbId", objMoveToArchive.match_id);
                    matchCount = liveMatchCollection.Count(filter);

                    LiveMatchInfo liveMatches = new LiveMatchInfo();
                    if (matchCount > 0)
                    {
                        liveMatchCollection.FindOneAndDelete(filter);

                        var _liveMatchCacheData = IntigateCache.RetrieveCacheByCacheName<LiveMatchCache>(livematchescache, cache);
                        _liveMatchCacheData = _liveMatchCacheData.Where(x => x.dbId != objMoveToArchive.match_id).ToList();
                        if (_liveMatchCacheData.Count > 0)
                        {
                            IntigateCache.SaveToCache<LiveMatchCache>(cache, livematchescache, _liveMatchCacheData);
                        }

                        var _liveMatchInfoCacheData = IntigateCache.RetrieveCacheByCacheName<LiveMatchInfo>(livematchinfocache, cache);
                        _liveMatchInfoCacheData = _liveMatchInfoCacheData.Where(x => x.dbId != objMoveToArchive.match_id).ToList();

                        if (_liveMatchInfoCacheData.Count > 0)
                        {
                            IntigateCache.SaveToCache<LiveMatchInfo>(cache, livematchinfocache, _liveMatchInfoCacheData);
                        }

                    }

                    /* End Matches Information */

                    /* MatchTeamPlayerInfo */
                    long matchTeamPlayerCount = 0;
                    string livematchplayerinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(objMoveToArchive.match_id);

                    var matchTeamPlayercollection = database.GetCollection<TeamPlayer>("MatchTeamPlayerInfo");
                    var matchTeamPlayerFilter = Builders<TeamPlayer>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    matchTeamPlayerCount = matchTeamPlayercollection.Count(matchTeamPlayerFilter);

                    if (matchTeamPlayerCount > 0)
                    {
                        await matchTeamPlayercollection.DeleteManyAsync(matchTeamPlayerFilter);
                        IntigateCache.DeleteCache(livematchplayerinfocache);

                    }

                    /* End MatchTeamPlayerInfo */

                    /* UserTeamPoints */
                    long userTeamPointsCount = 0;
                    var listUserTeamPointsWrites = new List<WriteModel<UserTeamPoints>>();
                    var liveUserTeamPointCollection = database.GetCollection<UserTeamPoints>("UserTeamPoints");
                    var _usetTeamPointsFilter = Builders<UserTeamPoints>.Filter.Eq("MatchId", objMoveToArchive.match_id);

                    userTeamPointsCount = liveUserTeamPointCollection.Count(_usetTeamPointsFilter);
                    if (userTeamPointsCount > 0)
                    {
                        listUserTeamPointsWrites.Add(new DeleteManyModel<UserTeamPoints>(_usetTeamPointsFilter));
                        await liveUserTeamPointCollection.BulkWriteAsync(listUserTeamPointsWrites);
                        //await liveUserTeamPointCollection.DeleteManyAsync(_usetTeamPointsFilter);

                    }

                    /* End UserTeamPoints */

                    /* UserTeamPlayers */
                    long userTeamPlayerCount = 0;
                    var listUserTeamPlayerWrites = new List<WriteModel<UserTeamPlayersLive>>();
                    var liveUserTeamPlayerCollection = database.GetCollection<UserTeamPlayersLive>("UserTeamPlayers");
                    var _userTeamPlayerFilter = Builders<UserTeamPlayersLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);

                    userTeamPlayerCount = liveUserTeamPlayerCollection.Count(_userTeamPlayerFilter);
                    if (userTeamPlayerCount > 0)
                    {
                        listUserTeamPlayerWrites.Add(new DeleteManyModel<UserTeamPlayersLive>(_userTeamPlayerFilter));
                        await liveUserTeamPlayerCollection.BulkWriteAsync(listUserTeamPlayerWrites);
                        //for (int _i = 0; _i < (userTeamPlayerCount / 1000) + 1; _i++)
                        //{
                        //    var _userTeamPlayersList = liveUserTeamPlayerCollection.Find(_userTeamPlayerFilter).Limit(1000).Skip(_i * 1000).ToList();
                        //    if (_userTeamPlayersList.Count > 0)
                        //    {
                        //        listWrites.Add(new DeleteManyModel<User>(filterDefinition));
                        //        archiveUserTeamCollection.InsertMany(_userTeamList);
                        //    }
                        //}


                        //await liveUserTeamPlayerCollection.DeleteManyAsync(_userTeamPlayerFilter);
                    }
                    /* End UserTeamPlayers */


                    /* Conest */
                    long contestCount = 0;
                    var liveContestWrite = new List<WriteModel<MatchContest>>();
                    string livematchcontest = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContest"] + Convert.ToString(objMoveToArchive.match_id);
                    var liveContestCollection = database.GetCollection<MatchContest>("Contest");

                    var _contestFilter = Builders<MatchContest>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    contestCount = liveContestCollection.Count(_contestFilter);

                    if (contestCount > 0)
                    {
                        liveContestWrite.Add(new DeleteManyModel<MatchContest>(_contestFilter));
                        await liveContestCollection.BulkWriteAsync(liveContestWrite);
                        //await liveContestCollection.DeleteManyAsync(_contestFilter);
                        IntigateCache.DeleteCache(livematchcontest);

                    }

                    /* End Contest */

                    /* ContestUserTeam */
                    long contestUserTeamCount = 0;
                    var listContestUserTeamWrite = new List<WriteModel<MatchContestUserTeamsMongoLive>>();
                    string livematchcontestuserteamcache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContestUserTeam"] + Convert.ToString(objMoveToArchive.match_id);

                    var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                    var _contestUserTeamFilter = Builders<MatchContestUserTeamsMongoLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    contestUserTeamCount = liveContestUserTeamCollection.Count(_contestUserTeamFilter);

                    if (contestUserTeamCount > 0)
                    {
                        listContestUserTeamWrite.Add(new DeleteManyModel<MatchContestUserTeamsMongoLive>(_contestUserTeamFilter));
                        await liveContestUserTeamCollection.BulkWriteAsync(listContestUserTeamWrite);
                        //await liveContestUserTeamCollection.DeleteManyAsync(_contestUserTeamFilter);
                        IntigateCache.DeleteCache(livematchcontestuserteamcache);
                    }

                    /* End ContestUserTeam */

                    /* MatchContestPrizeBreakup */
                    long prizeBreakUpCount = 0;
                    var listPrizeBreakUpWrite = new List<WriteModel<MatchContestPrizeBreakUpDataMongoLive>>();
                    var liveLeaguesPrizeBreakupCollection = database.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("MatchContestPrizeBreakup");

                    var _prizeBreakUpfilter = Builders<MatchContestPrizeBreakUpDataMongoLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    prizeBreakUpCount = liveLeaguesPrizeBreakupCollection.Count(_prizeBreakUpfilter);

                    List<MatchContestPrizeBreakUpData> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpData>();
                    if (prizeBreakUpCount > 0)
                    {
                        listPrizeBreakUpWrite.Add(new DeleteManyModel<MatchContestPrizeBreakUpDataMongoLive>(_prizeBreakUpfilter));
                        await liveLeaguesPrizeBreakupCollection.BulkWriteAsync(listPrizeBreakUpWrite);
                        //await liveLeaguesPrizeBreakupCollection.DeleteManyAsync(_prizeBreakUpfilter);
                    }

                    /* End MatchContestPrizeBreakup */
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public async Task DeleteUnnecessaryMatchFromLive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                using (var cache = new CacheService())
                {

                    /* Matches Information */

                    string livematchescache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatches"];
                    string livematchinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchInfo"];  //"Test_LiveMatchInfo";

                    var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                    var liveMatchCollection = database.GetCollection<LiveMatchInfo>("Matches");

                    long matchCount = 0;
                    var filter = Builders<LiveMatchInfo>.Filter.Ne("dbId", objMoveToArchive.match_id);
                    
                    matchCount = liveMatchCollection.Count(filter);

                    LiveMatchInfo liveMatches = new LiveMatchInfo();
                    if (matchCount > 0)
                    {
                        liveMatchCollection.DeleteMany(filter);

                        //var _liveMatchCacheData = IntigateCache.RetrieveCacheByCacheName<LiveMatchCache>(livematchescache, cache);
                        //_liveMatchCacheData = _liveMatchCacheData.Where(x => x.dbId != objMoveToArchive.match_id).ToList();
                        //if (_liveMatchCacheData.Count > 0)
                        //{
                        //    IntigateCache.SaveToCache<LiveMatchCache>(cache, livematchescache, _liveMatchCacheData);
                        //}

                        //var _liveMatchInfoCacheData = IntigateCache.RetrieveCacheByCacheName<LiveMatchInfo>(livematchinfocache, cache);
                        //_liveMatchInfoCacheData = _liveMatchInfoCacheData.Where(x => x.dbId != objMoveToArchive.match_id).ToList();

                        //if (_liveMatchInfoCacheData.Count > 0)
                        //{
                        //    IntigateCache.SaveToCache<LiveMatchInfo>(cache, livematchinfocache, _liveMatchInfoCacheData);
                        //}

                    }

                    /* End Matches Information */

                    /* MatchTeamPlayerInfo */
                    long matchTeamPlayerCount = 0;
                    string livematchplayerinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(objMoveToArchive.match_id);

                    var matchTeamPlayercollection = database.GetCollection<TeamPlayer>("MatchTeamPlayerInfo");
                    var matchTeamPlayerFilter = Builders<TeamPlayer>.Filter.Ne("MatchId", objMoveToArchive.match_id);
                    
                    matchTeamPlayerCount = matchTeamPlayercollection.Count(matchTeamPlayerFilter);

                    if (matchTeamPlayerCount > 0)
                    {
                        await matchTeamPlayercollection.DeleteManyAsync(matchTeamPlayerFilter);
                        //IntigateCache.DeleteCache(livematchplayerinfocache);

                    }

                    /* End MatchTeamPlayerInfo */

                    /* UserTeamPoints */
                    long userTeamPointsCount = 0;
                    var listUserTeamPointsWrites = new List<WriteModel<UserTeamPoints>>();
                    var liveUserTeamPointCollection = database.GetCollection<UserTeamPoints>("UserTeamPoints");
                    var _usetTeamPointsFilter = Builders<UserTeamPoints>.Filter.Ne("MatchId", objMoveToArchive.match_id);
                  
                    userTeamPointsCount = liveUserTeamPointCollection.Count(_usetTeamPointsFilter);
                    if (userTeamPointsCount > 0)
                    {
                        listUserTeamPointsWrites.Add(new DeleteManyModel<UserTeamPoints>(_usetTeamPointsFilter));
                        await liveUserTeamPointCollection.BulkWriteAsync(listUserTeamPointsWrites);
                        //await liveUserTeamPointCollection.DeleteManyAsync(_usetTeamPointsFilter);

                    }

                    /* End UserTeamPoints */

                    /* UserTeamPlayers */
                    long userTeamPlayerCount = 0;
                    var listUserTeamPlayerWrites = new List<WriteModel<UserTeamPlayersLive>>();
                    var liveUserTeamPlayerCollection = database.GetCollection<UserTeamPlayersLive>("UserTeamPlayers");
                    var _userTeamPlayerFilter = Builders<UserTeamPlayersLive>.Filter.Ne("MatchId", objMoveToArchive.match_id);
                    
                    userTeamPlayerCount = liveUserTeamPlayerCollection.Count(_userTeamPlayerFilter);
                    if (userTeamPlayerCount > 0)
                    {
                        listUserTeamPlayerWrites.Add(new DeleteManyModel<UserTeamPlayersLive>(_userTeamPlayerFilter));
                        await liveUserTeamPlayerCollection.BulkWriteAsync(listUserTeamPlayerWrites);
                        //for (int _i = 0; _i < (userTeamPlayerCount / 1000) + 1; _i++)
                        //{
                        //    var _userTeamPlayersList = liveUserTeamPlayerCollection.Find(_userTeamPlayerFilter).Limit(1000).Skip(_i * 1000).ToList();
                        //    if (_userTeamPlayersList.Count > 0)
                        //    {
                        //        listWrites.Add(new DeleteManyModel<User>(filterDefinition));
                        //        archiveUserTeamCollection.InsertMany(_userTeamList);
                        //    }
                        //}


                        //await liveUserTeamPlayerCollection.DeleteManyAsync(_userTeamPlayerFilter);
                    }
                    /* End UserTeamPlayers */


                    /* Conest */
                    long contestCount = 0;
                    var liveContestWrite = new List<WriteModel<MatchContest>>();
                    string livematchcontest = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContest"] + Convert.ToString(objMoveToArchive.match_id);
                    var liveContestCollection = database.GetCollection<MatchContest>("Contest");

                    var _contestFilter = Builders<MatchContest>.Filter.Ne("MatchId", objMoveToArchive.match_id);
                    
                    contestCount = liveContestCollection.Count(_contestFilter);

                    if (contestCount > 0)
                    {
                        liveContestWrite.Add(new DeleteManyModel<MatchContest>(_contestFilter));
                        await liveContestCollection.BulkWriteAsync(liveContestWrite);
                        //await liveContestCollection.DeleteManyAsync(_contestFilter);
                        //IntigateCache.DeleteCache(livematchcontest);

                    }

                    /* End Contest */

                    /* ContestUserTeam */
                    long contestUserTeamCount = 0;
                    var listContestUserTeamWrite = new List<WriteModel<MatchContestUserTeamsMongoLive>>();
                    string livematchcontestuserteamcache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContestUserTeam"] + Convert.ToString(objMoveToArchive.match_id);

                    var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                    var _contestUserTeamFilter = Builders<MatchContestUserTeamsMongoLive>.Filter.Ne("MatchId", objMoveToArchive.match_id);
                    
                    contestUserTeamCount = liveContestUserTeamCollection.Count(_contestUserTeamFilter);

                    if (contestUserTeamCount > 0)
                    {
                        listContestUserTeamWrite.Add(new DeleteManyModel<MatchContestUserTeamsMongoLive>(_contestUserTeamFilter));
                        await liveContestUserTeamCollection.BulkWriteAsync(listContestUserTeamWrite);
                        //await liveContestUserTeamCollection.DeleteManyAsync(_contestUserTeamFilter);
                        //IntigateCache.DeleteCache(livematchcontestuserteamcache);
                    }

                    /* End ContestUserTeam */

                    /* MatchContestPrizeBreakup */
                    long prizeBreakUpCount = 0;
                    var listPrizeBreakUpWrite = new List<WriteModel<MatchContestPrizeBreakUpDataMongoLive>>();
                    var liveLeaguesPrizeBreakupCollection = database.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("MatchContestPrizeBreakup");

                    var _prizeBreakUpfilter = Builders<MatchContestPrizeBreakUpDataMongoLive>.Filter.Ne("MatchId", objMoveToArchive.match_id);
                    
                    prizeBreakUpCount = liveLeaguesPrizeBreakupCollection.Count(_prizeBreakUpfilter);

                    List<MatchContestPrizeBreakUpData> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpData>();
                    if (prizeBreakUpCount > 0)
                    {
                        listPrizeBreakUpWrite.Add(new DeleteManyModel<MatchContestPrizeBreakUpDataMongoLive>(_prizeBreakUpfilter));
                        await liveLeaguesPrizeBreakupCollection.BulkWriteAsync(listPrizeBreakUpWrite);
                        //await liveLeaguesPrizeBreakupCollection.DeleteManyAsync(_prizeBreakUpfilter);
                    }

                    /* End MatchContestPrizeBreakup */
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public async Task DeleteMatchContestFromArchive(long contestId)
        {
            try
            {

                var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);


                var liveContestCollection = database.GetCollection<MatchContest>("ArchiveContest");
                var liveContestCollection1 = database.GetCollection<MatchContest>("ArchiveContest2314");

                var _contestFilter = Builders<MatchContest>.Filter.Eq("dbId", contestId);
                var contestList = liveContestCollection.Find(_contestFilter).ToList();

                if (contestList.Count > 0)
                {
                    //liveContestCollection1.InsertMany(contestList);
                    //liveContestCollection.DeleteMany(_contestFilter);

                }


                
                var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");
                var liveContestUserTeamCollection1 = database.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam2314");
                var _contestUserTeamFilter = Builders<MatchContestUserTeams>.Filter.Eq("MatchContestId", contestId);
                var contestUserTeamList = liveContestUserTeamCollection.Find(_contestUserTeamFilter).ToList();

                if (contestUserTeamList.Count > 0)
                {
                    //liveContestUserTeamCollection1.InsertMany(contestUserTeamList);
                    //liveContestUserTeamCollection.DeleteMany(_contestUserTeamFilter);
                }



                /* End MatchContestPrizeBreakup */

            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        #endregion "End Delete match details from live database"
    }
}
