﻿using MongoDB.Driver;
using Newtonsoft.Json;
using OneTo11_V2_MoveToArchive.Helpers;
using OneTo11_V2_MoveToArchive.Models;
using OneTo11_V2_MoveToArchive.QueryExtensions;
using OneTo11_V2_Redis;
using OneTo11_V2_Redis.Helpers;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;

namespace OneTo11_V2_MoveToArchive
{
    public partial class ArchiveMessageReceiver : DefaultBasicConsumer
    {
        private DBAccess authDBAccess;
        private DBAccess dBAccess;
        private DBAccess transactionDBAccess;
        private readonly IModel _channel;
        string liveconnection = ConfigurationManager.AppSettings["LiveMongoConnection"]; //"mongodb://oneto11admin:Intigate123@104.211.183.244:27017/oneto11live?authSource=admin";
        string connection = ConfigurationManager.AppSettings["ArchiveMongoConnection"]; //"mongodb+srv://oneto11rootuser:Wve8BYfHtexpmOSG@cluster1.t98az.mongodb.net/test";

        string connectionGameString = ConfigurationManager.AppSettings["OneTo11_Game_Connection"];
        string connectionAuthString = ConfigurationManager.AppSettings["OneTo11_Auth_Connection"];
        string connectionTransactionString = ConfigurationManager.AppSettings["OneTo11_Transaction_Connection"];

        MongoClient livedbClient;
        MongoClient dbClient;

        string filepath = ConfigurationManager.AppSettings["FirestoreAPIKeyPath"];
        public ArchiveMessageReceiver(IModel channel)
        {
            _channel = channel;
            livedbClient = new MongoClient(liveconnection);
            dbClient = new MongoClient(connection);
            authDBAccess = new DBAccess(connectionAuthString);
            dBAccess = new DBAccess(connectionGameString);
            transactionDBAccess = new DBAccess(connectionTransactionString);
            try
            {
                var credential = GoogleCredential.FromFile(filepath);
                var defaultApp = FirebaseApp.Create(new AppOptions()
                {
                    Credential = credential, //GoogleCredential.GetApplicationDefault(),
                                             //ServiceAccountId = "firebase-adminsdk-i1cxj@oneto11-1581577228969.iam.gserviceaccount.com",
                });
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            Library.WriteErrorLog("Archive receiver started");
            JsonMoveToArchive jsonMoveToArchive = new JsonMoveToArchive();

            string jsonData = Encoding.UTF8.GetString(body);
            jsonMoveToArchive = JsonConvert.DeserializeObject<JsonMoveToArchive>(jsonData);

            bool isExists = false;
            isExists = IsCompleteMatchExists(jsonMoveToArchive);

            if (isExists == false)
            {
                SaveMatchesToArchive(jsonMoveToArchive);
                SaveMatchTeamPlayerInfoToArchive(jsonMoveToArchive);
                SaveUserTeamPointsToArchive(jsonMoveToArchive);
                SaveUserTeamPlayersToArchive(jsonMoveToArchive);
                SaveContestToArchive(jsonMoveToArchive);
                SaveContestUserTeamToArchive(jsonMoveToArchive);
                SaveMatchContestPrizeBreakUpToArchive(jsonMoveToArchive);

                if (jsonMoveToArchive.status_id == 2 || jsonMoveToArchive.status_id == 5)
                {
                    GetCompletedMatchTransaction(jsonMoveToArchive);
                }

                DeleteMatchContestNotFilled(jsonMoveToArchive);
                SaveAdminNotificationRequest saveAdminNotificationRequest = new SaveAdminNotificationRequest();
                saveAdminNotificationRequest.MatchId = jsonMoveToArchive.match_id;
                if (jsonMoveToArchive.status_id == 2 || jsonMoveToArchive.status_id == 5)
                {
                    saveAdminNotificationRequest.AdminNotificationTypeId = 1; /* Match completed */
                    SaveAdminNotification(saveAdminNotificationRequest);
                    AddWinningAmountToWallet(jsonMoveToArchive.match_id);
                }
                else if (jsonMoveToArchive.status_id == 4)
                {
                    saveAdminNotificationRequest.AdminNotificationTypeId = 4; /* Match cancelled or abandoned */
                    SaveAdminNotification(saveAdminNotificationRequest);
                }

                UpdateMatchTeamPlayerPointsToSql(jsonMoveToArchive);

                SaveUserMatchTeamCount(jsonMoveToArchive);

                if (jsonMoveToArchive.status_id == 2 || jsonMoveToArchive.status_id == 5)
                {
                    GetIplMatchContestCashbackUser(jsonMoveToArchive);
                }
                DeleteMatchesCache(jsonMoveToArchive);

                DeleteMatchFromLive(jsonMoveToArchive);

                //DataCleanUp dataCleanUp = new DataCleanUp();
                //dataCleanUp.DeleteMatchFromLive(jsonMoveToArchive);



                Library.WriteErrorLog("Archive receiver completed");
            }



            _channel.BasicAck(deliveryTag, false);



        }

        public void Test(JsonMoveToArchive jsonMoveToArchive)
        {
            SaveMatchesToArchive(jsonMoveToArchive);
            SaveMatchTeamPlayerInfoToArchive(jsonMoveToArchive);
            SaveUserTeamPointsToArchive(jsonMoveToArchive);
            SaveUserTeamPlayersToArchive(jsonMoveToArchive);
            SaveContestToArchive(jsonMoveToArchive);
            SaveContestUserTeamToArchive(jsonMoveToArchive);
            SaveMatchContestPrizeBreakUpToArchive(jsonMoveToArchive);

            if (jsonMoveToArchive.status_id == 2 || jsonMoveToArchive.status_id == 5)
            {
                GetCompletedMatchTransaction(jsonMoveToArchive);
            }

            ////SaveMatchScoreCardToArchive(jsonMoveToArchive);

            DeleteMatchContestNotFilled(jsonMoveToArchive);
            SaveAdminNotificationRequest saveAdminNotificationRequest = new SaveAdminNotificationRequest();
            saveAdminNotificationRequest.MatchId = jsonMoveToArchive.match_id;
            if (jsonMoveToArchive.status_id == 2 || jsonMoveToArchive.status_id == 5)
            {
                saveAdminNotificationRequest.AdminNotificationTypeId = 1; /* Match completed */
                SaveAdminNotification(saveAdminNotificationRequest);
                AddWinningAmountToWallet(jsonMoveToArchive.match_id);
            }
            else if (jsonMoveToArchive.status_id == 4)
            {
                saveAdminNotificationRequest.AdminNotificationTypeId = 4; /* Match cancelled or abandoned */
                SaveAdminNotification(saveAdminNotificationRequest);
            }

            UpdateMatchTeamPlayerPointsToSql(jsonMoveToArchive);

            SaveUserMatchTeamCount(jsonMoveToArchive);
        }

        public void SaveMatchesToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                //var cache = new CacheService();
                //string livematchescache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatches"];
                //string livematchinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchInfo"];  //"Test_LiveMatchInfo";

                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveMatchCollection = database.GetCollection<LiveMatchInfo>("Matches");
                var archiveMatchCollection = databaseArchive.GetCollection<LiveMatchInfo>("ArchiveMatches");

                var filter = Builders<LiveMatchInfo>.Filter.Eq("dbId", objMoveToArchive.match_id);
                var _matchList = liveMatchCollection.Find(filter).ToList();

                LiveMatchInfo liveMatches = new LiveMatchInfo();
                if (_matchList.Count > 0)
                {
                    liveMatches = _matchList.FirstOrDefault();
                    if (liveMatches.status == 2 || liveMatches.status == 5)
                    {
                        liveMatches.status = 5;
                        liveMatches.status_str = "Result pending";
                    }
                    else
                    {
                        liveMatches.status = objMoveToArchive.status_id;
                        liveMatches.status_str = objMoveToArchive.status_str;
                    }
                    liveMatches.MatchEndTime = DateTime.UtcNow;
                    archiveMatchCollection.InsertOne(liveMatches);
                    try
                    {
                        UpdateMatchContestStatus(Convert.ToInt64(liveMatches.dbId), null, Convert.ToInt32(liveMatches.status), Convert.ToString(liveMatches.status_str), "MATCHSTATUS");


                        //liveMatchCollection.FindOneAndDelete(filter);

                        //var _liveMatchCacheData = IntigateCache.RetrieveCacheByCacheName<LiveMatchCache>(livematchescache, cache);
                        //_liveMatchCacheData = _liveMatchCacheData.Where(x => x.dbId != objMoveToArchive.match_id).ToList();
                        //if (_liveMatchCacheData.Count > 0)
                        //{
                        //    IntigateCache.SaveToCache<LiveMatchCache>(cache, livematchescache, _liveMatchCacheData);
                        //}

                        //var _liveMatchInfoCacheData = IntigateCache.RetrieveCacheByCacheName<LiveMatchInfo>(livematchinfocache, cache);
                        //_liveMatchInfoCacheData = _liveMatchInfoCacheData.Where(x => x.dbId != objMoveToArchive.match_id).ToList();

                        //if (_liveMatchInfoCacheData.Count > 0)
                        //{
                        //    IntigateCache.SaveToCache<LiveMatchInfo>(cache, livematchinfocache, _liveMatchInfoCacheData);
                        //}
                    }
                    catch (Exception ex)
                    {
                        //Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
                    }

                }

            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveMatchTeamPlayerInfoToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                string livematchplayerinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(objMoveToArchive.match_id);
                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var archiveDatabase = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var matchTeamPlayercollection = database.GetCollection<TeamPlayer>("MatchTeamPlayerInfo");
                var matchTeamPlayerFilter = Builders<TeamPlayer>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                var _matchTeamPlayerMongoList = matchTeamPlayercollection.Find(matchTeamPlayerFilter).ToList();

                if (_matchTeamPlayerMongoList.Count > 0)
                {
                    var archiveMatchTeamPlayercollection = archiveDatabase.GetCollection<TeamPlayer>("ArchiveMatchTeamPlayerInfo");

                    archiveMatchTeamPlayercollection.InsertMany(_matchTeamPlayerMongoList);


                    //matchTeamPlayercollection.DeleteMany(matchTeamPlayerFilter);
                    //IntigateCache.DeleteCache(livematchplayerinfocache);

                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveUserTeamPointsToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                //var cache = new CacheService();
                long totNum = 0;
                string livematchplayerinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(objMoveToArchive.match_id);

                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveUserTeamPointCollection = database.GetCollection<UserTeamPoints>("UserTeamPoints");
                var filter = Builders<UserTeamPoints>.Filter.Eq("MatchId", objMoveToArchive.match_id);

                totNum = liveUserTeamPointCollection.Count(filter);
                if (totNum > 0)
                {
                    var archiveUserTeamPointCollection = databaseArchive.GetCollection<UserTeamPoints>("ArchiveUserTeamPoints");

                    for (int _i = 0; _i < totNum / 10000 + 1; _i++)
                    {
                        var _userTeamPointsList = liveUserTeamPointCollection.Find(filter).Limit(10000).Skip(_i * 10000).ToList();
                        if (_userTeamPointsList.Count > 0)
                        {

                            archiveUserTeamPointCollection.InsertMany(_userTeamPointsList);
                        }
                    }
                    //liveUserTeamPointCollection.DeleteMany(filter);
                }


                //var _userTeamPointsList = liveUserTeamPointCollection.Find(filter).ToList();
                //if (_userTeamPointsList.Count > 0)
                //{
                //    var archiveUserTeamPointCollection = databaseArchive.GetCollection<UserTeamPoints>("ArchiveUserTeamPoints");
                //    archiveUserTeamPointCollection.InsertMany(_userTeamPointsList);

                //    //liveUserTeamPointCollection.DeleteMany(filter);

                //}


            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveUserTeamPlayersToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                long totNum = 0;
                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveUserTeamPlayerCollection = database.GetCollection<UserTeamPlayersLive>("UserTeamPlayers");
                var filter = Builders<UserTeamPlayersLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);

                //var _userTeamPlayersList = liveUserTeamPlayerCollection.Find(filter).ToList();
                totNum = liveUserTeamPlayerCollection.Count(filter);
                if (totNum > 0)
                {
                    var archiveUserTeamCollection = databaseArchive.GetCollection<UserTeamPlayersLive>("ArchiveUserTeamPlayers");

                    for (int _i = 0; _i < totNum / 10000 + 1; _i++)
                    {
                        var _userTeamPlayersList = liveUserTeamPlayerCollection.Find(filter).Limit(10000).Skip(_i * 10000).ToList();
                        if (_userTeamPlayersList.Count > 0)
                        {

                            archiveUserTeamCollection.InsertMany(_userTeamPlayersList);
                        }
                    }
                    //liveUserTeamPlayerCollection.DeleteMany(filter);
                }

                //if (_userTeamPlayersList.Count > 0)
                //{
                //    var archiveUserTeamCollection = databaseArchive.GetCollection<UserTeamPlayersLive>("ArchiveUserTeamPlayers");
                //    archiveUserTeamCollection.InsertMany(_userTeamPlayersList);

                //    //liveUserTeamPlayerCollection.DeleteMany(filter);
                //}
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveContestToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                //var cache = new CacheService();
                string livematchcontest = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContest"] + Convert.ToString(objMoveToArchive.match_id);
                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveContestCollection = database.GetCollection<MatchContest>("Contest");
                var archiveContestCollection = databaseArchive.GetCollection<MatchContest>("ArchiveContest");

                var filter = Builders<MatchContest>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                //var _contestList = IntigateCache.RetrieveCacheByCacheName<MatchContest>(livematchcontest, cache);

                var _contestList = liveContestCollection.Find(filter).ToList();

                if (_contestList.Count > 0)
                {
                    List<MatchContest> matchContestList = new List<MatchContest>();
                    foreach (var contest in _contestList)
                    {
                        MatchContest matchContest = new MatchContest();
                        matchContest._id = contest._id;
                        matchContest.dbId = contest.dbId;
                        matchContest.MatchId = contest.MatchId;
                        matchContest.API_MatchId = contest.API_MatchId;
                        matchContest.Title = contest.Title;
                        matchContest.MaxUsers = contest.MaxUsers;
                        matchContest.WinerPercent = contest.WinerPercent;
                        matchContest.ContestInvitationCode = contest.ContestInvitationCode;
                        matchContest.TotalContestAmount = contest.TotalContestAmount;
                        matchContest.TotalTeam = contest.TotalTeam;
                        matchContest.FirstWinner = contest.FirstWinner;
                        matchContest.EntryFee = contest.EntryFee;
                        matchContest.IsConfirm = contest.IsConfirm;
                        matchContest.MultipleEntryAllowed = contest.MultipleEntryAllowed;
                        matchContest.IsPractice = contest.IsPractice;
                        if (contest.status == 3 && (objMoveToArchive.status_id == 2 || objMoveToArchive.status_id == 5))
                        {
                            matchContest.status = 5;
                            matchContest.status_str = "Result pending";
                        }
                        else if (objMoveToArchive.status_id == 4)
                        {
                            matchContest.status = 4;
                            matchContest.status_str = "Cancelled";
                        }
                        else
                        {
                            matchContest.status = contest.status;
                            matchContest.status_str = contest.status_str;
                        }

                        matchContestList.Add(matchContest);

                        UpdateMatchContestStatus(matchContest.MatchId, matchContest.dbId, matchContest.status, matchContest.status_str, "MATCHCONTESTSTATUS");
                    }
                    if (matchContestList.Count > 0)
                    {
                        archiveContestCollection.InsertMany(matchContestList);

                        //liveContestCollection.DeleteMany(filter);
                        //IntigateCache.DeleteCache(livematchcontest);
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveContestUserTeamToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();
                //var cache = new CacheService();
                string livematchcontestuserteamcache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContestUserTeam"] + Convert.ToString(objMoveToArchive.match_id);

                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");

                var archiveMatchCollection = databaseArchive.GetCollection<LiveMatchInfo>("ArchiveMatches");

                var matchfilter = Builders<LiveMatchInfo>.Filter.Eq("dbId", objMoveToArchive.match_id);
                var _matchList = archiveMatchCollection.Find(matchfilter).ToList();

                var contestCollection = databaseArchive.GetCollection<MatchContest>("ArchiveContest");
                var contestfilter = Builders<MatchContest>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                //contestfilter = contestfilter & Builders<MatchContest>.Filter.Ne("status", 4);
                var _matchContestList = contestCollection.Find(contestfilter).ToList();


                var filter = Builders<MatchContestUserTeamsMongoLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                var _contestUserTeamList = liveContestUserTeamCollection.Find(filter).ToList();
                var distinctRank = _contestUserTeamList.Select(x => new
                {
                    x.rank,
                    x.MatchContestId
                }).Distinct().OrderByDescending(x => x.MatchContestId).ThenByDescending(x => x.rank);


                foreach (var mch in _matchContestList)
                {
                    try
                    {
                        List<MatchContestUserTeams> matchContestUserTeamsList = new List<MatchContestUserTeams>();
                        foreach (var r in distinctRank.Where(y => y.MatchContestId == mch.dbId))
                        {
                            int rankCount = _contestUserTeamList.Where(x => x.rank == r.rank && x.MatchContestId == r.MatchContestId).Count();
                            foreach (var cut in _contestUserTeamList.Where(x => x.rank == r.rank && x.MatchContestId == r.MatchContestId))
                            {
                                MatchContestUserTeams matchContestUserTeams = new MatchContestUserTeams();
                                matchContestUserTeams._id = cut._id;
                                matchContestUserTeams.MatchContestUserTeamId = cut.MatchContestUserTeamId;
                                matchContestUserTeams.UserTeamId = cut.UserTeamId;
                                matchContestUserTeams.UserId = cut.UserId;
                                matchContestUserTeams.FantacyPoints = cut.FantacyPoints;
                                matchContestUserTeams.MatchContestId = cut.MatchContestId;
                                matchContestUserTeams.MatchId = cut.MatchId;
                                matchContestUserTeams.isInWinning_Jone = (objMoveToArchive.status_id == 4 || mch.status == 4) ? false : cut.isInWinning_Jone;
                                //matchContestUserTeams.winning_Amount = (((cut.winning_Amount ?? 0) > 0 && (rankCount > 0)) ? (cut.winning_Amount / rankCount) : cut.winning_Amount);
                                matchContestUserTeams.winning_Amount = (objMoveToArchive.status_id == 4 || mch.status == 4) ? 0 : GetWinnigPrize(cut.MatchContestId, cut.rank, rankCount);
                                matchContestUserTeams.rank = cut.rank;
                                matchContestUserTeams.TeamAbbr = cut.TeamAbbr;
                                matchContestUserTeams.IsWinningAmountAdded = false;
                                matchContestUserTeamsList.Add(matchContestUserTeams);


                                //if (matchContestUserTeams.winning_Amount > 0 && (objMoveToArchive.status_id == 2 || objMoveToArchive.status_id == 5)) /* Amount greater than 0 and status completed */
                                //{
                                //    SaveNotificationRequest saveNotificationRequest = new SaveNotificationRequest();
                                //    saveNotificationRequest.LoggedInUserId = cut.UserId;
                                //    saveNotificationRequest.NotificationTypeId = 5; /* CongratulationsYouWon */
                                //    saveNotificationRequest.Team1VsTeam2 = mch.TeamA + " Vs " + mch.TeamB;
                                //    saveNotificationRequest.Points = cut.FantacyPoints;
                                //    saveNotificationRequest.Rank = Convert.ToInt32(cut.rank);
                                //    saveNotificationRequest.Amount = matchContestUserTeams.winning_Amount;
                                //    saveNotificationRequest.Date = objMoveToArchive.date_start;
                                //    int notificationResponse = SaveNotification(saveNotificationRequest);
                                //}
                            }
                        }

                        if (matchContestUserTeamsList.Count > 0)
                        {
                            archiveContestUserTeamCollection.InsertMany(matchContestUserTeamsList);

                            //liveContestUserTeamCollection.DeleteMany(filter);
                            //IntigateCache.DeleteCache(livematchcontestuserteamcache);
                        }
                    }
                    catch (Exception ex)
                    {
                        Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
                    }
                }

                /* When match status is completed */
                if (objMoveToArchive.status_id == 2 || objMoveToArchive.status_id == 5)
                {
                    SaveApproveContestResultInSqlDb(objMoveToArchive);
                }


            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveMatchContestPrizeBreakUpToArchive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveLeaguesPrizeBreakupCollection = database.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("MatchContestPrizeBreakup");
                var archiveLeaguesPrizeBreakupCollection = databaseArchive.GetCollection<MatchContestPrizeBreakUpData>("ArchiveMatchContestPrizeBreakup");

                var filter = Builders<MatchContestPrizeBreakUpDataMongoLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                var _leaguePrizeBreakupList = liveLeaguesPrizeBreakupCollection.Find(filter).ToList();

                List<MatchContestPrizeBreakUpData> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpData>();
                if (_leaguePrizeBreakupList.Count > 0)
                {
                    foreach (var obj in _leaguePrizeBreakupList)
                    {
                        MatchContestPrizeBreakUpData matchContestPrizeBreakUpData = new MatchContestPrizeBreakUpData();
                        matchContestPrizeBreakUpData._id = obj._id;
                        matchContestPrizeBreakUpData.dbId = obj.dbId;
                        matchContestPrizeBreakUpData.MatchId = obj.MatchId;
                        matchContestPrizeBreakUpData.MatchContestId = obj.MatchContestId;
                        matchContestPrizeBreakUpData.StartRank = obj.StartRank;
                        matchContestPrizeBreakUpData.EndRank = obj.EndRank;
                        matchContestPrizeBreakUpData.Prize = obj.Prize;
                        matchContestPrizeBreakUpDataList.Add(matchContestPrizeBreakUpData);
                    }
                    if (matchContestPrizeBreakUpDataList.Count > 0)
                    {
                        archiveLeaguesPrizeBreakupCollection.InsertMany(matchContestPrizeBreakUpDataList);

                        //liveLeaguesPrizeBreakupCollection.DeleteMany(filter);
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void UpdateMatchContestStatus(long? match_id, long? contest_id, int? status_id, string status_str, string flag)
        {
            int result = 0;
            try
            {
                string[] matchParam = { "@match_id", "@contest_id", "@status_id", "@status_str", "@flag" };
                result = dBAccess.SaveDataStoredProcedure("[dbo].[PROC_UPDATE_MATCH_CONTEST_STATUS]", matchParam
                                             , match_id, contest_id, status_id, status_str, flag);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void GetCompletedMatchTransaction(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                string[] notificationParam = { "@MatchId" };
                DataTable data = dBAccess.GetDataFromStoredProcedure("[dbo].[PROC_GET_COMPLETED_MATCH_TRANSACTION]", notificationParam, objMoveToArchive.match_id);

                if (data.Rows.Count > 0)
                {
                    UpdateMatchCompletedDateInWalletTransaction(data);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void UpdateMatchCompletedDateInWalletTransaction(DataTable dataTable)
        {
            try
            {
                string[] paramName = { "@dtWalletTransaction" };
                int result = transactionDBAccess.SaveDataStoredProcedure("[dbo].[PROC_UPDATE_MATCH_COMPLETE_DATE_IN_TRANSACTION]", paramName, dataTable);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        //public void SaveMatchScoreCardToArchive(JsonMoveToArchive objMoveToArchive)
        //{
        //    try
        //    {
        //        //var dbList = dbClient.ListDatabases().ToList();
        //        var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
        //        var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
        //        var liveMatchScoreCardCollection = database.GetCollection<PlayerScorecardResponse>("MatchScoreCard");
        //        var archiveMatchScoreCardCollection = databaseArchive.GetCollection<PlayerScorecardResponse>("ArchiveMatchScoreCard");

        //        var filter = Builders<PlayerScorecardResponse>.Filter.Eq("dbmchid", objMoveToArchive.match_id);
        //        var _matchScoreCardList = liveMatchScoreCardCollection.Find(filter).ToList();


        //        if (_matchScoreCardList.Count > 0)
        //        {
        //            archiveMatchScoreCardCollection.InsertMany(_matchScoreCardList);
        //            //liveMatchScoreCardCollection.DeleteMany(filter);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
        //    }
        //}

        public void DeleteMatchContestNotFilled(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                string[] paramName = { "@MatchId" };
                int result = dBAccess.SaveDataStoredProcedure("[dbo].[PROC_DELETE_MATCHCONTESTNOTFILLED]", paramName, objMoveToArchive.match_id);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public int SaveAdminNotification(SaveAdminNotificationRequest notificationRequest)
        {
            int result = 0;
            try
            {
                string[] notificationParam = { "@MatchId", "@ContestId", "@AdminNotificationType_Fk_Id" };
                DataTable data = dBAccess.GetDataFromStoredProcedure("[dbo].[PROC_SAVE_ADMIN_NOTIFICATION_MESSAGE]", notificationParam
                                             , notificationRequest.MatchId, notificationRequest.ContestId, notificationRequest.AdminNotificationTypeId
                                             );

                if (data.Rows.Count > 0)
                {
                    result = Convert.ToInt32(data.Rows[0]["ResultCode"]);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
            return result;
        }

        public void AddWinningAmountToWallet(long match_id)
        {
            try
            {
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);

                string[] param1 = { "@MatchId" };
                DataTable data = transactionDBAccess.GetDataFromStoredProcedure("[dbo].[PROC_GET_RESULT_PENDING_CONTEST]", param1, match_id);
                if (data.Rows.Count > 0)
                {
                    foreach (DataRow dataRow in data.Rows)
                    {
                        int result = 0;
                        string[] param2 = { "@MatchId", "@MatchContestId" };
                        result = transactionDBAccess.SaveDataStoredProcedure("[dbo].[PROC_ADD_WINNIG_AMOUNT_TO_WALLET]", param2, match_id, Convert.ToInt64(dataRow["MatchContestId"]));

                        int result1 = 0;
                        string[] param3 = { "@MatchId", "@ContestId" };
                        result1 = dBAccess.SaveDataStoredProcedure("[dbo].[ADMIN_PROC_UPDATE_APPROVAL_PROCESS_CONTEST_STATUS]", param3, match_id, Convert.ToInt64(dataRow["MatchContestId"]));

                        var archiveContestCollection = databaseArchive.GetCollection<MatchContest>("ArchiveContest");

                        var filter = Builders<MatchContest>.Filter.Eq("dbId", Convert.ToInt64(dataRow["MatchContestId"]));
                        var _contestList = archiveContestCollection.Find(filter).ToList();

                        if (_contestList.Count > 0)
                        {
                            ///update contest status 
                            var updatecontestdefinition = Builders<MatchContest>.Update
                                .Set(p => p.status, 2)
                                .Set(q => q.status_str, "Completed");
                            archiveContestCollection.UpdateOne(filter, updatecontestdefinition);
                        }

                        var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");
                        var _ContestUserTeamFilter = Builders<MatchContestUserTeams>.Filter.Eq("MatchContestId", Convert.ToInt64(dataRow["MatchContestId"]));
                        var _contestUserTeamList = archiveContestUserTeamCollection.Find(_ContestUserTeamFilter).ToList();

                        if (_contestUserTeamList.Count > 0)
                        {
                            ///update contest user team winning amount added 
                            var updatecontestuserteamdefinition = Builders<MatchContestUserTeams>.Update
                                .Set(p => p.IsWinningAmountAdded, true);
                            archiveContestUserTeamCollection.UpdateMany(_ContestUserTeamFilter, updatecontestuserteamdefinition);
                        }
                    }

                    /* This section is processed from admin */

                    //var archiveMatchCollection = databaseArchive.GetCollection<LiveMatches>("ArchiveMatches");
                    //var matchFilter = Builders<LiveMatches>.Filter.Eq("dbId", match_id);
                    //var _matchList = archiveMatchCollection.Find(matchFilter).ToList();

                    //if (_matchList.Count > 0)
                    //{
                    //    ///update match status 
                    //    var updatematchdefinition = Builders<LiveMatches>.Update
                    //        .Set(p => p.status, 2)
                    //        .Set(q => q.status_str, "Completed");
                    //    archiveMatchCollection.UpdateOne(matchFilter, updatematchdefinition);
                    //}

                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }

        }

        private decimal GetWinnigPrize(Int64 cid, Int64? rank, int rankcount)
        {
            Int64 startrank = rank ?? 0;
            Int64 endrank = startrank + rankcount - 1;
            var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
            var contestPriceBreakupcollection = database.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("MatchContestPrizeBreakup");
            //List<MatchContestUserTeams> matchContestUserTeamsList = new List<MatchContestUserTeams>();
            var contestPriceBreakupfilter = Builders<MatchContestPrizeBreakUpDataMongoLive>.Filter.Eq(x => x.MatchContestId, cid);
            var contestPriceBreakupupdate = contestPriceBreakupcollection.Find(contestPriceBreakupfilter).ToList();
            decimal amount = 0;
            for (var i = rank; i <= endrank; i++)
            {
                var obj = contestPriceBreakupupdate.Where(x => x.EndRank >= i && x.StartRank <= i).FirstOrDefault();
                if (obj != null)
                    amount = amount + (obj.Prize ?? 0);
            }
            amount = Math.Round((amount / rankcount), 2);
            return amount;
        }

        public void SaveApproveContestResultInSqlDb(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");
                var filter = Builders<MatchContestUserTeams>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                //filter = filter & Builders<MatchContestUserTeams>.Filter.Eq("isInWinning_Jone", true);
                filter = filter & Builders<MatchContestUserTeams>.Filter.Where(x => x.winning_Amount > 0);
                var _matchContestUserTeamsList = archiveContestUserTeamCollection.Find(filter).ToList();
                if (_matchContestUserTeamsList.Count > 0)
                {
                    List<ApproveContestResult> approveContestResultList = new List<ApproveContestResult>();
                    foreach (var cut in _matchContestUserTeamsList)
                    {
                        ApproveContestResult approveContestResult = new ApproveContestResult();
                        approveContestResult.UserId = cut.UserId;
                        approveContestResult.MatchId = cut.MatchId;
                        approveContestResult.MatchContestId = cut.MatchContestId;
                        approveContestResult.UserTeamId = cut.UserTeamId;
                        approveContestResult.TeamName = cut.TeamAbbr;
                        approveContestResult.FantacyPoints = cut.FantacyPoints;
                        approveContestResult.WinningAmount = cut.winning_Amount;
                        approveContestResult.IsInWinning_Jone = cut.isInWinning_Jone;
                        approveContestResult.Rank = cut.rank;
                        approveContestResultList.Add(approveContestResult);
                    }

                    if (approveContestResultList.Count > 0)
                    {
                        try
                        {
                            string[] paramName = { "@dtApproveContestResult" };
                            var result = transactionDBAccess.GetDataFromStoredProcedure<ProcSaveApproveContestResult>("[dbo].[PROC_SAVE_APPROVE_CONTEST_RESULT]", paramName, DataTableExtension.ConvertListToDataTable(approveContestResultList));
                        }
                        catch (Exception ex)
                        {
                            Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }

        }

        public void UpdateMatchTeamPlayerPointsToSql(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                var archiveDatabase = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var archiveMatchTeamPlayercollection = archiveDatabase.GetCollection<TeamPlayer>("ArchiveMatchTeamPlayerInfo");
                var matchTeamPlayerFilter = Builders<TeamPlayer>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                var _matchTeamPlayerMongoList = archiveMatchTeamPlayercollection.Find(matchTeamPlayerFilter).ToList();

                if (_matchTeamPlayerMongoList.Count > 0)
                {
                    List<UpdatePlayerPointsToSql> updatePlayerPointsToSqlList = new List<UpdatePlayerPointsToSql>();
                    foreach (var ppoints in _matchTeamPlayerMongoList)
                    {
                        UpdatePlayerPointsToSql updatePlayerPointsToSql = new UpdatePlayerPointsToSql
                        {
                            MatchTeamPlayerId = ppoints.MatchTeamPlayerId,
                            MatchId = ppoints.MatchId,
                            FantacyPoints = (!string.IsNullOrEmpty(Convert.ToString(ppoints.FantacyPoints))) ? ppoints.FantacyPoints : 0
                        };

                        updatePlayerPointsToSqlList.Add(updatePlayerPointsToSql);

                    }

                    if (updatePlayerPointsToSqlList.Count > 0)
                    {
                        string[] paramName = { "@dtUpdatePlayerPointsToSql" };
                        var result = dBAccess.SaveDataStoredProcedure("[dbo].[PROC_UPDATE_MatchTeamPlayers_FantacyPoints]", paramName, DataTableExtension.ConvertListToDataTable(updatePlayerPointsToSqlList));
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveUserMatchTeamCount(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                string[] notificationParam = { "@MatchId" };
                var data = dBAccess.GetDataFromStoredProcedure<ProcResultStatus>("[dbo].[uspSaveUserContestAndTeamCount]", notificationParam, objMoveToArchive.match_id).ToList();
                if (data.Count > 0)
                {
                    if (data.FirstOrDefault().ResultStatus == 1)
                    {
                        SaveUserWinningAmount(objMoveToArchive);
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void DeleteMatchesCache(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                //var cache = new CacheService();
                string cacheName = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:Matches"] + ":" + Convert.ToString(objMoveToArchive.match_id);
                IntigateCache.DeleteCache(cacheName);

                string contestDetailsCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + "Contest:ContestDetails:" + Convert.ToString(objMoveToArchive.match_id);
                IntigateContestCache.DeleteCache(contestDetailsCache);

                string newContestDetailsCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + "Contest:NewContestDetails:" + Convert.ToString(objMoveToArchive.match_id);
                IntigateContestCache.DeleteCache(newContestDetailsCache);

                string currentContestMappingCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + "Contest:CurrentContestMapping:" + Convert.ToString(objMoveToArchive.match_id);
                IntigateContestCache.DeleteCache(currentContestMappingCache);

                string joinContestCountCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + "JoinedContestCount:" + Convert.ToString(objMoveToArchive.match_id);
                IntigateContestCache.DeleteCache(joinContestCountCache);

                string newLiveContestDetailsCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + "Contest:NewLiveContestDetails:" + Convert.ToString(objMoveToArchive.match_id);
                IntigateContestCache.DeleteCache(newLiveContestDetailsCache);

            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveUserWinningAmount(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                string[] notificationParam = { "@MatchId" };
                DataTable data = transactionDBAccess.GetDataFromStoredProcedure("[dbo].[uspGetUserTotalWinningByMatchId]", notificationParam, objMoveToArchive.match_id);
                if (data.Rows.Count > 0)
                {
                    string[] paramName = { "@dtUserWinningAmount" };
                    var result = dBAccess.SaveDataStoredProcedure("[dbo].[uspSaveUserTotalWinningByMatchId]", paramName, data);
                }

                GetUserMatchContestTeamCount(objMoveToArchive);

            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        //public void GetUserMatchTeamCount(JsonMoveToArchive objMoveToArchive)
        //{
        //    try
        //    {
        //        string[] notificationParam = { "@MatchId" };
        //        var data = dBAccess.GetDataFromStoredProcedure<ArchiveUserMatchTeamCount>("[dbo].[uspGetUserContestAndTeamCount]", notificationParam, objMoveToArchive.match_id).ToList();
        //        if (data.Count > 0)
        //        {
        //            var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
        //            var archiveArchiveUserMatchTeamCountCollection = databaseArchive.GetCollection<ArchiveUserMatchTeamCount>("ArchiveUserMatchTeamCount");
        //            var filter = Builders<ArchiveUserMatchTeamCount>.Filter.Eq("MatchId", objMoveToArchive.match_id);
        //            var _List = archiveArchiveUserMatchTeamCountCollection.Find(filter).ToList();
        //            if (_List.Count == 0)
        //            {
        //                archiveArchiveUserMatchTeamCountCollection.InsertMany(data);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
        //    }
        //}

        public void GetUserMatchContestTeamCount(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                string[] notificationParam = { "@MatchId" };
                var data = dBAccess.GetDataFromStoredProcedure<ArchiveUserMatchContestTeamCount>("[dbo].[uspGetUserMatchContestTeamCountByMatchId]", notificationParam, objMoveToArchive.match_id).ToList();
                if (data.Count > 0)
                {
                    var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                    var archiveArchiveUserMatchTeamCountCollection = databaseArchive.GetCollection<ArchiveUserMatchContestTeamCount>("ArchiveUserMatchContestTeamCount");
                    var filter = Builders<ArchiveUserMatchContestTeamCount>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    var _List = archiveArchiveUserMatchTeamCountCollection.Find(filter).ToList();
                    if (_List.Count == 0)
                    {
                        archiveArchiveUserMatchTeamCountCollection.InsertMany(data);

                        //for (int _i = 0; _i < (data.Count / 10000) + 1; _i++)
                        //{
                        //    var _userUserMatchTeamCountList = data.Skip(_i * 10000).Take(10000).ToList();
                        //    if (_userUserMatchTeamCountList.Count > 0)
                        //    {

                        //        archiveArchiveUserMatchTeamCountCollection.InsertMany(_userUserMatchTeamCountList);
                        //    }
                        //}


                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        #region "Methods for rank correction and adding winning prize"

        public void GetContestUserTeamToArchive(long MatchContestId)
        {
            try
            {

                //var dbList = dbClient.ListDatabases().ToList();p-
                //var cache = new CacheService();
                //string livematchcontest = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContest"] + Convert.ToString(objMoveToArchive.match_id);

                //var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                //var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");
                //var archiveContestUserTeamCollectionBackup = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam_" + MatchContestId);
                var archiveLeaguesPrizeBreakupCollection = databaseArchive.GetCollection<MatchContestPrizeBreakUpData>("ArchiveMatchContestPrizeBreakup");
                var filter = Builders<MatchContestPrizeBreakUpData>.Filter.Eq("MatchContestId", MatchContestId);
                var priceBreakupList = archiveLeaguesPrizeBreakupCollection.Find(filter).ToList();



                //var archiveMatchCollection = databaseArchive.GetCollection<LiveMatches>("ArchiveMatches");

                //var matchfilter = Builders<LiveMatches>.Filter.Eq("dbId", objMoveToArchive.match_id);
                //var _matchList = archiveMatchCollection.Find(matchfilter).ToList();

                //var contestCollection = databaseArchive.GetCollection<MatchContest>("ArchiveContest");
                //var contestfilter = Builders<MatchContest>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                //var _matchContestList = contestCollection.Find(contestfilter).ToList();


                var filter1 = Builders<MatchContestUserTeams>.Filter.Eq("MatchContestId", MatchContestId);
                var _contestUserTeamList1 = archiveContestUserTeamCollection.Find(filter1).ToList();

                var _contestUserTeamList = _contestUserTeamList1.Where(x => x.isInWinning_Jone == true).OrderBy(m => m.rank).ToList();
                //if (_contestUserTeamList.Count > 0)
                //{
                //    //archiveContestUserTeamCollectionBackup.InsertMany(_contestUserTeamList);
                //}
                //var ranking = _contestUserTeamList.GroupBy(d => d.MatchContestId)
                //                  .SelectMany(g => g.OrderByDescending(y => y.FantacyPoints)
                //                     .Select((x, i) => new { points = x.FantacyPoints, rank = i + 1 })).ToList();
                //var _result = _contestUserTeamList.Select(x=> new 
                //{
                //    x.FantacyPoints
                //}).GroupBy(x=>x.FantacyPoints).OrderByDescending(x => x.FantacyPoints)

                //var priceBreakupList = contestPriceBreakupupdate.Where(x => x.MatchContestId == 181900).ToList();
                foreach (var contestTeam in _contestUserTeamList)
                {
                    if (priceBreakupList.Count > 0)
                    {
                        //var ranking = _contestUserTeamList.GroupBy(d => d.MatchContestId)
                        //          .SelectMany(g => g.OrderByDescending(y => y.FantacyPoints)
                        //             .Select((x, i) => new { points = x.FantacyPoints, rank = i + 1 })).ToList();

                        long? rank = contestTeam.rank; //ranking.Where(x => x.points == contestTeam.FantacyPoints).FirstOrDefault().rank;
                                                       //contestTeam.rank = rank;
                                                       //contestTeam.isInWinning_Jone = priceBreakupList.Where(x => x.EndRank >= rank).ToList().Count > 0 ? true : false;

                        //contestTeam.winning_Amount = priceBreakupList.Where(x => x.EndRank >= rank).ToList().Count > 0 ? Convert.ToDecimal(priceBreakupList.Where(x => x.EndRank >= rank).FirstOrDefault().Prize) : 0;
                        //contestTeam.IsWinningAmountAdded = false;
                        ///update LeaguesUserTeam data
                        var contestUserTeamUpdateDefinition = Builders<MatchContestUserTeams>.Filter.Eq(x => x.MatchContestUserTeamId, contestTeam.MatchContestUserTeamId);
                        //var contestUserTeamUpdateFilter = Builders<MatchContestUserTeams>.Update.Set(q => q.isInWinning_Jone, contestTeam.isInWinning_Jone).Set(q => q.winning_Amount, contestTeam.winning_Amount).Set(q => q.rank, contestTeam.rank);
                        var contestUserTeamUpdateFilter = Builders<MatchContestUserTeams>.Update.Set(q => q.isInWinning_Jone, contestTeam.isInWinning_Jone).Set(q => q.rank, contestTeam.rank);
                        archiveContestUserTeamCollection.UpdateOne(contestUserTeamUpdateDefinition, contestUserTeamUpdateFilter);
                    }

                }
                //var distinctRank = _contestUserTeamList.Select(x => new
                //{
                //    x.rank,
                //    x.MatchContestId
                //}).Distinct().OrderByDescending(x => x.MatchContestId).ThenByDescending(x => x.rank);





            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveApproveContestResultInSqlDbBack(long MatchContestId)
        {
            try
            {
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");
                var filter = Builders<MatchContestUserTeams>.Filter.Eq("MatchContestId", MatchContestId);
                filter = filter & Builders<MatchContestUserTeams>.Filter.Eq("isInWinning_Jone", true);
                filter = filter & Builders<MatchContestUserTeams>.Filter.Where(x => x.winning_Amount > 0);
                var _matchContestUserTeamsList = archiveContestUserTeamCollection.Find(filter).ToList();
                if (_matchContestUserTeamsList.Count > 0)
                {
                    List<ApproveContestResult> approveContestResultList = new List<ApproveContestResult>();
                    foreach (var cut in _matchContestUserTeamsList)
                    {
                        ApproveContestResult approveContestResult = new ApproveContestResult();
                        approveContestResult.UserId = cut.UserId;
                        approveContestResult.MatchId = cut.MatchId;
                        approveContestResult.MatchContestId = cut.MatchContestId;
                        approveContestResult.UserTeamId = cut.UserTeamId;
                        approveContestResult.TeamName = cut.TeamAbbr;
                        approveContestResult.FantacyPoints = cut.FantacyPoints;
                        approveContestResult.WinningAmount = cut.winning_Amount;
                        approveContestResult.IsInWinning_Jone = cut.isInWinning_Jone;
                        approveContestResult.Rank = cut.rank;
                        approveContestResultList.Add(approveContestResult);
                    }

                    if (approveContestResultList.Count > 0)
                    {
                        try
                        {
                            string[] paramName = { "@dtApproveContestResult" };
                            var result = transactionDBAccess.GetDataFromStoredProcedure<ProcSaveApproveContestResult>("[dbo].[PROC_SAVE_APPROVE_CONTEST_RESULT_BACK]", paramName, DataTableExtension.ConvertListToDataTable(approveContestResultList));
                        }
                        catch (Exception ex)
                        {
                            Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }

        }

        public void SaveContestUserTeamToArchiveForCorrection(long MatchContestId)
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();

                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                //var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                var archiveContestUserTeamCollection = databaseArchive.GetCollection<MatchContestUserTeams>("ArchiveContestUserTeam");


                var filter = Builders<MatchContestUserTeams>.Filter.Eq("MatchContestId", MatchContestId);
                filter = filter & Builders<MatchContestUserTeams>.Filter.Eq("isInWinning_Jone", true);
                var _contestUserTeamList = archiveContestUserTeamCollection.Find(filter).ToList();

                var distinctRank = _contestUserTeamList.Select(x => new
                {
                    x.rank,
                    x.MatchContestId
                }).Distinct().OrderByDescending(x => x.MatchContestId).ThenByDescending(x => x.rank);



                try
                {
                    List<MatchContestUserTeams> matchContestUserTeamsList = new List<MatchContestUserTeams>();
                    foreach (var r in distinctRank.Where(y => y.MatchContestId == MatchContestId))
                    {
                        int rankCount = _contestUserTeamList.Where(x => x.rank == r.rank && x.MatchContestId == r.MatchContestId).Count();
                        foreach (var cut in _contestUserTeamList.Where(x => x.rank == r.rank && x.MatchContestId == r.MatchContestId))
                        {
                            MatchContestUserTeams matchContestUserTeams = new MatchContestUserTeams();
                            matchContestUserTeams.winning_Amount = GetWinnigPrizeArchive(cut.MatchContestId, cut.rank, rankCount);

                            var contestUserTeamUpdateDefinition = Builders<MatchContestUserTeams>.Filter.Eq(x => x.MatchContestUserTeamId, cut.MatchContestUserTeamId);
                            var contestUserTeamUpdateFilter = Builders<MatchContestUserTeams>.Update.Set(q => q.winning_Amount, matchContestUserTeams.winning_Amount);
                            archiveContestUserTeamCollection.UpdateOne(contestUserTeamUpdateDefinition, contestUserTeamUpdateFilter);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
                }



            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public void SaveMatchTeamPlayerInfoToArchiveTest(long match_id)
        {
            try
            {
                var archiveDatabase = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var archiveMatchTeamPlayerBackupcollection = archiveDatabase.GetCollection<TeamPlayer>("ArchiveMatchTeamPlayerInfoBackup");
                var archiveMatchTeamPlayercollection = archiveDatabase.GetCollection<TeamPlayer>("ArchiveMatchTeamPlayerInfo");
                var matchTeamPlayerFilter = Builders<TeamPlayer>.Filter.Eq("MatchId", match_id);
                var _matchTeamPlayerMongoList = archiveMatchTeamPlayerBackupcollection.Find(matchTeamPlayerFilter).ToList();

                string[] paramScheduleName = { "@MatchId" };
                var _result = dBAccess.GetDataFromStoredProcedure<UpdateId>("[dbo].[PROC_GetMatchTeamPlayer_TestMove]", paramScheduleName, match_id).ToList();

                if (_matchTeamPlayerMongoList.Count > 0 && _result.Count > 0)
                {

                    var newList = from x in _matchTeamPlayerMongoList
                                  from lm in _result
                                  where (x.MatchId == lm.MatchId && x.MatchTeamPlayerId == lm.MatchTeamPlayerId)
                                  select new TeamPlayer
                                  {
                                      _id = lm.Id,
                                      MatchTeamPlayerId = x.MatchTeamPlayerId,
                                      MatchId = x.MatchId,
                                      TeamId = x.TeamId,
                                      TeamName = x.TeamName,
                                      Team_Abbr = x.Team_Abbr,
                                      Name = x.Name,
                                      Playing_Role = x.Playing_Role,
                                      SelectedBy = x.SelectedBy,
                                      CaptainBy = x.CaptainBy,
                                      ViceCaptainBy = x.ViceCaptainBy,
                                      FantacyPoints = x.FantacyPoints,
                                      Rating = x.Rating,
                                      PlayerImage = x.PlayerImage,
                                      IsAnnounced = x.IsAnnounced,
                                      starting11 = x.starting11,
                                      run = x.run,
                                      four = x.four,
                                      six = x.six,
                                      sr = x.sr,
                                      fifty = x.fifty,
                                      duck = x.duck,
                                      wkts = x.wkts,
                                      maidenover = x.maidenover,
                                      er = x.er,
                                      _catch = x._catch,
                                      runoutstumping = x.runoutstumping,
                                      runoutthrower = x.runoutthrower,
                                      runoutcatcher = x.runoutcatcher,
                                      directrunout = x.directrunout,
                                      stumping = x.stumping,
                                      thirty = x.thirty,
                                      bonus = x.bonus,
                                      Credit = x.Credit
                                  };
                    //var newList = _matchTeamPlayerMongoList.Select(x => new TeamPlayer
                    //{
                    //    _id = -1 * x._id,
                    //    MatchTeamPlayerId = x.MatchTeamPlayerId,
                    //    MatchId = x.MatchId,
                    //    TeamId = x.TeamId,
                    //    TeamName = x.TeamName,
                    //    Team_Abbr = x.Team_Abbr,
                    //    Name = x.Name,
                    //    Playing_Role = x.Playing_Role,
                    //    SelectedBy = x.SelectedBy,
                    //    CaptainBy = x.CaptainBy,
                    //    ViceCaptainBy = x.ViceCaptainBy,
                    //    FantacyPoints = x.FantacyPoints,
                    //    Rating = x.Rating,
                    //    PlayerImage = x.PlayerImage,
                    //    IsAnnounced = x.IsAnnounced,
                    //    starting11 = x.starting11,
                    //    run = x.run,
                    //    four = x.four,
                    //    six = x.six,
                    //    sr = x.sr,
                    //    fifty = x.fifty,
                    //    duck = x.duck,
                    //    wkts = x.wkts,
                    //    maidenover = x.maidenover,
                    //    er = x.er,
                    //    _catch = x._catch,
                    //    runoutstumping = x.runoutstumping,
                    //    runoutthrower = x.runoutthrower,
                    //    runoutcatcher = x.runoutcatcher,
                    //    directrunout = x.directrunout,
                    //    stumping = x.stumping,
                    //    thirty = x.thirty,
                    //    bonus = x.bonus,
                    //    Credit = x.Credit

                    //}).ToList();
                    if (newList.ToList().Count > 0)
                    {


                        archiveMatchTeamPlayercollection.InsertMany(newList.ToList());
                        //matchTeamPlayercollection.DeleteMany(matchTeamPlayerFilter);

                        //IntigateCache.DeleteCache(livematchplayerinfocache);
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        private decimal GetWinnigPrizeArchive(Int64 cid, Int64? rank, int rankcount)
        {
            Int64 startrank = rank ?? 0;
            Int64 endrank = startrank + rankcount - 1;
            var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
            var contestPriceBreakupcollection = database.GetCollection<MatchContestPrizeBreakUpData>("ArchiveMatchContestPrizeBreakup");
            //List<MatchContestUserTeams> matchContestUserTeamsList = new List<MatchContestUserTeams>();
            var contestPriceBreakupfilter = Builders<MatchContestPrizeBreakUpData>.Filter.Eq(x => x.MatchContestId, cid);
            var contestPriceBreakupupdate = contestPriceBreakupcollection.Find(contestPriceBreakupfilter).ToList();
            decimal amount = 0;
            for (var i = rank; i <= endrank; i++)
            {
                var obj = contestPriceBreakupupdate.Where(x => x.EndRank >= i && x.StartRank <= i).FirstOrDefault();
                if (obj != null)
                    amount = amount + (obj.Prize ?? 0);
            }
            amount = Math.Round((amount / rankcount), 2);
            return amount;
        }

        public void TeamPlayerBackup()
        {
            var archiveDatabase = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
            var archiveMatchTeamPlayercollection = archiveDatabase.GetCollection<TeamPlayer>("ArchiveMatchTeamPlayerInfo");
            var matchTeamPlayerFilter = Builders<TeamPlayer>.Filter.Empty;
            var _matchTeamPlayerMongoList = archiveMatchTeamPlayercollection.Find(matchTeamPlayerFilter).ToList();

            if (_matchTeamPlayerMongoList.Count > 0)
            {
                var archiveMatchTeamBackupPlayercollection = archiveDatabase.GetCollection<TeamPlayer>("ArchiveMatchTeamPlayerInfoBackup");
                archiveMatchTeamBackupPlayercollection.InsertMany(_matchTeamPlayerMongoList);
                archiveMatchTeamPlayercollection.DeleteMany(matchTeamPlayerFilter);


                GetMatchesFromArchive();
            }
        }

        public void GetMatchesFromArchive()
        {
            try
            {
                //var dbList = dbClient.ListDatabases().ToList();


                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var archiveMatchCollection = databaseArchive.GetCollection<LiveMatchInfo>("ArchiveMatches");

                var filter = Builders<LiveMatchInfo>.Filter.Empty;
                var _matchList = archiveMatchCollection.Find(filter).ToList();


                if (_matchList.Count > 0)
                {
                    foreach (var item in _matchList)
                    {
                        JsonMoveToArchive jsonMoveToArchive = new JsonMoveToArchive();
                        jsonMoveToArchive.match_id = item.dbId;
                        SaveUserMatchTeamCount(jsonMoveToArchive);
                        //SaveMatchTeamPlayerInfoToArchiveTest(item.dbId);
                    }

                }

            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        public bool IsCompleteMatchExists(JsonMoveToArchive objMoveToArchive)
        {
            bool isExists = false;
            try
            {

                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);
                var liveMatchCollection = database.GetCollection<LiveMatchInfo>("Matches");
                var liveMatchFilter = Builders<LiveMatchInfo>.Filter.Eq("dbId", objMoveToArchive.match_id);
                var _liveMatchList = liveMatchCollection.Find(liveMatchFilter).ToList();

                var archiveCompletedMatchCollection = databaseArchive.GetCollection<CompletedMatchInfo>("CompletedMatches");

                var filter = Builders<CompletedMatchInfo>.Filter.Eq("dbId", objMoveToArchive.match_id);
                var _matchList = archiveCompletedMatchCollection.Find(filter).ToList();

                if (_matchList.Count > 0)
                {
                    isExists = true;
                }
                else
                {
                    LiveMatchInfo liveMatches = new LiveMatchInfo();
                    if (_liveMatchList.Count > 0)
                    {
                        liveMatches = _liveMatchList.FirstOrDefault();
                        CompletedMatchInfo completedMatchInfo = new CompletedMatchInfo();
                        completedMatchInfo._id = liveMatches._id;
                        completedMatchInfo.dbId = liveMatches.dbId;
                        archiveCompletedMatchCollection.InsertOne(completedMatchInfo);
                    }

                    isExists = false;
                }

            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }

            return isExists;
        }

        public void GetIplMatchContestCashbackUser(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                string[] paramScheduleName = { "@match_id" };
                DataTable dt = dBAccess.GetDataFromStoredProcedure("[dbo].[PROC_GET_IPL_MATCH_CASHBACK_USERS]", paramScheduleName, objMoveToArchive.match_id);
                if (dt.Rows.Count > 0)
                {
                    string[] param = { "@dtIplCashbackUser" };
                    int rowCount = transactionDBAccess.SaveDataStoredProcedure("[dbo].[PROC_SAVE_IPL_MATCH_CASHBACK]", param, dt);

                    SendPushNotificationForCashback(dt);
                }


            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void SendPushNotificationForCashback(DataTable dataTable)
        {
            try
            {                
                List<UserScratchCard> userScratchCardList = new List<UserScratchCard>();
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    UserScratchCard userScratchCard = new UserScratchCard
                    {
                        UserId = Convert.ToInt64(dataRow["UserId"])
                    };

                    userScratchCardList.Add(userScratchCard);
                }

                if (userScratchCardList.Count > 0)
                {
                    string[] param = { "@dtScratchCardUsers" };
                    var result = this.authDBAccess.GetDataFromStoredProcedure<UserToken>("[dbo].[PROC_GetScratchCardUsersForNotification]", param, DataTableExtension.ConvertListToDataTable(userScratchCardList)).ToList();
                    foreach (var item in result)
                    {
                        SendPushNotificationForCashback(item.token);
                    }

                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void SendPushNotificationForCashback(string token)
        {
            try
            {  

                var message = new Message()
                {
                    Notification = new Notification()
                    {
                        //Title = "TOC vs KAC | Lineups Out 🏏",
                        //Body = "The 8:00pm deadline is almost here! Have you created your team yet?",
                        Title = "🔥 Cash Prize Credited in your Wallet",
                        Body = "🤑 Cash Prize credited in your wallet, go to wallet and scratch the card and get your 🔥 Cash Prize",
                    },
                    Token = token
                };


                //Console.WriteLine(defaultApp.Name);
                // Send a message to the device corresponding to the provided
                // registration token.
                try
                {

                    var messaging = FirebaseMessaging.DefaultInstance;
                    string res = messaging.SendAsync(message).ConfigureAwait(true).GetAwaiter().GetResult();
                    // Response is a message ID string.
                    // Console.WriteLine("Successfully sent message: " + res);
                }
                catch (Exception ex)
                {

                    Library.WriteErrorLog(ex);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }


        #endregion "End Methods for rank correction and adding winning prize"

        #region "Delete match details from live database"

        public void DeleteMatchFromLive(JsonMoveToArchive objMoveToArchive)
        {
            try
            {
                using (var cache = new CacheService())
                {

                    /* Matches Information */

                    string livematchescache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatches"];
                    string livematchinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchInfo"];  //"Test_LiveMatchInfo";

                    var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                    var liveMatchCollection = database.GetCollection<LiveMatchInfo>("Matches");


                    var filter = Builders<LiveMatchInfo>.Filter.Eq("dbId", objMoveToArchive.match_id);
                    var _matchList = liveMatchCollection.Find(filter).ToList();

                    LiveMatchInfo liveMatches = new LiveMatchInfo();
                    if (_matchList.Count > 0)
                    {
                        liveMatchCollection.FindOneAndDelete(filter);

                        var _liveMatchCacheData = IntigateCache.RetrieveCacheByCacheName<LiveMatchCache>(livematchescache, cache);
                        _liveMatchCacheData = _liveMatchCacheData.Where(x => x.dbId != objMoveToArchive.match_id).ToList();
                        if (_liveMatchCacheData.Count > 0)
                        {
                            IntigateCache.SaveToCache<LiveMatchCache>(cache, livematchescache, _liveMatchCacheData);
                        }

                        var _liveMatchInfoCacheData = IntigateCache.RetrieveCacheByCacheName<LiveMatchInfo>(livematchinfocache, cache);
                        _liveMatchInfoCacheData = _liveMatchInfoCacheData.Where(x => x.dbId != objMoveToArchive.match_id).ToList();

                        if (_liveMatchInfoCacheData.Count > 0)
                        {
                            IntigateCache.SaveToCache<LiveMatchInfo>(cache, livematchinfocache, _liveMatchInfoCacheData);
                        }

                    }

                    /* End Matches Information */

                    /* MatchTeamPlayerInfo */
                    string livematchplayerinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(objMoveToArchive.match_id);

                    var matchTeamPlayercollection = database.GetCollection<TeamPlayer>("MatchTeamPlayerInfo");
                    var matchTeamPlayerFilter = Builders<TeamPlayer>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    var _matchTeamPlayerMongoList = matchTeamPlayercollection.Find(matchTeamPlayerFilter).ToList();

                    if (_matchTeamPlayerMongoList.Count > 0)
                    {
                        matchTeamPlayercollection.DeleteMany(matchTeamPlayerFilter);
                        IntigateCache.DeleteCache(livematchplayerinfocache);

                    }

                    /* End MatchTeamPlayerInfo */

                    /* UserTeamPoints */

                    var liveUserTeamPointCollection = database.GetCollection<UserTeamPoints>("UserTeamPoints");
                    var _usetTeamPointsFilter = Builders<UserTeamPoints>.Filter.Eq("MatchId", objMoveToArchive.match_id);


                    var _userTeamPointsList = liveUserTeamPointCollection.Find(_usetTeamPointsFilter).ToList();
                    if (_userTeamPointsList.Count > 0)
                    {
                        liveUserTeamPointCollection.DeleteMany(_usetTeamPointsFilter);

                    }

                    /* End UserTeamPoints */

                    /* UserTeamPlayers */
                    var liveUserTeamPlayerCollection = database.GetCollection<UserTeamPlayersLive>("UserTeamPlayers");
                    var _userTeamPlayerFilter = Builders<UserTeamPlayersLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);

                    var _userTeamPlayersList = liveUserTeamPlayerCollection.Find(_userTeamPlayerFilter).ToList();
                    if (_userTeamPlayersList.Count > 0)
                    {
                        liveUserTeamPlayerCollection.DeleteMany(_userTeamPlayerFilter);
                    }
                    /* End UserTeamPlayers */


                    /* Conest */

                    string livematchcontest = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContest"] + Convert.ToString(objMoveToArchive.match_id);
                    var liveContestCollection = database.GetCollection<MatchContest>("Contest");

                    var _contestFilter = Builders<MatchContest>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    var _contestList = liveContestCollection.Find(_contestFilter).ToList();

                    if (_contestList.Count > 0)
                    {
                        liveContestCollection.DeleteMany(_contestFilter);
                        IntigateCache.DeleteCache(livematchcontest);

                        //foreach (var item in _contestList)
                        //{
                        //    string cacheName = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchLeaderBoard"] + Convert.ToString(objMoveToArchive.match_id) + ":" + Convert.ToString(item.dbId);
                        //    IntigateCache.DeleteCache(cacheName);
                        //}

                    }

                    /* End Contest */

                    /* ContestUserTeam */

                    string livematchcontestuserteamcache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContestUserTeam"] + Convert.ToString(objMoveToArchive.match_id);

                    var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                    var _contestUserTeamFilter = Builders<MatchContestUserTeamsMongoLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    var _contestUserTeamList = liveContestUserTeamCollection.Find(_contestUserTeamFilter).ToList();

                    if (_contestUserTeamList.Count > 0)
                    {
                        liveContestUserTeamCollection.DeleteMany(_contestUserTeamFilter);
                        IntigateCache.DeleteCache(livematchcontestuserteamcache);
                    }

                    /* End ContestUserTeam */

                    /* MatchContestPrizeBreakup */

                    var liveLeaguesPrizeBreakupCollection = database.GetCollection<MatchContestPrizeBreakUpDataMongoLive>("MatchContestPrizeBreakup");

                    var _prizeBreakUpfilter = Builders<MatchContestPrizeBreakUpDataMongoLive>.Filter.Eq("MatchId", objMoveToArchive.match_id);
                    var _leaguePrizeBreakupList = liveLeaguesPrizeBreakupCollection.Find(_prizeBreakUpfilter).ToList();

                    List<MatchContestPrizeBreakUpData> matchContestPrizeBreakUpDataList = new List<MatchContestPrizeBreakUpData>();
                    if (_leaguePrizeBreakupList.Count > 0)
                    {
                        liveLeaguesPrizeBreakupCollection.DeleteMany(_prizeBreakUpfilter);
                    }

                    /* End MatchContestPrizeBreakup */
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "ArchiveMessageReceiver");
            }
        }

        #endregion "End Delete match details from live database"

        public void GetContestForCancel()
        {
            List<ContestForCancel> _list = new List<ContestForCancel>();
            string[] paramScheduleName = {  };
            var dt = dBAccess.GetDataFromStoredProcedure<ContestForCancel>("[dbo].[Proc_GetContestForCancel]", paramScheduleName).ToList();
            foreach(var item in dt)
            {
               
                var databaseArchive = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoArchiveDatabase"]);                
                var archiveContestCollection = databaseArchive.GetCollection<MatchContest>("ArchiveContest");

                var filter = Builders<MatchContest>.Filter.Eq("dbId", item.Id);
                //var _contestList = IntigateCache.RetrieveCacheByCacheName<MatchContest>(livematchcontest, cache);
                long _contestList = -1;
                _contestList = archiveContestCollection.Find(filter).Count();
                if(_contestList == 0)
                {
                    ContestForCancel contestForCancel = new ContestForCancel
                    {
                        Id = item.Id
                    };
                    _list.Add(contestForCancel);
                }
            }
            if(_list.Count > 0)
            {
                string[] paramScheduleName1 = { "@dtContestForCorrection" };
                var dt1 = dBAccess.SaveDataStoredProcedure("[dbo].[Proc_SaveContestForCorrection]", paramScheduleName1, DataTableExtension.ConvertListToDataTable(_list));
            }
        }

    }
}
