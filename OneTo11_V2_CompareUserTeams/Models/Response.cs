﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_CompareUserTeams.Models
{

    public class JsonCompareUserTeam
    {
        public Int64 match_id { get; set; }
    }

    public class LiveMatchInfo
    {
        public Int64 _id { get; set; }
        public Int64 dbId { get; set; }
        public string cid { get; set; }
        public string match_id { get; set; }
        public string title { get; set; }
        public Int32 formatId { get; set; }
        public string format_str { get; set; }
        public Int32 status { get; set; }
        public string status_str { get; set; }
        public string status_note { get; set; }
        public string TeamA_Id { get; set; }
        public string TeamA { get; set; }
        public string TeamA_Logo { get; set; }
        public string TeamB_Id { get; set; }
        public string TeamB { get; set; }
        public string TeamB_Logo { get; set; }
        public string ContestName { get; set; }
        public string TeamA_score { get; set; }
        public string TeamA_fullscore { get; set; }
        public string TeamB_score { get; set; }
        public string TeamB_fullscore { get; set; }
        public string TeamA_overs { get; set; }
        public string TeamB_overs { get; set; }
        public string GameTypeId { get; set; }
        public DateTime MatchStartTime { get; set; }
        public DateTime MatchEndTime { get; set; }
        public string seriesname { get; set; }

        public bool IsVerified { get; set; }
    }

    public class ContestDetails
    {
        public long? ContestId { get; set; }
        public long? CategoryId { get; set; }
    }

    public class MatchContestUserTeamsMongoLive
    {
        public long _id { get; set; }
        public long MatchContestUserTeamId { get; set; }
        public long? UserTeamId { get; set; }
        public long? UserId { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long MatchContestId { get; set; }
        public long MatchId { get; set; }
        public bool isInWinning_Jone { get; set; }
        public decimal? winning_Amount { get; set; }
        public long? rank { get; set; }
        public string TeamAbbr { get; set; }
    }

    public class UserTeamPlayersLive
    {
        public long _id { get; set; }
        public long UserTeamPlayerId { get; set; }
        public long MatchTeamPlayerId { get; set; }
        public decimal? FantacyPoints { get; set; }
        public int IsCaptain { get; set; }
        public int IsViceCaptain { get; set; }
        public long MatchId { get; set; }
        public long UserTeamId { get; set; }
    }

    public class UserTeamPlayers
    {
        public long MatchTeamPlayerId { get; set; }
        public int IsCaptain { get; set; }
        public int IsViceCaptain { get; set; }
    }

    public class MatchContest
    {
        public MatchContest()
        {
            //user = new List<MatchLeagueUserTeams>();
        }
        public Int64 _id { get; set; }
        public long dbId { get; set; }
        public long? MatchId { get; set; }
        public string API_MatchId { get; set; }
        public string Title { get; set; }
        public Int32? MaxUsers { get; set; }
        public decimal? WinerPercent { get; set; }
        public string ContestInvitationCode { get; set; }
        public decimal? TotalContestAmount { get; set; }
        public long TotalTeam { get; set; }
        public long? FirstWinner { get; set; }
        public decimal? EntryFee { get; set; }
        public bool IsConfirm { get; set; }
        public bool MultipleEntryAllowed { get; set; }
        public bool IsPractice { get; set; }
        public int? status { get; set; }
        public string status_str { get; set; }
    }

}
