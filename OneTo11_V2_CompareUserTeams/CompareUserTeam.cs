﻿using MongoDB.Driver;
using Newtonsoft.Json;
using OneTo11_V2_CompareUserTeams.Helpers;
using OneTo11_V2_CompareUserTeams.Models;
using OneTo11_V2_Redis;
using OneTo11_V2_Redis.Helpers;
using RabbitMQ.Client;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_CompareUserTeams
{
    public class CompareUserTeam : DefaultBasicConsumer
    {
        private DBAccess dBAccess;
        private readonly IModel _channel;

        string liveconnection = ConfigurationManager.AppSettings["LiveMongoConnection"];
        string connection = ConfigurationManager.AppSettings["ArchiveMongoConnection"];

        string connectionGameString = ConfigurationManager.AppSettings["OneTo11_Game_Connection"];

        MongoClient livedbClient;
        MongoClient dbClient;
        public CompareUserTeam(IModel channel)
        {
            _channel = channel;
            livedbClient = new MongoClient(liveconnection);
            dbClient = new MongoClient(connection);
            dBAccess = new DBAccess(connectionGameString);
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            Library.WriteErrorLog("Compare user team players receiver started");
            JsonCompareUserTeam jsonCompareUserTeam = new JsonCompareUserTeam();

            string jsonData = Encoding.UTF8.GetString(body);
            jsonCompareUserTeam = JsonConvert.DeserializeObject<JsonCompareUserTeam>(jsonData);

            GetContestByMatchId(jsonCompareUserTeam.match_id);
            Library.WriteErrorLog("Compare user team players receiver stop");

            _channel.BasicAck(deliveryTag, false);

        }

        //public void GetLiveMatches(JsonCompareUserTeam jsonCompareUserTeam)
        //{
        //    var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
        //    var liveMatchCollection = database.GetCollection<LiveMatchInfo>("Matches");
        //    var filter = Builders<LiveMatchInfo>.Filter.Eq("dbId", jsonCompareUserTeam.match_id);
        //    var _matchList = liveMatchCollection.Find(filter).ToList();
        //    foreach (var m in _matchList)
        //    {
               
        //    }
        //}

        public void GetContestByMatchId(Int64 match_id)
        {
            try
            {
                string[] param = { "@match_id" };
                var _contestList = this.dBAccess.GetDataFromStoredProcedure<ContestDetails>("PROC_GET_CONTEST_FOR_COMPARE_TEAM", param, match_id).ToList();
                foreach (var c in _contestList)
                {
                    CompareUserTeamPlayers(match_id, c.ContestId);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void CompareUserTeamPlayers(long match_id, long? ContestId)
        {
            try
            {
                var database = livedbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                var filter = Builders<MatchContestUserTeamsMongoLive>.Filter.Eq("MatchContestId", ContestId);
                var _contestUserTeamList = liveContestUserTeamCollection.Find(filter).ToList();
                List<UserTeamPlayers> userTeamPlayersList = new List<UserTeamPlayers>();
                int userTeamCount = 0;
                foreach (var cut in _contestUserTeamList)
                {
                    userTeamCount = userTeamCount + 1;
                    var liveUserTeamPlayerCollection = database.GetCollection<UserTeamPlayersLive>("UserTeamPlayers");
                    var userTeamPlayerFilter = Builders<UserTeamPlayersLive>.Filter.Eq("UserTeamId", cut.UserTeamId);
                    var _userTeamPlayersList = liveUserTeamPlayerCollection.Find(userTeamPlayerFilter).ToList();
                    foreach (var utp in _userTeamPlayersList)
                    {
                        UserTeamPlayers userTeamPlayers = new UserTeamPlayers
                        {
                            MatchTeamPlayerId = utp.MatchTeamPlayerId,
                            IsCaptain = utp.IsViceCaptain,
                            IsViceCaptain = utp.IsViceCaptain
                        };

                        userTeamPlayersList.Add(userTeamPlayers);
                    }
                }

                var result = userTeamPlayersList.GroupBy(x => new { x.MatchTeamPlayerId, x.IsCaptain, x.IsViceCaptain })
                       .Select(g => new { g.Key.MatchTeamPlayerId, g.Key.IsCaptain, g.Key.IsViceCaptain, MyCount = g.Count() });

                var _list = result.Where(x => x.MyCount < userTeamCount).ToList();
                if (_list.Count == 0)
                {
                    /* Cancel contest */
                    var liveContestCollection = database.GetCollection<MatchContest>("Contest");
                    var contestFilter = Builders<MatchContest>.Filter.Eq("dbId", ContestId);
                    var _contestList = liveContestCollection.Find(contestFilter).ToList();
                    if (_contestList.Count > 0)
                    {
                        var contestUpdateFilter = Builders<MatchContest>.Update
                                            .Set(p => p.status, 4)
                                            .Set(p => p.status_str, "Cancelled");
                        liveContestCollection.UpdateOne(contestFilter, contestUpdateFilter);

                        UpdateMatchContestStatus(match_id, ContestId, 4, "Cancelled", "MATCHCONTESTSTATUS");

                        //string contestDetailsCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + "Contest:ContestDetails:" + Convert.ToString(match_id);
                        //IntigateContestCache.DeleteCache(contestDetailsCache);

                        //using (var cache = new ContestCacheService())
                        //{
                        //    string newContestDetailsCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + "Contest:NewContestDetails:" + Convert.ToString(match_id);

                        //    RedisValue[] hashFields = new RedisValue[1];
                        //    RedisValue li = new RedisValue(Convert.ToString(ContestId));
                        //    hashFields.SetValue(li, 0);
                        //    IntigateContestCache.DeleteToCacheHash(cache, newContestDetailsCache, li);
                        //};

                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void CompareWinnerTakesAllTeam(long ContestId)
        {

        }

        public void UpdateMatchContestStatus(long? match_id, long? contest_id, int? status_id, string status_str, string flag)
        {
            int result = 0;
            try
            {
                string[] matchParam = { "@match_id", "@contest_id", "@status_id", "@status_str", "@flag" };
                result = dBAccess.SaveDataStoredProcedure("[dbo].[PROC_UPDATE_MATCH_CONTEST_STATUS]", matchParam
                                             , match_id, contest_id, status_id, status_str, flag);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }
    }
}
