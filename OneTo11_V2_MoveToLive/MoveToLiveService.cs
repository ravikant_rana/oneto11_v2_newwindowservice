﻿using OneTo11_V2_MoveToLive.Helpers;
using OneTo11_V2_MoveToLive.Models;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace OneTo11_V2_MoveToLive
{
    public partial class MoveToLiveService : ServiceBase
    {
        private const string exchangeName = "HelloWorld_RabbitMQ";
        private const string queueName = "HelloQueue";
        private Timer timer1 = null;
        public MoveToLiveService()
        {
            InitializeComponent();
        }

        public void StartMoveToLiveService()
        {
            OnStart(null);
            //Execute(null, null);
            //System.Threading.Thread.Sleep(1000000000);
        }

        protected override void OnStart(string[] args)
        {
            timer1 = new Timer();
            this.timer1.Interval = 1000;  //for 1  second
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.Execute);
            timer1.Enabled = true;
            //Execute(null, null);
        }

        public void Execute(object sender, ElapsedEventArgs e)
        {
            Library.WriteErrorLog("Execute started at: " + DateTime.Now);
            timer1.Enabled = false;

            GetConnectionFactory();

            timer1.Enabled = true;
            Library.WriteErrorLog("Excute Completed at: " + DateTime.Now);
        }

        protected override void OnStop()
        {
        }

        public void GetConnectionFactory()
        {

            ConnectionFactory connectionFactory = new ConnectionFactory
            {

                HostName = ConfigurationManager.AppSettings["MQHostName"],
                UserName = ConfigurationManager.AppSettings["MQUserName"],
                Password = ConfigurationManager.AppSettings["MQPassword"],
                Port = Convert.ToInt32(ConfigurationManager.AppSettings["MQPort"]),
                VirtualHost = ConfigurationManager.AppSettings["MQVirtualHost"]
            };

            var Rconnection = connectionFactory.CreateConnection();

            var Rchannel = Rconnection.CreateModel();

            // accept only one unack-ed message at a time

            // uint prefetchSize, ushort prefetchCount, bool global

            Rchannel.BasicQos(0, 1, false);


            MoveToLiveReceiver moveToLiveReceiver = new MoveToLiveReceiver(Rchannel);
            Rchannel.BasicConsume("oneto11-queue-MoveToLiveSpark", false, moveToLiveReceiver);
            //moveToLiveReceiver.SendMatchContestUserTeamDataToLive(3743);
            //JsonMoveToLive jsonMoveToLive = new JsonMoveToLive();
            //jsonMoveToLive.match_id = 1037;
            //moveToLiveReceiver.Test(jsonMoveToLive);
            //moveToLiveReceiver.CallNotFullyFillContestPrizeBreakUp(jsonMoveToLive.match_id);
            //moveToLiveReceiver.SaveLiveGamesInCache(Convert.ToInt64(jsonMoveToLive.match_id));
            //moveToLiveReceiver.SaveLiveMatchContestInCache(Convert.ToInt64(jsonMoveToLive.match_id));
            //moveToLiveReceiver.SaveLiveUserTeamInCache(1942);
            //moveToLiveReceiver.SaveContestUserTeamInCache(1942);
            //moveToLiveReceiver.GeneratePDF(1497);



            Library.WriteErrorLog("Execute Completed at: " + DateTime.Now);

            System.Threading.Thread.Sleep(86400000); // For 24 hours
        }
    }
}
