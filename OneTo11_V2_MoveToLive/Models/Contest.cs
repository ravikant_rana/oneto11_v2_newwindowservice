﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToLive.Models
{
    public class MatchContest
    {
        public MatchContest()
        {
            //user = new List<MatchLeagueUserTeams>();
        }
        public Int64 _id { get; set; }
        public long dbId { get; set; }
        public long? MatchId { get; set; }
        public string API_MatchId { get; set; }
        public string Title { get; set; }
        public Int32? MaxUsers { get; set; }
        public decimal? WinerPercent { get; set; }
        public string ContestInvitationCode { get; set; }
        public decimal? TotalContestAmount { get; set; }
        public long TotalTeam { get; set; }
        public long? FirstWinner { get; set; }
        public decimal? EntryFee { get; set; }
        public bool IsConfirm { get; set; }
        public bool MultipleEntryAllowed { get; set; }
        public bool IsPractice { get; set; }
        public int? status { get; set; }
        public string status_str { get; set; }
    }

    public class MatchContestData
    {
        public long ContestId { get; set; }
        public long? MatchId { get; set; }
        public string API_MatchId { get; set; }
        public string Title { get; set; }
        public Int32? MaxUsers { get; set; }
        public decimal? WinerPercent { get; set; }
        public string ContestInvitationCode { get; set; }
        public decimal? TotalContestAmount { get; set; }
        public long TotalTeam { get; set; }
        public long? FirstWinner { get; set; }
        public decimal? EntryFee { get; set; }
        public bool IsConfirm { get; set; }
        public bool MultipleEntryAllowed { get; set; }
        public bool IsPractice { get; set; }
        public int? Status { get; set; }
        public string status_str { get; set; }
    }

    public class MatchContestUserTeamsData
    {
        public Int64 _id { get; set; }
        public long MatchContestUserTeamId { get; set; }
        public long? UserTeamId { get; set; }
        public long? UserId { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long MatchContestId { get; set; }
        public long MatchId { get; set; }
        public string TeamAbbr { get; set; }
    }

    public class MatchContestUserTeams
    {
        public Int64 _id { get; set; }
        public long MatchContestUserTeamId { get; set; }
        public long? UserTeamId { get; set; }
        public long? UserId { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long MatchContestId { get; set; }
        public long MatchId { get; set; }
        public bool isInWinning_Jone { get; set; }
        public decimal? winning_Amount { get; set; }
        public long? rank { get; set; }
        public string TeamAbbr { get; set; }
        public bool IsWinningAmountAdded { get; set; }
    }

    public class MatchContestUserTeamsMongoLive
    {
        public long _id { get; set; }
        public long MatchContestUserTeamId { get; set; }
        public long? UserTeamId { get; set; }
        public long? UserId { get; set; }
        public decimal? FantacyPoints { get; set; }
        public long MatchContestId { get; set; }
        public long MatchId { get; set; }
        public bool isInWinning_Jone { get; set; }
        public decimal? winning_Amount { get; set; }
        public long? rank { get; set; }
        public string TeamAbbr { get; set; }
    }

    public class MatchContestUserTeamsMongoLiveCache
    {
        [Description("team_name")]
        public string tn { get; set; }
        [Description("points")]
        public decimal? pts { get; set; }
        [Description("user_name")]
        //public string un { get; set; }
        //[Description("team_key")]
        public long? tk { get; set; }
        [Description("rank")]
        public long? rank { get; set; }
        [Description("user_id")]
        public long? uid { get; set; }
        [Description("avtar_id")]
        public long? ai { get; set; }
        [Description("isInWinning_Jone")]
        public bool iwj { get; set; }

    }
}
