﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToLive.Models
{
    public class JsonMoveToLive
    {
        public long match_id { get; set; }
    }

    public class GetDataForContestNotFilled
    {
        public Int64? MatchId { get; set; }
        public Int64? MatchContestId { get; set; }
    }

    public class GetDataForNotification
    {
        public Int64 UserId { get; set; }
        public Int64? MatchId { get; set; }
        public Int64 MatchContestId { get; set; }
        public string TeamA { get; set; }
        public string TeamB { get; set; }
        public string date_start { get; set; }
    }

    public class ResizePendingMatch
    {
        public long MatchId { get; set; }
        public string ApiMatchId { get; set; }
        public string Title { get; set; }
        public DateTime MatchStartTime { get; set; }
    }
}
