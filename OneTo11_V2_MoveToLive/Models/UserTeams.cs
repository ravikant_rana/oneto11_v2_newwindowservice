﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToLive.Models
{
    public class PROC_GetUserTeamPlayer_Result
    {
        public string TeamAbbr { get; set; }
        public long UserTeamId { get; set; }
        public string Captain { get; set; }
        public string ViceCaptain { get; set; }
        public string Player3 { get; set; }
        public string Player4 { get; set; }
        public string Player5 { get; set; }
        public string Player6 { get; set; }
        public string Player7 { get; set; }
        public string Player8 { get; set; }
        public string Player9 { get; set; }
        public string Player10 { get; set; }
        public string Player11 { get; set; }
    }
}
