﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToLive.Models
{
    public class AzureStorageAppSetting
    {
        //public string AzureStorageConnection { get; set; }
        //public string AzureStorageAccountPath { get; set; }
        public string ConnectionString { get; set; }
        public string OuterDirectory { get; set; }
        public string ProfileDirectory { get; set; }
        public string PatientDocumentDirectory { get; set; }
    }

    public class ImageFileDto : FileDto
    {
        public string FileContainerName { get; set; }
        public string FilePrefix { get; set; }
        public bool IsChangeFileName { get; set; }
        public string FileFolderName { get; set; }
        public Int64 mid { get; set; }
    }

    public class FileDto
    {
        public string FileByteStream { get; set; }
        public string fileName { get; set; }
        public string mimetype { get; set; }
        public string fileType { get; set; }

    }

    public class SaveAdminNotificationRequest
    {
        public long? MatchId { get; set; }
        public long? ContestId { get; set; }
        public Nullable<int> AdminNotificationTypeId { get; set; }
    }

    public class SaveNotificationRequest
    {
        public Nullable<long> LoggedInUserId { get; set; }
        public Nullable<int> NotificationTypeId { get; set; }
        public decimal? Amount { get; set; }
        public string JoiningId { get; set; }
        public string Date { get; set; }
        public string Team1VsTeam2 { get; set; }
        public decimal? Points { get; set; }
        public Nullable<int> Rank { get; set; }
        public string ContestName { get; set; }
        public Nullable<int> NoOfTotalUsersJoinedToday { get; set; }
        public Nullable<int> TotalParticipants { get; set; }
        public Nullable<int> NoOfTotalTeams { get; set; }
        public string Reason { get; set; }
        public string Param1 { get; set; }
        public string Param2 { get; set; }
        public string Param3 { get; set; }
        public string Param4 { get; set; }
        public string Param5 { get; set; }

    }
}
