﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToLive.Models
{
    public class LiveMatches
    {
        public Int64 _id { get; set; }
        public Int64 dbId { get; set; }
    }

    public class LiveMatchInfo
    {
        public Int64 _id { get; set; }
        public Int64 dbId { get; set; }
        public string cid { get; set; }
        public string match_id { get; set; }
        public string title { get; set; }
        public Int32 formatId { get; set; }
        public string format_str { get; set; }
        public Int32 status { get; set; }
        public string status_str { get; set; }
        public string status_note { get; set; }
        public string TeamA_Id { get; set; }
        public string TeamA { get; set; }
        public string TeamA_Logo { get; set; }
        public string TeamB_Id { get; set; }
        public string TeamB { get; set; }
        public string TeamB_Logo { get; set; }
        public string ContestName { get; set; }
        public string TeamA_score { get; set; }
        public string TeamA_fullscore { get; set; }
        public string TeamB_score { get; set; }
        public string TeamB_fullscore { get; set; }
        public string TeamA_overs { get; set; }
        public string TeamB_overs { get; set; }
        public string GameTypeId { get; set; }
        public DateTime MatchStartTime { get; set; }
        public DateTime MatchEndTime { get; set; }
        public string seriesname { get; set; }         
        public bool IsVerified { get; set; }
        

    }

    public class TeamPlayer
    {
        public Int64 _id { get; set; }
        public Int64 MatchTeamPlayerId { get; set; }
        public Int64 MatchId { get; set; }
        public Int64 TeamId { get; set; }
        public string TeamName { get; set; }
        public string Team_Abbr { get; set; }
        public string Name { get; set; }
        public string Playing_Role { get; set; }
        public decimal SelectedBy { get; set; }
        public decimal CaptainBy { get; set; }
        public decimal ViceCaptainBy { get; set; }
        public decimal FantacyPoints { get; set; }
        public string Rating { get; set; }
        public string PlayerImage { get; set; }
        public bool? IsAnnounced { get; set; }
        public string starting11 { get; set; }
        public string run { get; set; }
        public string four { get; set; }
        public string six { get; set; }
        public string sr { get; set; }
        public string fifty { get; set; }
        public string duck { get; set; }
        public string wkts { get; set; }
        public string maidenover { get; set; }
        public string er { get; set; }
        public string _catch { get; set; }
        public string runoutstumping { get; set; }
        public string runoutthrower { get; set; }
        public string runoutcatcher { get; set; }
        public string directrunout { get; set; }
        public string stumping { get; set; }
        public string thirty { get; set; }
        public string bonus { get; set; }
        public decimal? Credit { get; set; }

        //public bool? IsSubstitute { get; set; }
    }

    public class LiveMatchContestUserTeamArray
    {
        //public Int64 M { get; set; }
        //public Int64 U { get; set; }
        public string UT { get; set; }
        public string CUT { get; set; }
        public string CC { get; set; }
    }

    public class LiveMatchUserTeam
    {
        [Description("UserTeamId")]
        public long ut { get; set; }
        [Description("TeamAbbr")]
        public string ta { get; set; }

        [Description("CaptainId")]
        public long c { get; set; }

        [Description("ViceCaptainId")]
        public long vc { get; set; }

        [Description("PlayersId")]
        public string pid { get; set; }
    }

    public class LiveMatchUserTeamCache
    {
        
        [Description("TeamAbbr")]
        public string ta { get; set; }

        [Description("CaptainId")]
        public long c { get; set; }

        [Description("ViceCaptainId")]
        public long vc { get; set; }

        [Description("PlayersId")]
        public List<Players> plid { get; set; }
    }

    public class Players
    {
        public long pid { get; set; }
    }

    public class ContestUserTeamCache
    {
        
        [Description("ContestId")]
        public long cid { get; set; }
        [Description("UserId")]
        public long? uid { get; set; }
    }

}
