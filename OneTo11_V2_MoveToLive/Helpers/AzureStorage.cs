﻿using OneTo11_V2_MoveToLive.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azure = AzureStorage;
namespace OneTo11_V2_MoveToLive.Helpers
{
    public class AzureStorageAccount
    {
        //private AzureStorageAppSetting _azurestorageconfig { get; set; }

        //public AzureStorageAccount(IOptions<AzureStorageAppSetting> azureStorage)
        //{
        //    _azurestorageconfig = azureStorage.Value;
        //}
        AzureStorageAppSetting _azurestorageconfig = new AzureStorageAppSetting();
        public AzureStorageAccount()
        {
            _azurestorageconfig.ConnectionString = ConfigurationManager.AppSettings["AzureConnectionString"];
            _azurestorageconfig.OuterDirectory = ConfigurationManager.AppSettings["AzureOuterDirectory"];
        }

        public string SaveFileOnAzure(ImageFileDto obj)
        {

            string extension = Path.GetExtension(obj.fileName).ToLower();
            try
            {
                Azure.Configurations.AppSettings.StorageAccountConnection = _azurestorageconfig.ConnectionString;
                Azure.BlobModels.FileDto file = new Azure.BlobModels.FileDto();
                file.FileName = obj.fileName;
                byte[] bArray = Convert.FromBase64String(obj.FileByteStream);

                ///////for audio
                if (extension == ".mp3")
                {
                    file.FileName = obj.FilePrefix + DateTime.Now.AddMilliseconds(1).ToString().Replace("/", "").Replace(":", "").Replace(" ", "_").Replace("-", "").Replace("PM", "1").Replace("AM", "0").Replace(" ", "") + ".mp3";
                    file.FileMime = "audio/mp3";
                }
                else if (extension == ".m4a")
                {
                    file.FileName = obj.FilePrefix + DateTime.Now.AddMilliseconds(1).ToString().Replace("/", "").Replace(":", "").Replace(" ", "_").Replace("-", "").Replace("PM", "1").Replace("AM", "0").Replace(" ", "") + ".m4a";
                    file.FileMime = "audio/m4a";
                }
                else if (extension == ".pdf")
                {
                    file.FileName = obj.FilePrefix + DateTime.Now.AddMilliseconds(1).ToString().Replace("/", "").Replace(":", "").Replace(" ", "_").Replace("-", "").Replace("PM", "1").Replace("AM", "0").Replace(" ", "") + ".pdf";
                    file.FileMime = "application/pdf";
                }
                ///////for video 
                else if (extension == ".mp4")
                {
                    file.FileName = obj.FilePrefix + DateTime.Now.AddMilliseconds(1).ToString().Replace("/", "").Replace(":", "").Replace(" ", "_").Replace("-", "").Replace("PM", "1").Replace("AM", "0").Replace(" ", "") + ".mp4";
                    file.FileMime = "video/mp4";
                }
                else if (extension == ".avi")
                {
                    file.FileName = obj.FilePrefix + DateTime.Now.AddMilliseconds(1).ToString().Replace("/", "").Replace(":", "").Replace(" ", "_").Replace("-", "").Replace("PM", "1").Replace("AM", "0").Replace(" ", "") + ".avi";
                    file.FileMime = "video/avi";
                }
                else if (extension == ".mov")
                {
                    file.FileName = obj.FilePrefix + DateTime.Now.AddMilliseconds(1).ToString().Replace("/", "").Replace(":", "").Replace(" ", "_").Replace("-", "").Replace("PM", "1").Replace("AM", "0").Replace(" ", "") + ".mov";
                    file.FileMime = "video/quicktime";
                }
                else if (extension == ".jpg" || extension == ".jpeg" || extension == ".png" || extension == ".jfif")
                {
                    file.FileName = obj.FilePrefix + DateTime.Now.AddMilliseconds(1).ToString().Replace("/", "").Replace(":", "").Replace(" ", "_").Replace("-", "").Replace("PM", "1").Replace("AM", "0").Replace(" ", "") + extension;
                    file.FileMime = "image/jpeg";
                }

                file.FileBytes = bArray;
                file.ContainerAccount = obj.FileContainerName;
                file.FolderName = obj.FileFolderName;
                var retrnurl = _azurestorageconfig.OuterDirectory;
                Azure.Core.AzureBlobStorage.UploadFile(file);
                return retrnurl + file.FolderName + "/" + file.FileName;
            }
            catch (Exception ex)
            { return null; }

        }

        public string SavePdfOnAzure(ImageFileDto obj)
        {

            string extension = Path.GetExtension(obj.fileName).ToLower();
            try
            {
                Azure.Configurations.AppSettings.StorageAccountConnection = _azurestorageconfig.ConnectionString;
                Azure.BlobModels.FileDto file = new Azure.BlobModels.FileDto();
                file.FileName = obj.fileName;
                byte[] bArray = Convert.FromBase64String(obj.FileByteStream);

                ///////for audio
                if (extension == ".pdf")
                {
                    //file.FileName = obj.FilePrefix + obj.fileName;
                    file.FileName = obj.fileName;
                    file.FileMime = "application/pdf";
                }

                file.FileBytes = bArray;
                file.ContainerAccount = obj.FileContainerName;
                file.FolderName = obj.FileFolderName;
                var retrnurl = _azurestorageconfig.OuterDirectory;
                Azure.Core.AzureBlobStorage.UploadFile(file);
                return retrnurl + file.FolderName + "/" + file.FileName;
            }
            catch (Exception ex)
            { return null; }

        }


        public string SaveImageOnAzure(ImageFileDto obj)
        {
            try
            {
                Azure.Configurations.AppSettings.StorageAccountConnection = _azurestorageconfig.ConnectionString;
                Azure.BlobModels.FileDto file = new Azure.BlobModels.FileDto();
                file.FileName = obj.fileName;
                byte[] bArray = Convert.FromBase64String(obj.FileByteStream);
                if (obj.IsChangeFileName == true)
                    file.FileName = Convert.ToString(obj.mid) + ".png";
                file.FileMime = "image/png";
                file.FileBytes = bArray;
                file.ContainerAccount = obj.FileContainerName;
                Azure.Core.AzureBlobStorage.UploadFile(file);
                return file.FileName;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void DeleteImage(string fileName, string fileContainer)
        {
            try
            {

                Azure.Configurations.AppSettings.StorageAccountConnection = _azurestorageconfig.ConnectionString;
                Azure.BlobModels.FileInfoDto file = new Azure.BlobModels.FileInfoDto();
                file.FileName = fileName;
                file.ContainerAccount = fileContainer;
                Azure.Core.AzureBlobStorage.DeleteFile(file);
            }
            catch { }
        }

        //public string SaveImageInFolder(ImageFileDto obj)
        //{

        //    string FileByteStream = obj.FileByteStream;
        //    string fileNme = obj.fileName;

        //    if (obj.IsChangeFileName == true)
        //        fileNme = obj.ImgPrefix + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "_").Replace("-", "") + ".jpeg";

        //    try
        //    {
        //        string FileName = string.Empty;
        //        if (!String.IsNullOrEmpty(FileByteStream))
        //        {
        //            string rootpath = HostingEnvironment.MapPath(obj.FolderPath);
        //            if (!Directory.Exists(rootpath))
        //            {
        //                Directory.CreateDirectory(rootpath);
        //            }

        //            byte[] bArray = Convert.FromBase64String(FileByteStream);
        //            byte[] byteArray = bArray; // Put the bytes of the image here....                         
        //            MemoryStream ms = new MemoryStream(byteArray);

        //            FileName = fileNme;
        //            string filePath = Path.Combine(rootpath, FileName);
        //            if (File.Exists(filePath))
        //            {
        //                File.Delete(filePath);
        //            }

        //            using (FileStream targetStream = new FileStream(filePath, FileMode.Create,
        //                                  FileAccess.Write, FileShare.None))
        //            { //read from the input stream in 65000 byte chunks
        //                const int bufferLen = 65000;
        //                byte[] buffer = new byte[bufferLen];
        //                int count = 0;
        //                while ((count = ms.Read(buffer, 0, bufferLen)) > 0)
        //                {
        //                    // save to output stream
        //                    targetStream.Write(buffer, 0, count);
        //                }
        //                targetStream.Close();
        //                ms.Close();
        //            }
        //            ms.Dispose();
        //            //}
        //        }
        //    }
        //    catch (Exception)
        //    {

        //        //throw;
        //    }



        //    return fileNme;

        //}
    }
}
