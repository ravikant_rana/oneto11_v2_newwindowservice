﻿using OneTo11_V2_MoveToLive.QueryExtensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToLive.Helpers
{
    public class DBAccess
    {
        public string ConnectionString;

        public DBAccess(string connection)
        {
            ConnectionString = connection;
        }

        /* Tradition ADO.Net Approch for connectivity*/
        public List<T> GetDataFromStoredProcedure<T>(string StoredProcedureName, string[] ParametersName, params object[] ParametersValues)
        {
            List<T> _list = new List<T>();
            try
            {
                DataTable dtb = new DataTable();
                //string connectionstring = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;

                using (SqlConnection sqlConn = new SqlConnection(ConnectionString))
                {

                    SqlCommand cmdRecords = new SqlCommand(StoredProcedureName, sqlConn);
                    SqlDataAdapter daReport = new SqlDataAdapter(cmdRecords);
                    using (cmdRecords)
                    {
                        cmdRecords.CommandType = CommandType.StoredProcedure;
                        int i = 0;
                        foreach (string oName in ParametersName)
                        {
                            SqlParameter ForeignKeyTablePrm = new SqlParameter(oName, ParametersValues[i]);
                            cmdRecords.Parameters.Add(ForeignKeyTablePrm);
                            i++;
                        }
                        daReport.Fill(dtb);
                    }
                }
                _list = DataTableExtension.ToList<T>(dtb);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "LogMoveToLive");
            }
            return _list;
        }

        /* Tradition ADO.Net Approch for connectivity*/
        public List<T> GetDataFromStoredProcedureForJoinedContest<T>(string StoredProcedureName, string[] ParametersName, params object[] ParametersValues)
        {
            List<T> _list = new List<T>();
            try
            {
                DataTable dtb = new DataTable();
                //string connectionstring = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;

                using (SqlConnection sqlConn = new SqlConnection(ConnectionString))
                {

                    SqlCommand cmdRecords = new SqlCommand(StoredProcedureName, sqlConn);
                    SqlDataAdapter daReport = new SqlDataAdapter(cmdRecords);
                    using (cmdRecords)
                    {
                        cmdRecords.CommandType = CommandType.StoredProcedure;
                        int i = 0;
                        foreach (string oName in ParametersName)
                        {
                            SqlParameter ForeignKeyTablePrm = new SqlParameter(oName, ParametersValues[i]);
                            cmdRecords.Parameters.Add(ForeignKeyTablePrm);
                            i++;
                        }
                        daReport.Fill(dtb);
                    }
                }
                _list = DataTableExtension.ToList<T>(dtb);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex, "LogMoveToLive");
            }
            return _list;
        }
        /**********************End Generic function Ado dot net********************************/


        /* Tradition ADO.Net Approch for connectivity*/
        public DataTable GetDataFromStoredProcedure(string StoredProcedureName, string[] ParametersName, params object[] ParametersValues)
        {
            DataTable dtb = new DataTable();
            try
            {
               
                //string connectionstring = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdRecords = new SqlCommand(StoredProcedureName, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdRecords);
                using (cmdRecords)
                {
                    cmdRecords.CommandType = CommandType.StoredProcedure;
                    int i = 0;
                    foreach (string oName in ParametersName)
                    {
                        SqlParameter ForeignKeyTablePrm = new SqlParameter(oName, ParametersValues[i]);
                        cmdRecords.Parameters.Add(ForeignKeyTablePrm);
                        i++;
                    }
                    daReport.Fill(dtb);
                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex, "LogMoveToLive");
            }
            return dtb;
        }


        /* Tradition ADO.Net Approch for DataSet connectivity*/
        public DataSet GetDatasetFromStoredProcedure(string StoredProcedureName, string[] ParametersName, params object[] ParametersValues)
        {
            DataSet dtb = new DataSet();
            try
            {
                // string connectionstring = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdRecords = new SqlCommand(StoredProcedureName, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdRecords);
                using (cmdRecords)
                {
                    cmdRecords.CommandType = CommandType.StoredProcedure;
                    int i = 0;
                    foreach (string oName in ParametersName)
                    {
                        SqlParameter ForeignKeyTablePrm = new SqlParameter(oName, ParametersValues[i]);
                        cmdRecords.Parameters.Add(ForeignKeyTablePrm);
                        i++;
                    }
                    daReport.Fill(dtb);
                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex, "LogMoveToLive");
            }
            return dtb;

        }

        /* Tradition ADO.Net Approch for connectivity*/
        public int SaveDataStoredProcedure(string StoredProcedureName, string[] ParametersName, params object[] ParametersValues)
        {
            int result = 0;

            try
            {
                //Read the connection string from Web.Config file
                // string connectionstring = _configuration.GetSection("ConnectionStrings").GetSection("DefaultConnection").Value;
                SqlConnection sqlConn = new SqlConnection(ConnectionString);
                SqlCommand cmdRecords = new SqlCommand(StoredProcedureName, sqlConn);
                SqlDataAdapter daReport = new SqlDataAdapter(cmdRecords);
                using (cmdRecords)
                {
                    //Create the SqlCommand object
                    //SqlCommand cmd = new SqlCommand(StoredProcedureName, con);

                    //Specify that the SqlCommand is a stored procedure
                    cmdRecords.CommandType = CommandType.StoredProcedure;
                    //Add the output parameter to the command object 
                    int i = 0;
                    foreach (string oName in ParametersName)
                    {
                        SqlParameter ForeignKeyTablePrm = new SqlParameter(oName, ParametersValues[i]);
                        cmdRecords.Parameters.Add(ForeignKeyTablePrm);
                        i++;
                    }

                    //Open the connection and execute the query
                    sqlConn.Open();
                    result = cmdRecords.ExecuteNonQuery();
                    sqlConn.Close();

                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex, "LogMoveToLive");
            }
            return result;
        }
    }
}
