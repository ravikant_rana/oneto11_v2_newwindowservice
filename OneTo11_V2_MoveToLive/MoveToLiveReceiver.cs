﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using MongoDB.Driver;
using Newtonsoft.Json;
using OneTo11_V2_MoveToLive.Helpers;
using OneTo11_V2_MoveToLive.Models;
using OneTo11_V2_MoveToLive.QueryExtensions;
using OneTo11_V2_Redis;
using OneTo11_V2_Redis.Helpers;
using OneTo11_V2_Redis.Helpers.Enums;
using RabbitMQ.Client;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_MoveToLive
{
    public partial class MoveToLiveReceiver : DefaultBasicConsumer
    {
        private readonly IModel _channel;
        private DBAccess dBAccess;
        private DBAccess authDBAccess;
        private DBAccess transactionDBAccess;
        string connectionGameString = ConfigurationManager.AppSettings["OneTo11_Game_Connection"];
        string connectionAuhtString = ConfigurationManager.AppSettings["OneTo11_Auth_Connection"];
        //string connectionTransactiontString = ConfigurationManager.AppSettings["OneTo11_Transaction_Connection"];
        string connection = ConfigurationManager.AppSettings["LiveMongoConnection"]; //"mongodb+srv://oneto11rootuser:Wve8BYfHtexpmOSG@cluster1.t98az.mongodb.net/test";
        MongoClient dbClient;
        public MoveToLiveReceiver(IModel channel)
        {
            _channel = channel;
            //dbClient = new MongoClient(connection);
            dBAccess = new DBAccess(connectionGameString);
            authDBAccess = new DBAccess(connectionAuhtString);
            //transactionDBAccess = new DBAccess(connectionTransactiontString);
            dbClient = new MongoClient(connection);
            //authDBAccess = new AuthDBAccess();
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            Library.WriteErrorLog("Move to live receiver started at: " + DateTime.Now);

            try
            {
                JsonMoveToLive jsonMoveToLive = new JsonMoveToLive();

                string jsonData = Encoding.UTF8.GetString(body);
                jsonMoveToLive = JsonConvert.DeserializeObject<JsonMoveToLive>(jsonData);

                SaveMatchContestNotFilled(jsonMoveToLive.match_id);

                SaveLiveMatchContestInCache(jsonMoveToLive.match_id);

                SendMatchContestUserTeamDataToLive(jsonMoveToLive.match_id);
                try
                {
                    /* Get started match details for notification */
                    GetMatchStartedDetails(Convert.ToInt64(jsonMoveToLive.match_id));

                    SaveLiveGamesInCache(jsonMoveToLive.match_id);

                    /* Save not fully filled contest prize breakup in sql database */
                    CallNotFullyFillContestPrizeBreakUp(Convert.ToInt64(jsonMoveToLive.match_id));

                    /* Generate match contest team players pdf */
                    GeneratePDF(Convert.ToInt64(jsonMoveToLive.match_id));                    

                    DeleteUpcomingMatchCache();

                    PublishRabbitMQMessage(jsonMoveToLive);

                    PublishRabbitMQMessageForCompareTeam(jsonMoveToLive);
                }
                catch (Exception ex)
                {
                    Library.WriteErrorLog(ex);
                }
                //DeleteMatchAfterLiveMovement(jsonMoveToLive.match_id);



            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
            /* End notification */

            Library.WriteErrorLog("Move to live receiver end at: " + DateTime.Now);

            _channel.BasicAck(deliveryTag, false);

        }

        public void Test(JsonMoveToLive jsonMoveToLive)
        {
            try
            {
                SaveLiveGamesInCache(jsonMoveToLive.match_id);

                SaveMatchContestNotFilled(jsonMoveToLive.match_id);

                SaveLiveMatchContestInCache(jsonMoveToLive.match_id);

                SendMatchContestUserTeamDataToLive(jsonMoveToLive.match_id);

                /* Get started match details for notification */
                GetMatchStartedDetails(Convert.ToInt64(jsonMoveToLive.match_id));

                /* Save not fully filled contest prize breakup in sql database */
                CallNotFullyFillContestPrizeBreakUp(Convert.ToInt64(jsonMoveToLive.match_id));

                /* Generate match contest team players pdf */
                GeneratePDF(Convert.ToInt64(jsonMoveToLive.match_id));

               
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }
        /* Move matches and their team players from sql to mongo database at the time of live*/
        public void SaveLiveGamesInCache(Int64? match_id)
        {
            try
            {

                //string livematches = ConfigurationManager.AppSettings["CacheStartingAbbr"] + CacheObjectKeys.Matches + ":getupcomingmatches";

                //MongoClient dbClient = new MongoClient(connection);

                var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var collection = database.GetCollection<LiveMatchInfo>("Matches");
                var matchTeamPlayercollection = database.GetCollection<TeamPlayer>("MatchTeamPlayerInfo");
                var matchfilter = Builders<LiveMatchInfo>.Filter.Eq("dbId", match_id);

                var _matchMongoList = collection.Find(matchfilter).ToList();

                using (var cache = new CacheService())
                {
                    string livematchescache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatches"]; // "dev_Test_LiveMatches";
                    string livematchinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchInfo"];
                    string livematchplayerinfocache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(match_id);

                    string[] paramScheduleName = { "@match_id" };
                    DataSet dbSet = dBAccess.GetDatasetFromStoredProcedure("[dbo].[PROC_GET_ScheluedMatchesByMatchId]", paramScheduleName, match_id);
                    DataTable dataMatches = dbSet.Tables[0];
                    DataTable dataPlayers = dbSet.Tables[1];
                    if (dataMatches.Rows.Count > 0 && dataPlayers.Rows.Count > 0)
                    {
                        List<LiveMatches> liveMatches = new List<LiveMatches>();
                        List<LiveMatchInfo> liveMatchInfo = new List<LiveMatchInfo>();
                        List<TeamPlayer> teamPlayerList = new List<TeamPlayer>();
                        liveMatches = IntigateCache.RetrieveCacheByCacheName<LiveMatches>(livematchescache, cache);
                        var filter = liveMatches.Where(x => x.dbId == match_id).ToList();
                        if (filter.Count == 0)
                        {
                            LiveMatches mch = new LiveMatches();

                            mch._id = Convert.ToInt64(dataMatches.Rows[0]["id"]);
                            mch.dbId = Convert.ToInt64(dataMatches.Rows[0]["id"]);
                            liveMatches.Add(mch);

                            IntigateCache.SaveToCache<LiveMatches>(cache, livematchescache, liveMatches);

                            liveMatchInfo = IntigateCache.RetrieveCacheByCacheName<LiveMatchInfo>(livematchinfocache, cache);

                            var liveMatchInfoFilter = liveMatchInfo.Where(x => x.dbId == match_id).ToList();

                            if (liveMatchInfoFilter.Count == 0)
                            {
                                LiveMatchInfo mchInfo = new LiveMatchInfo();
                                mchInfo._id = Convert.ToInt64(dataMatches.Rows[0]["id"]);
                                mchInfo.dbId = Convert.ToInt64(dataMatches.Rows[0]["id"]);
                                mchInfo.cid = Convert.ToString(dataMatches.Rows[0]["cid"]);
                                mchInfo.match_id = Convert.ToString(dataMatches.Rows[0]["match_id"]);
                                mchInfo.title = Convert.ToString(dataMatches.Rows[0]["title"]);
                                mchInfo.formatId = Convert.ToInt32(dataMatches.Rows[0]["formatId"]);
                                mchInfo.format_str = Convert.ToString(dataMatches.Rows[0]["format_str"]);
                                mchInfo.status = Convert.ToInt32(dataMatches.Rows[0]["status"]);
                                mchInfo.status_str = Convert.ToString(dataMatches.Rows[0]["status_str"]);
                                mchInfo.status_note = Convert.ToString(dataMatches.Rows[0]["status_note"]);
                                mchInfo.TeamA_Id = Convert.ToString(dataMatches.Rows[0]["TeamA_Id"]);
                                mchInfo.TeamA = Convert.ToString(dataMatches.Rows[0]["TeamA"]);
                                mchInfo.TeamA_Logo = Convert.ToString(dataMatches.Rows[0]["TeamA_Logo"]);
                                mchInfo.TeamB_Id = Convert.ToString(dataMatches.Rows[0]["TeamB_Id"]);
                                mchInfo.TeamB = Convert.ToString(dataMatches.Rows[0]["TeamB"]);
                                mchInfo.TeamB_Logo = Convert.ToString(dataMatches.Rows[0]["TeamB_Logo"]);
                                mchInfo.ContestName = Convert.ToString(dataMatches.Rows[0]["ContestName"]);
                                mchInfo.GameTypeId = Convert.ToString(dataMatches.Rows[0]["GameTypeId"]);
                                mchInfo.MatchStartTime = Convert.ToDateTime(dataMatches.Rows[0]["date_start"]);
                                mchInfo.MatchEndTime = Convert.ToDateTime(dataMatches.Rows[0]["date_end"]);
                                mchInfo.seriesname = Convert.ToString(dataMatches.Rows[0]["seriesname"]);
                                mchInfo.IsVerified = false;

                                liveMatchInfo.Add(mchInfo);

                                IntigateCache.SaveToCache<LiveMatchInfo>(cache, livematchinfocache, liveMatchInfo);

                                if (_matchMongoList.Count == 0)
                                {
                                    collection.InsertOne(mchInfo);
                                }


                                string pendingResizeMatchCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + "Matches:" + ConfigurationManager.AppSettings["redis:PendingResizeMatch"];

                                List<ResizePendingMatch> resizePendingMatchesList = new List<ResizePendingMatch>();

                                resizePendingMatchesList = IntigateCache.RetrieveCacheByCacheName<ResizePendingMatch>(pendingResizeMatchCache, cache);

                                foreach (var m in liveMatchInfo)
                                {
                                    ResizePendingMatch resizePendingMatch = new ResizePendingMatch
                                    {
                                        MatchId = m.dbId,
                                        ApiMatchId = m.match_id,
                                        Title = m.title,
                                        MatchStartTime = m.MatchStartTime
                                    };

                                    resizePendingMatchesList.Add(resizePendingMatch);
                                }

                                if (resizePendingMatchesList.Count > 0)
                                {
                                    IntigateCache.SaveToCache<ResizePendingMatch>(cache, pendingResizeMatchCache, resizePendingMatchesList);
                                }

                            }

                            teamPlayerList = IntigateCache.RetrieveCacheByCacheName<TeamPlayer>(livematchplayerinfocache, cache);
                            var teamPlayerListFilter = teamPlayerList.Where(x => x.MatchId == match_id).ToList();
                            if (teamPlayerListFilter.Count == 0)
                            {
                                foreach (DataRow dataRow in dataPlayers.Rows)
                                {
                                    TeamPlayer teamPlayer = new TeamPlayer
                                    {
                                        _id = (DBNull.Value == dataRow["Id"]) ? 0 : Convert.ToInt64(dataRow["Id"]),
                                        MatchTeamPlayerId = (DBNull.Value == dataRow["MatchTeamPlayerId"]) ? 0 : Convert.ToInt64(dataRow["MatchTeamPlayerId"]),
                                        MatchId = (DBNull.Value == dataRow["MatchId"]) ? 0 : Convert.ToInt64(dataRow["MatchId"]),
                                        TeamId = (DBNull.Value == dataRow["TeamId"]) ? 0 : Convert.ToInt64(dataRow["TeamId"]),
                                        TeamName = (DBNull.Value == dataRow["TeamName"]) ? "" : Convert.ToString(dataRow["TeamName"]),
                                        Team_Abbr = (DBNull.Value == dataRow["Team_Abbr"]) ? "" : Convert.ToString(dataRow["Team_Abbr"]),
                                        Name = (DBNull.Value == dataRow["Name"]) ? "" : Convert.ToString(dataRow["Name"]),
                                        Playing_Role = (DBNull.Value == dataRow["PlayerType"]) ? "" : Convert.ToString(dataRow["PlayerType"]),
                                        SelectedBy = (DBNull.Value == dataRow["SelectedBy"]) ? 0 : Convert.ToDecimal(dataRow["SelectedBy"]),
                                        CaptainBy = (DBNull.Value == dataRow["CaptainBy"]) ? 0 : Convert.ToDecimal(dataRow["CaptainBy"]),
                                        ViceCaptainBy = (DBNull.Value == dataRow["ViceCaptainBy"]) ? 0 : Convert.ToDecimal(dataRow["ViceCaptainBy"]),
                                        FantacyPoints = (DBNull.Value == dataRow["FantacyPoints"]) ? 0 : Convert.ToDecimal(dataRow["FantacyPoints"]),
                                        PlayerImage = (DBNull.Value == dataRow["PlayerImage"]) ? "" : Convert.ToString(dataRow["PlayerImage"]),
                                        IsAnnounced = (DBNull.Value == dataRow["IsAnnounced"]) ? false : Convert.ToBoolean(dataRow["IsAnnounced"]),
                                        Rating = (dataRow["Credit"] != DBNull.Value) ? Convert.ToString(dataRow["Credit"]) : "0",
                                        Credit = (dataRow["Credit"] != DBNull.Value) ? Convert.ToDecimal(dataRow["Credit"]) : 0,
                                        starting11 = "0",
                                        run = "0",
                                        four = "0",
                                        six = "0",
                                        sr = "0",
                                        fifty = "0",
                                        duck = "0",
                                        wkts = "0",
                                        maidenover = "0",
                                        er = "0",
                                        _catch = "0",
                                        runoutstumping = "0",
                                        runoutthrower = "0",
                                        runoutcatcher = "0",
                                        directrunout = "0",
                                        stumping = "0",
                                        thirty = "0",
                                        bonus = "0",

                                    };

                                    teamPlayerList.Add(teamPlayer);
                                }

                                if (teamPlayerList.Count > 0)
                                {
                                    IntigateCache.SaveToCache<TeamPlayer>(cache, livematchplayerinfocache, teamPlayerList);

                                    var matchTeamPlayerFilter = Builders<TeamPlayer>.Filter.Eq("MatchId", match_id);

                                    var _matchTeamPlayerMongoList = matchTeamPlayercollection.Find(matchTeamPlayerFilter).ToList();
                                    if (_matchTeamPlayerMongoList.Count == 0)
                                    {
                                        matchTeamPlayercollection.InsertMany(teamPlayerList);
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void SaveLiveUserTeamInCache(Int64? match_id)
        {
            try
            {
                using (var cache = new CacheService())
                {
                    string cacheName = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchUserTeam"] + Convert.ToString(match_id);
                    //string cacheName = "prod_LiveMatchUserTeam:" + Convert.ToString(match_id);
                    string[] paramScheduleName = { "@match_id" };
                    var _result = dBAccess.GetDataFromStoredProcedure<LiveMatchUserTeam>("[dbo].[PROC_GET_LIVE_MATCH_USERTEAM_TO_CACHE]", paramScheduleName, match_id).ToList();
                    if (_result.Count > 0)
                    {
                        int record = 10000;

                        for (int i = 0; i < _result.Count; i = i + record)
                        {
                            int j = 0;
                            var _result1 = _result.Skip(i).Take(record).ToList();

                            HashEntry[] hashFields = new HashEntry[_result1.Count];
                            foreach (var item in _result1)
                            {
                                LiveMatchUserTeamCache liveMatchUserTeamCache = new LiveMatchUserTeamCache();
                                liveMatchUserTeamCache.ta = item.ta;
                                liveMatchUserTeamCache.c = item.c;
                                liveMatchUserTeamCache.vc = item.vc;
                                Int64[] players = Array.ConvertAll(item.pid.Split(','), Int64.Parse);
                                List<Players> playersList = new List<Players>();
                                foreach (var p in players)
                                {
                                    Players players2 = new Players();
                                    players2.pid = p;
                                    playersList.Add(players2);
                                }
                                liveMatchUserTeamCache.plid = playersList;
                                HashEntry li = new HashEntry(Convert.ToInt64(item.ut), JsonConvert.SerializeObject(liveMatchUserTeamCache));
                                hashFields.SetValue(li, j);
                                j++;
                            }
                            cache.SaveHash(cacheName, hashFields);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }

        }
                
        public void SaveLiveMatchContestInCache(Int64? match_id)
        {
            try
            {
                //MongoClient dbClient = new MongoClient(connection);

                var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var contestcollection = database.GetCollection<MatchContest>("Contest");
                var matchContestFilter = Builders<MatchContest>.Filter.Eq("MatchId", match_id);
                var _matchContestMongoList = contestcollection.Find(matchContestFilter).ToList();


                using (var cache = new CacheService())
                {
                    string livematchcontestcache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContest"] + Convert.ToString(match_id);

                    string[] paramScheduleName = { "@match_id" };
                    DataSet dbSet = dBAccess.GetDatasetFromStoredProcedure("[dbo].[PROC_GET_LiveMatchContest]", paramScheduleName, match_id);
                    DataTable dataContest = dbSet.Tables[0];
                    DataTable dataMatchContestUserTeam = dbSet.Tables[1];

                    if (dataContest.Rows.Count > 0 && dataMatchContestUserTeam.Rows.Count > 0)
                    {
                        List<MatchContest> matchContestList = new List<MatchContest>();
                        List<MatchContestData> matchContestDataList = new List<MatchContestData>();
                        List<MatchContestUserTeamsData> matchContestUserTeamsDataList = new List<MatchContestUserTeamsData>();
                        matchContestDataList = DataTableExtension.ToList<MatchContestData>(dataContest);
                        matchContestUserTeamsDataList = DataTableExtension.ToList<MatchContestUserTeamsData>(dataMatchContestUserTeam);
                        //var matchContestListFilter = matchContestDataList.Where(x => x.MatchId == match_id).ToList();
                        if (matchContestDataList.Count > 0)
                        {
                            foreach (var dataRow in matchContestDataList)
                            {
                                MatchContest matchContest = new MatchContest();

                                matchContest._id = dataRow.ContestId;
                                matchContest.dbId = dataRow.ContestId;
                                matchContest.MatchId = dataRow.MatchId;
                                matchContest.API_MatchId = dataRow.API_MatchId;
                                matchContest.Title = dataRow.Title;
                                matchContest.MaxUsers = dataRow.MaxUsers;
                                matchContest.WinerPercent = dataRow.WinerPercent;
                                matchContest.ContestInvitationCode = dataRow.ContestInvitationCode;
                                matchContest.TotalContestAmount = dataRow.TotalContestAmount;
                                matchContest.TotalTeam = dataRow.TotalTeam;
                                matchContest.FirstWinner = dataRow.FirstWinner;
                                matchContest.EntryFee = dataRow.EntryFee;
                                //matchContest.IsConfirm = dataRow.IsConfirm;
                                matchContest.MultipleEntryAllowed = dataRow.MultipleEntryAllowed;
                                matchContest.IsPractice = dataRow.IsPractice;
                                matchContest.status = dataRow.Status;
                                matchContest.status_str = dataRow.status_str;
                                matchContestList.Add(matchContest);

                                //try
                                //{
                                //    string livematchcontestCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchLeaderBoard"] + Convert.ToString(match_id) + ":" + Convert.ToString(dataRow.ContestId);
                                //    SaveLiveMatchLeaderBoardInCache(livematchcontestCache, dataRow.ContestId);
                                //}
                                //catch (Exception ex)
                                //{
                                //    Library.WriteErrorLog(ex);
                                //}

                            }

                            if (matchContestList.Count > 0)
                            {
                                IntigateCache.SaveToCache<MatchContest>(cache, livematchcontestcache, matchContestList);
                                if (_matchContestMongoList.Count == 0)
                                {
                                    contestcollection.InsertMany(matchContestList);
                                }
                            }

                        }

                        //List<MatchContestUserTeams> matchContestUserTeamsList = new List<MatchContestUserTeams>();
                        //foreach (var user in matchContestUserTeamsDataList)
                        //{
                        //    MatchContestUserTeams matchContestUserTeams = new MatchContestUserTeams();
                        //    matchContestUserTeams._id = user.MatchContestUserTeamId;
                        //    matchContestUserTeams.MatchContestUserTeamId = user.MatchContestUserTeamId;
                        //    matchContestUserTeams.UserTeamId = user.UserTeamId;
                        //    matchContestUserTeams.FantacyPoints = user.FantacyPoints;
                        //    matchContestUserTeams.UserId = user.UserId;
                        //    matchContestUserTeams.MatchContestId = user.MatchContestId;
                        //    matchContestUserTeams.MatchId = user.MatchId;
                        //    matchContestUserTeams.TeamAbbr = user.TeamAbbr;
                        //    matchContestUserTeams.IsWinningAmountAdded = false;
                        //    matchContestUserTeamsList.Add(matchContestUserTeams);
                        //}

                    }
                }

            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void SendMatchContestUserTeamDataToLive(Int64 MatchId)
        {
            try
            {
                using (var cache = new CacheService())
                {
                    string cacheName = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchContestUserTeam"] + Convert.ToString(MatchId);
                    List<LiveMatchContestUserTeamArray> liveUserTeam = new List<LiveMatchContestUserTeamArray>();

                    string[] paramScheduleName = { "MatchId" };
                    var dbSet = dBAccess.GetDataFromStoredProcedure("[dbo].[GetLiveMatchUserTeam]", paramScheduleName, MatchId);
                    SortedSetEntry[] val = new SortedSetEntry[dbSet.Rows.Count];
                    int i = 0;
                    foreach (DataRow dataMatches in dbSet.Rows)
                    {
                        LiveMatchContestUserTeamArray liveMatche = new LiveMatchContestUserTeamArray();
                        //liveMatche.U = Convert.ToInt64(dataMatches["U"]);
                        //liveMatche.M = Convert.ToInt64(dataMatches["M"]);
                        liveMatche.UT = Convert.ToString(dataMatches["UT"]);
                        liveMatche.CUT = Convert.ToString(dataMatches["CUT"]);
                        liveMatche.CC = Convert.ToString(dataMatches["CC"]);
                        SortedSetEntry li = new SortedSetEntry(JsonConvert.SerializeObject(liveMatche), Convert.ToInt64(dataMatches["U"]));
                        val.SetValue(li, i);
                        i++;

                    }
                    cache.AddtoList(cacheName, val);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }

            // bool added = cacheClient.Add("MyList", liveMatches, DateTimeOffset.Now.AddMinutes(10));
        }

        public void SaveContestUserTeamInCache(Int64 match_id)
        {
            try
            {
                using (var cache = new CacheService())
                {
                    string cacheName = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:ContestUserTeam"] + Convert.ToString(match_id);
                    var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                    var contestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                    var matchContestUserTeamFilter = Builders<MatchContestUserTeamsMongoLive>.Filter.Eq("MatchId", match_id);
                    var _matchContestUserTeamMongoList = contestUserTeamCollection.Find(matchContestUserTeamFilter).ToList();
                    if (_matchContestUserTeamMongoList.Count > 0)
                    {
                        int record = 10000;

                        for (int i = 0; i < _matchContestUserTeamMongoList.Count; i = i + record)
                        {
                            int j = 0;
                            var _result1 = _matchContestUserTeamMongoList.Skip(i).Take(record).ToList();

                            HashEntry[] hashFields = new HashEntry[_result1.Count];
                            foreach (var item in _result1)
                            {
                                ContestUserTeamCache liveContestUserTeamCache = new ContestUserTeamCache();
                                liveContestUserTeamCache.cid = item.MatchContestId;
                                liveContestUserTeamCache.uid = item.UserId;

                                HashEntry li = new HashEntry(Convert.ToInt64(item._id), JsonConvert.SerializeObject(liveContestUserTeamCache));
                                hashFields.SetValue(li, j);
                                j++;
                            }
                            cache.SaveHash(cacheName, hashFields);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void SaveLiveMatchLeaderBoardInCache(string cacheName, Int64 ContestId)
        {
            using (var cache = new CacheService())
            {
                var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                var _filter = Builders<MatchContestUserTeamsMongoLive>.Filter.Eq("MatchContestId", ContestId);
                var _contestUserTeamList = liveContestUserTeamCollection.Find(_filter).ToList();
                if (_contestUserTeamList.Count > 0)
                {
                    List<MatchContestUserTeamsMongoLiveCache> mcutmlList = new List<MatchContestUserTeamsMongoLiveCache>();
                    foreach (var item in _contestUserTeamList)
                    {
                        MatchContestUserTeamsMongoLiveCache obj = new MatchContestUserTeamsMongoLiveCache
                        {
                            tn = item.TeamAbbr,
                            pts = item.FantacyPoints ?? 0,
                            //un = item.TeamAbbr,
                            tk = item.UserTeamId,
                            rank = item.rank,
                            //image = "https://OneTo11storageaccount.blob.core.windows.net/profile/"+Convert.ToString(item.UserId)+".png",
                            uid = item.UserId,
                            ai = 1,
                            iwj = item.isInWinning_Jone
                        };
                        mcutmlList.Add(obj);
                    }

                    if (mcutmlList.Count > 0)
                    {
                        IntigateCache.SaveToCache(cache, cacheName, mcutmlList);
                    }
                }

            }
        }

        /* Save match contest not fully filled insert into MatchContestNotFilled table and save admin notification */
        public void SaveMatchContestNotFilled(Int64? match_id)
        {
            try
            {
                string[] paramScheduleName = { "@MatchId" };
                var _result = dBAccess.GetDataFromStoredProcedure<GetDataForContestNotFilled>("[dbo].[PROC_SAVE_NOT_FULLY_FILL_CONTEST]", paramScheduleName, match_id).ToList();
                foreach (var item in _result)
                {
                    SaveAdminNotificationRequest saveAdminNotificationRequest = new SaveAdminNotificationRequest();
                    saveAdminNotificationRequest.MatchId = item.MatchId;
                    saveAdminNotificationRequest.ContestId = item.MatchContestId;
                    saveAdminNotificationRequest.AdminNotificationTypeId = 3; /* Contest not filled */
                    SaveAdminNotification(saveAdminNotificationRequest);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        /* Save not fully filled contest prize breakup in sql database */
        public async Task CallNotFullyFillContestPrizeBreakUp(Int64? match_id)
        {
            try
            {
                await Task.Run(() =>
                {
                    int rowCount = 0;
                    string[] paramScheduleName = { "@MatchId" };
                    rowCount = dBAccess.SaveDataStoredProcedure("[dbo].[PROC_SAVE_NOT_FULLY_FILL_CONTEST_PRIZEBREAKUP]", paramScheduleName, match_id);
                });
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void DeleteMatchAfterLiveMovement(Int64 matchId)
        {
            try
            {
                string[] paramName = { "@match_id" };
                var matchResult = dBAccess.SaveDataStoredProcedure("[dbo].[PROC_DELETE_MatchAfterLiveMovement]", paramName, matchId);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public int SaveAdminNotification(SaveAdminNotificationRequest notificationRequest)
        {
            int result = 0;
            try
            {
                string[] notificationParam = { "@MatchId", "@ContestId", "@AdminNotificationType_Fk_Id" };
                DataTable data = dBAccess.GetDataFromStoredProcedure("[dbo].[PROC_SAVE_ADMIN_NOTIFICATION_MESSAGE]", notificationParam
                                             , notificationRequest.MatchId, notificationRequest.ContestId, notificationRequest.AdminNotificationTypeId
                                             );

                if (data.Rows.Count > 0)
                {
                    result = Convert.ToInt32(data.Rows[0]["ResultCode"]);
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
            return result;
        }

        public void GeneratePDF(long match_id)
        {
            try
            {
                var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var liveMatchCollection = database.GetCollection<LiveMatchInfo>("Matches");
                var filter = Builders<LiveMatchInfo>.Filter.Eq("dbId", match_id);
                var _matchList = liveMatchCollection.Find(filter).ToList();
                LiveMatchInfo liveMatches = new LiveMatchInfo();
                if (_matchList.Count > 0)
                {
                    liveMatches = _matchList.FirstOrDefault();

                    var liveContestCollection = database.GetCollection<MatchContest>("Contest");
                    var contestfilter = Builders<MatchContest>.Filter.Eq("MatchId", match_id);
                    
                    var _contestList = liveContestCollection.Find(contestfilter).ToList();
                    foreach (var contest in _contestList)
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            string path = ConfigurationManager.AppSettings["DocumentPath"];
                            string fileName = liveMatches.TeamA + "vs" + liveMatches.TeamB + "_" + Convert.ToString(contest.ContestInvitationCode) + ".pdf";
                            string filePath = path + fileName;
                            Document document = new Document(PageSize.A2, 10, 10, 10, 10);
                            PdfWriter wri = PdfWriter.GetInstance(document, new FileStream(filePath, FileMode.OpenOrCreate));

                            //PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                            document.Open();

                            float[] headers = {
                                12,8,8,8,8,8,8,8,8,8,8,8
                            }; //Header Widths
                            PdfPTable table = new PdfPTable(12);
                            table.SetWidths(headers); //Set the pdf headers
                            table.WidthPercentage = 100;
                            //var image = Image.GetInstance(@"logo.jpg");
                            iTextSharp.text.Image logo = iTextSharp.text.Image.GetInstance(ConfigurationManager.AppSettings["AppLogoPath"]);
                            //string imageFile = @"logo.jpg";
                            table.AddCell(new PdfPCell(logo) //new BaseColor(153, 51, 0)
                            {
                                Colspan = 1,
                                Border = 0,
                                PaddingLeft = 10,
                                PaddingTop = 2,
                                PaddingBottom = 2,
                                HorizontalAlignment = Element.ALIGN_LEFT,
                                //BackgroundColor = BaseColor.RED,
                                BackgroundColor = new BaseColor(1, 141, 230),
                                FixedHeight = 20
                            });

                            table.AddCell(new PdfPCell(new Phrase(Convert.ToString(liveMatches.title), new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.WHITE))) //new BaseColor(153, 51, 0)
                            {
                                Colspan = 2,
                                Border = 0,
                                PaddingBottom = 10,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                //BackgroundColor = BaseColor.RED,
                                BackgroundColor = new BaseColor(1, 141, 230),
                            });

                            table.AddCell(new PdfPCell(new Phrase("Contest win Rs. " + contest.TotalContestAmount, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.WHITE))) //new BaseColor(153, 51, 0)
                            {
                                Colspan = 2,
                                Border = 0,
                                PaddingBottom = 10,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                //BackgroundColor = BaseColor.RED,
                                BackgroundColor = new BaseColor(1, 141, 230),
                            });

                            table.AddCell(new PdfPCell(new Phrase("Entry fee Rs. " + contest.EntryFee, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.WHITE))) //new BaseColor(153, 51, 0)
                            {
                                Colspan = 2,
                                Border = 0,
                                PaddingBottom = 10,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                //BackgroundColor = BaseColor.RED,
                                BackgroundColor = new BaseColor(1, 141, 230),
                            });

                            table.AddCell(new PdfPCell(new Phrase("Members: " + contest.TotalTeam, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.WHITE))) //new BaseColor(153, 51, 0)
                            {
                                Colspan = 2,
                                Border = 0,
                                PaddingBottom = 10,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                //BackgroundColor = BaseColor.RED,
                                BackgroundColor = new BaseColor(1, 141, 230),
                            });

                            table.AddCell(new PdfPCell(new Phrase("Invitation code: " + contest.ContestInvitationCode, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.WHITE))) //new BaseColor(153, 51, 0)
                            {
                                Colspan = 2,
                                Border = 0,
                                PaddingBottom = 10,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                //BackgroundColor = BaseColor.RED,
                                BackgroundColor = new BaseColor(1, 141, 230),
                            });

                            table.AddCell(new PdfPCell(new Phrase("Part 1", new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.WHITE))) //new BaseColor(153, 51, 0)
                            {
                                Colspan = 1,
                                Border = 0,
                                PaddingBottom = 10,
                                HorizontalAlignment = Element.ALIGN_CENTER,
                                //BackgroundColor = BaseColor.RED,
                                BackgroundColor = new BaseColor(1, 141, 230),
                            });

                            AddCellToHeader(table, "User (Team)");
                            AddCellToHeader(table, "Player 1 (Captain)");
                            AddCellToHeader(table, "Player 2 (Vice Captain)");
                            AddCellToHeader(table, "Player 3");
                            AddCellToHeader(table, "Player 4");
                            AddCellToHeader(table, "Player 5");
                            AddCellToHeader(table, "Player 6");
                            AddCellToHeader(table, "Player 7");
                            AddCellToHeader(table, "Player 8");
                            AddCellToHeader(table, "Player 9");
                            AddCellToHeader(table, "Player 10");
                            AddCellToHeader(table, "Player 11");


                            string[] userTeamParam = { "@MatchId", "@ContestId" };
                            var result = dBAccess.GetDataFromStoredProcedure<PROC_GetUserTeamPlayer_Result>("[dbo].[PROC_GetContestUserTeamPlayer]", userTeamParam, match_id, contest.dbId).ToList();
                            if (result.Count > 0)
                            {
                                int i = 1;
                                foreach (var player in result)
                                {
                                    BaseColor baseColor = BaseColor.WHITE;
                                    if (i % 2 == 0)
                                    {
                                        baseColor = BaseColor.LIGHT_GRAY;
                                    }
                                    else
                                    {
                                        baseColor = BaseColor.WHITE;
                                    }
                                    AddCellToBody(table, player.TeamAbbr, baseColor);
                                    AddCellToBody(table, player.Captain, baseColor);
                                    AddCellToBody(table, player.ViceCaptain, baseColor);
                                    AddCellToBody(table, player.Player3, baseColor);
                                    AddCellToBody(table, player.Player4, baseColor);
                                    AddCellToBody(table, player.Player5, baseColor);
                                    AddCellToBody(table, player.Player6, baseColor);
                                    AddCellToBody(table, player.Player7, baseColor);
                                    AddCellToBody(table, player.Player8, baseColor);
                                    AddCellToBody(table, player.Player9, baseColor);
                                    AddCellToBody(table, player.Player10, baseColor);
                                    AddCellToBody(table, player.Player11, baseColor);

                                    //document.NewPage();

                                    i++;
                                    //table.AddCell(player.TeamAbbr);
                                    //table.AddCell(player.Captain);
                                    //table.AddCell(player.ViceCaptain);
                                    //table.AddCell(player.Player3);
                                    //table.AddCell(player.Player4);
                                    //table.AddCell(player.Player5);
                                    //table.AddCell(player.Player6);
                                    //table.AddCell(player.Player7);
                                    //table.AddCell(player.Player8);
                                    //table.AddCell(player.Player9);
                                    //table.AddCell(player.Player10);
                                    //table.AddCell(player.Player11);
                                }
                                document.Add(table);

                                document.Close();
                                //byte[] bytes = memoryStream.ToArray();
                                //memoryStream.Close();
                                //Response.Clear();
                                //Response.ContentType = "application/pdf";

                                //string pdfName = "User";
                                //Response.AddHeader("Content-Disposition", "attachment; filename=" + pdfName + ".pdf");
                                //Response.ContentType = "application/pdf";
                                //Response.Buffer = true;
                                //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                                //Response.BinaryWrite(bytes);
                                //Response.End();
                                //Response.Close();

                                AzureStorageAccount azureStorageAccount = new AzureStorageAccount();


                                byte[] fileBytes = null;

                                // Create new local file and copy contents of uploaded file
                                //using (var localFile = System.IO.File.OpenWrite(fileName))
                                //using (var uploadedFile = Files.File.OpenReadStream())
                                //{
                                //    fileBytes = new byte[uploadedFile.Length];
                                //    uploadedFile.CopyTo(localFile);

                                //}

                                byte[] btstrm = ConvertImageToByteArray(filePath);

                                ImageFileDto image = new ImageFileDto();
                                image.fileName = fileName;
                                image.FileByteStream = Convert.ToBase64String(btstrm);
                                image.FilePrefix = "";
                                image.FileContainerName = "contestpdf";
                                image.FileFolderName = "contestpdf";
                                //image.mid = MemId;
                                image.IsChangeFileName = true;
                                var fileNameFromAzure = azureStorageAccount.SavePdfOnAzure(image);

                                //if (System.IO.File.Exists(fileName))
                                //{
                                //    System.IO.File.Delete(fileName);
                                //}
                            }
                            else
                            {
                                string[] contestPdfLog = { "@MatchId", "@ContestId", "@PdfName" };
                                int r = dBAccess.SaveDataStoredProcedure("[dbo].[PROC_SaveContestPdfLog]", userTeamParam, match_id, contest.dbId, fileName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public static byte[] ConvertImageToByteArray(string imagePath)
        {
            byte[] imageByteArray = null;
            FileStream fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
            using (BinaryReader reader = new BinaryReader(fileStream))
            {
                imageByteArray = new byte[reader.BaseStream.Length];
                for (int i = 0; i < reader.BaseStream.Length; i++)
                    imageByteArray[i] = reader.ReadByte();
            }
            return imageByteArray;
        }

        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.BLACK)))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Padding = 5,
                BackgroundColor = BaseColor.LIGHT_GRAY //new BaseColor(0, 51, 102)
            });
        }
        // Method to add single cell to the body  
        private static void AddCellToBody(PdfPTable tableLayout, string cellText, BaseColor baseColor)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.BLACK)))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                Padding = 5,
                BackgroundColor = baseColor               
            });
        }

        /* Get match started details for notification */
        public void GetMatchStartedDetails(Int64? match_id)
        {
            try
            {
                string[] paramScheduleName = { "@MatchId" };
                var _result = dBAccess.GetDataFromStoredProcedure<GetDataForNotification>("[dbo].[PROC_GET_MatchStartedDetails]", paramScheduleName, match_id).ToList();
                foreach (var item in _result)
                {
                    SaveNotificationRequest saveNotificationRequest = new SaveNotificationRequest();
                    saveNotificationRequest.LoggedInUserId = item.UserId;
                    saveNotificationRequest.NotificationTypeId = 4; /* joined contest starts */
                    saveNotificationRequest.Team1VsTeam2 = item.TeamA + " Vs " + item.TeamB;
                    saveNotificationRequest.Date = item.date_start;
                    int notificationResponse = SaveNotification(saveNotificationRequest);
                }


            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public int SaveNotification(SaveNotificationRequest notificationRequest)
        {
            int result = 0;
            try
            {
                string[] notificationParam = { "@UserId", "@NotificationTypeId", "@Amount", "@JoiningId", "@Date", "@Team1VsTeam2", "@Points", "@Rank", "@ContestName"
                                             , "@NoOfTotalUsersJoinedToday", "@TotalParticipants","@NoOfTotalTeams","@Reason","@Param1","@Param2","@Param3","@Param4", "@Param5" };
                DataTable data = authDBAccess.GetDataFromStoredProcedure("[dbo].[PROC_SAVE_NOTIFICATION_MESSAGE]", notificationParam, notificationRequest.LoggedInUserId, notificationRequest.NotificationTypeId
                                             , notificationRequest.Amount, notificationRequest.JoiningId, notificationRequest.Date, notificationRequest.Team1VsTeam2, notificationRequest.Points, notificationRequest.Rank
                                             , notificationRequest.ContestName, notificationRequest.NoOfTotalUsersJoinedToday, notificationRequest.TotalParticipants, notificationRequest.NoOfTotalTeams, notificationRequest.Reason
                                             , notificationRequest.Param1, notificationRequest.Param2, notificationRequest.Param3, notificationRequest.Param4, notificationRequest.Param5);

                if (data.Rows.Count > 0)
                {
                    result = Convert.ToInt32(data.Rows[0]["ResultCode"]);
                }
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public void DeleteUpcomingMatchCache()
        {
            var cache = new CacheService();
            string cacheName = "prod_Matches:getmatchdetails";
            IntigateCache.DeleteCache(cacheName);





        }

        public void PublishRabbitMQMessage(JsonMoveToLive jsonMoveToLive)
        {
            #region "Publish message queue for live updates"
            try
            {
                string message = JsonConvert.SerializeObject(jsonMoveToLive);
                MessagingQueue.GetConnectionFactory("oneto11-CacheCleanUp", "oneto11-queue-CacheCleanUp", "oneto11-CacheCleanUp.#", message);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
            #endregion "End Publish message queue for live updates"
        }

        public void PublishRabbitMQMessageForCompareTeam(JsonMoveToLive jsonMoveToLive)
        {
            #region "Publish message queue for live updates"
            try
            {
                string message = JsonConvert.SerializeObject(jsonMoveToLive);
                MessagingQueue.GetConnectionFactory("oneto11-CompareUserTeamPlayers", "oneto11-queue-CompareUserTeamPlayers", "oneto11-CompareUserTeamPlayers.#", message);
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
            #endregion "End Publish message queue for live updates"
        }

    }
}
