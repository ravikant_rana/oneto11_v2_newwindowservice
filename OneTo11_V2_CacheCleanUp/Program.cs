﻿using OneTo11_V2_Redis.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_CacheCleanUp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            Configurations.CacheEndpoint = Convert.ToString(ConfigurationManager.AppSettings["CacheEndpoint"]);
            Configurations.CachePassword = Convert.ToString(ConfigurationManager.AppSettings["CachePassword"]);
            Configurations.ContestCacheEndpoint = Convert.ToString(ConfigurationManager.AppSettings["ContestCacheEndpoint"]);
            Configurations.ContestCachePassword = Convert.ToString(ConfigurationManager.AppSettings["ContestCachePassword"]);
            Configurations.LeaderboardCacheEndpoint = Convert.ToString(ConfigurationManager.AppSettings["LeaderboardCacheEndpoint"]);
            Configurations.LeaderboardCachePassword = Convert.ToString(ConfigurationManager.AppSettings["LeaderboardCachePassword"]);
            Configurations.CacheSyncTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["CacheSyncTimeout"]);
            Configurations.CacheConnectTimeout = Convert.ToInt32(ConfigurationManager.AppSettings["CacheConnectTimeout"]);
#if DEBUG

            new CacheCleanUpService().StartCacheCleanUpService();
#else

            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new CacheCleanUpService()
            };
            ServiceBase.Run(ServicesToRun);
#endif
        }
    }
}
