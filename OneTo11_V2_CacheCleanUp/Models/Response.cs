﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_CacheCleanUp.Models
{
    public class CacheCleanUpResult
    {
        public long match_id { get; set; }
    }

    public class ContestResult
    {
        public long cid { get; set; }
    }
}
