﻿using Newtonsoft.Json;
using OneTo11_V2_CacheCleanUp.Helpers;
using OneTo11_V2_CacheCleanUp.Models;
using OneTo11_V2_Redis;
using OneTo11_V2_Redis.Helpers;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_CacheCleanUp
{
    public partial class CacheCleanUpReceiver : DefaultBasicConsumer
    {
        private DBAccess dBAccess;
        private readonly IModel _channel;

        string connectionGameString = ConfigurationManager.AppSettings["OneTo11_Game_Connection"];

        public CacheCleanUpReceiver(IModel channel)
        {
            _channel = channel;
            dBAccess = new DBAccess(connectionGameString);
        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            try
            {
                Library.WriteErrorLog("Cache clean up receiver started");

                CacheCleanUpResult cacheCleanUpResult = new CacheCleanUpResult();
                string jsonData = Encoding.UTF8.GetString(body);
                cacheCleanUpResult = JsonConvert.DeserializeObject<CacheCleanUpResult>(jsonData);

                /* Delete contest and upcoming leader board cache*/
                ContestCacheCleanUp(cacheCleanUpResult.match_id);

                /* Delete matches and joined contest count cache */
                MatchesCacheCleanUp(cacheCleanUpResult.match_id);

                Library.WriteErrorLog("Cache clean up completed");
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }

            _channel.BasicAck(deliveryTag, false);
        }

        /* Delete contest and upcoming leader board cache */
        public void ContestCacheCleanUp(long mid)
        {
            try
            {
                //var cache = new CacheService();
                string[] paramScheduleName = { "@MatchId" };
                var _result = dBAccess.GetDataFromStoredProcedure<ContestResult>("[dbo].[PROC_GET_CONTEST_FOR_CACHE_CLEAN_UP]", paramScheduleName, mid).ToList();
                foreach (var c in _result)
                {
                    string contestCacheName = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:Contest"] + ":" + Convert.ToString(mid) + ":" + Convert.ToString(c.cid);
                    IntigateCache.DeleteCache(contestCacheName);

                    string leaderboardCacheName = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:UpcomingLeaderBoard"] + ":" + Convert.ToString(c.cid);
                    IntigateCache.DeleteCache(leaderboardCacheName);

                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }

        }

        /* Delete matches and joined contest count cache */
        public void MatchesCacheCleanUp(long mid)
        {
            try
            {
                string matchesCacheName = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:Matches"] + ":" + Convert.ToString(mid);
                IntigateCache.DeleteCache(matchesCacheName);

                string joinedContestCountCacheName = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:JoinedContestCount"] + ":" + Convert.ToString(mid);
                IntigateContestCache.DeleteCache(joinedContestCountCacheName);
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }

        public void MoveUpcomingLeaderboard(long ContestId)
        {
            //Int32 totalJoinedTeamCount = 0;
            try
            {
                using (var cache = new LeaderboardCacheService())
                {
                    using (var newCache = new CacheService())
                    {
                        
                        string cacheJoinedContestName = "prod_UpcomingLeaderBoard:" + Convert.ToString(ContestId);

                        var Item = IntigateLeaderboardCache.RetrieveHgetAllByCacheName(cacheJoinedContestName, cache);

                        if (Item.Count() > 0)
                        {
                            IntigateCache.SaveToCacheHash(newCache, cacheJoinedContestName, Item);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }

            //return totalJoinedTeamCount;
        }

        public void GetDataForMovement()
        {
            string[] matchContestParam = { };
            DataTable result = dBAccess.GetDataFromStoredProcedure("[dbo].[PROC_GetDataForMovement]", matchContestParam);
            if (result.Rows.Count > 0)
            {
                foreach (DataRow dataRow in result.Rows)
                {
                    MoveUpcomingLeaderboard(Convert.ToInt64(dataRow["Id"]));
                }
            }
        }

        //public void UpcomingLeaderBoardCacheCleanUp(long mid)
        //{

        //}

        //public void JoinedContestCountCacheCleanUp(long mid)
        //{

        //}
    }
}
