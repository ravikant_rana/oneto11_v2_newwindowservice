﻿using OneTo11_V2_LiveGames.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace OneTo11_V2_LiveGames
{
    public partial class LiveGamesService : ServiceBase
    {
        private Timer timer1 = null;
        private DBAccess gamesDBAccess;
        //private LiveGamesDBAccess liveGamesDBAccess;
        string filepath = ConfigurationManager.AppSettings["FirestoreAPIKeyPath"];
        string connectionGameString = ConfigurationManager.AppSettings["OneTo11_Game_Connection"];
        public LiveGamesService()
        {
            InitializeComponent();
            gamesDBAccess = new DBAccess(connectionGameString);

            //var credential = GoogleCredential.FromFile(filepath);
            //var defaultApp = FirebaseApp.Create(new AppOptions()
            //{
            //    Credential = credential, //GoogleCredential.GetApplicationDefault(),
            //                             //ServiceAccountId = "firebase-adminsdk-i1cxj@oneto11-1581577228969.iam.gserviceaccount.com",
            //});
        }

        public void StartLiveGamesService()
        {

            OnStart(null);
            //Execute(null, null);
            //System.Threading.Thread.Sleep(1000000000);
        }

        protected override void OnStart(string[] args)
        {
            this.timer1 = new Timer();
            this.timer1.Interval = 1000 * 60;  //for 1 Min
            //this.timer1.Interval = 86400000; // 24 hours
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.Execute);
            this.timer1.Enabled = true;

        }
        public void Execute(object sender, ElapsedEventArgs e)
        {
            Library.WriteErrorLog("Excute started at" + DateTime.Now);
            timer1.Enabled = false;

            

            timer1.Enabled = true;
            Library.WriteErrorLog("Excute Completed at" + DateTime.Now);
        }
        protected override void OnStop()
        {
        }
    }
}
