﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_LiveUpdates.Models
{
    public class Teamplayer
    {
        public Int64? _id { get; set; }
        public Int64? MatchTeamPlayerId { get; set; }
        public Int64? MatchId { get; set; }
        public Int64? TeamId { get; set; }
        public string TeamName { get; set; }
        public string Team_Abbr { get; set; }
        public string Name { get; set; }
        public string Playing_Role { get; set; }
        public string SelectedBy { get; set; }
        public string CaptainBy { get; set; }
        public string ViceCaptainBy { get; set; }
        public decimal FantacyPoints { get; set; }
        public string PlayerImage { get; set; }
        public string Rating { get; set; }
        public bool IsAnnounced { get; set; }
        public string starting11 { get; set; }
        public string run { get; set; }
        public string four { get; set; }
        public string six { get; set; }
        public string sr { get; set; }
        public string fifty { get; set; }
        public string duck { get; set; }
        public string wkts { get; set; }
        public string maidenover { get; set; }
        public string er { get; set; }
        public string _catch { get; set; }
        public string runoutstumping { get; set; }
        public string runoutthrower { get; set; }
        public string runoutcatcher { get; set; }
        public string directrunout { get; set; }
        public string stumping { get; set; }
        public string thirty { get; set; }
        public string bonus { get; set; }

        public decimal? Credit { get; set; }
    }
}
