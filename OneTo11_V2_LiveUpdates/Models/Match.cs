﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_LiveUpdates.Models
{
    public class LiveMatchResponse : MongoDbGenericRepository.Models.Document
    {
        public long? dbmchid { get; set; }
        public string status { get; set; }
        public Match response { get; set; }
        public string etag { get; set; }
        public string modified { get; set; }
        public string datetime { get; set; }
        public string api_version { get; set; }
    }

    public class Match
    {
        public int? match_id { get; set; }
        public string title { get; set; }
        public string short_title { get; set; }
        public string subtitle { get; set; }
        public int? format { get; set; }
        public string format_str { get; set; }
        public int? status { get; set; }
        public string status_str { get; set; }
        public string status_note { get; set; }
        public string verified { get; set; }
        public string pre_squad { get; set; }
        public string odds_available { get; set; }
        public int? game_state { get; set; }
        public string game_state_str { get; set; }
        public Competition competition { get; set; }
        public Teama teama { get; set; }
        public Teamb teamb { get; set; }
        public string date_start { get; set; }
        public string date_end { get; set; }
        public int? timestamp_start { get; set; }
        public int? timestamp_end { get; set; }
        public Venue venue { get; set; }
        public string umpires { get; set; }
        public string referee { get; set; }
        public string equation { get; set; }
        public string live { get; set; }
        public string result { get; set; }
        public int? result_type { get; set; }
        public string win_margin { get; set; }
        public int? winning_team_id { get; set; }
        public int? commentary { get; set; }
        public int? wagon { get; set; }
        public int? latest_inning_number { get; set; }
        public Toss toss { get; set; }
        public Points points { get; set; }
    }

    public class Competition
    {
        public int? cid { get; set; }
        public string title { get; set; }
        public string abbr { get; set; }
        public string type { get; set; }
        public string category { get; set; }
        public string match_format { get; set; }
        public string status { get; set; }
        public string season { get; set; }
        public string datestart { get; set; }
        public string dateend { get; set; }
        public string total_matches { get; set; }
        public string total_rounds { get; set; }
        public string total_teams { get; set; }
        public string country { get; set; }
    }

    public class Teama
    {
        public int? team_id { get; set; }
        public string name { get; set; }
        public string short_name { get; set; }
        public string logo_url { get; set; }
        public string thumb_url { get; set; }
        public string scores_full { get; set; }
        public string scores { get; set; }
        public string overs { get; set; }
    }

    public class Teamb
    {
        public int? team_id { get; set; }
        public string name { get; set; }
        public string short_name { get; set; }
        public string logo_url { get; set; }
        public string thumb_url { get; set; }
        public string scores_full { get; set; }
        public string scores { get; set; }
        public string overs { get; set; }
    }

    public class Venue
    {
        public string name { get; set; }
        public string location { get; set; }
        public string timezone { get; set; }
    }

    public class Toss
    {
        public string text { get; set; }
        public int? winner { get; set; }
        public int? decision { get; set; }
    }

    public class Points
    {
        public Teama1 teama { get; set; }
        public Teamb1 teamb { get; set; }
    }

    public class Teama1
    {
        public List<Playing11> playing11 { get; set; }
        public List<Substitute> substitute { get; set; }
    }

    public class Playing11
    {
        public string pid { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public string rating { get; set; }
        public string point { get; set; }
        public string starting11 { get; set; }
        public string run { get; set; }
        public string four { get; set; }
        public string six { get; set; }
        public string sr { get; set; }
        public string fifty { get; set; }
        public string duck { get; set; }
        public string wkts { get; set; }
        public string maidenover { get; set; }
        public string er { get; set; }
        public string @catch { get; set; }
        public string runoutstumping { get; set; }
        public string runoutthrower { get; set; }
        public string runoutcatcher { get; set; }
        public string directrunout { get; set; }
        public string stumping { get; set; }
        public string thirty { get; set; }
        public string bonus { get; set; }
    }

    public class Substitute
    {
        public string pid { get; set; }
        public string name { get; set; }
        public string role { get; set; }
        public string rating { get; set; }
        public string point { get; set; }
        public string starting11 { get; set; }
        public string run { get; set; }
        public string four { get; set; }
        public string six { get; set; }
        public string sr { get; set; }
        public string fifty { get; set; }
        public string duck { get; set; }
        public string wkts { get; set; }
        public string maidenover { get; set; }
        public string er { get; set; }
        public string @catch { get; set; }
        public string runoutstumping { get; set; }
        public string runoutthrower { get; set; }
        public string runoutcatcher { get; set; }
        public string directrunout { get; set; }
        public string stumping { get; set; }
        public string thirty { get; set; }
        public string bonus { get; set; }
    }

    public class Teamb1
    {
        public List<Playing11> playing11 { get; set; }
        public List<Substitute> substitute { get; set; }
    }

    public class APIPlayerPointsResponse : MongoDbGenericRepository.Models.Document
    {
        public long? MatchId { get; set; }
        public long PlayerId { get; set; }
        public decimal? Points { get; set; }
    }

    public class ArchiveData
    {
        public int? status_id { get; set; }
        public string status_str { get; set; }
        public long match_id { get; set; }
        public string date_start { get; set; }
    }

    public class LiveMatchInfo
    {
        public Int64 _id { get; set; }
        public Int64 dbId { get; set; }
        public string cid { get; set; }
        public string match_id { get; set; }
        public string title { get; set; }
        public Int32 formatId { get; set; }
        public string format_str { get; set; }
        public Int32 status { get; set; }
        public string status_str { get; set; }
        public string status_note { get; set; }
        public string TeamA_Id { get; set; }
        public string TeamA { get; set; }
        public string TeamA_Logo { get; set; }
        public string TeamB_Id { get; set; }
        public string TeamB { get; set; }
        public string TeamB_Logo { get; set; }
        public string ContestName { get; set; }
        public string TeamA_score { get; set; }
        public string TeamA_fullscore { get; set; }
        public string TeamB_score { get; set; }
        public string TeamB_fullscore { get; set; }
        public string TeamA_overs { get; set; }
        public string TeamB_overs { get; set; }
        public string GameTypeId { get; set; }
        public DateTime MatchStartTime { get; set; }
        public DateTime MatchEndTime { get; set; }
        public string seriesname { get; set; }

        public bool IsVerified { get; set; }

    }
}
