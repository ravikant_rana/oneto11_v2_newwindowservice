﻿using MongoDB.Driver;
using Newtonsoft.Json;
using OneTo11_V2_LiveUpdates.Helpers;
using OneTo11_V2_LiveUpdates.Models;
using OneTo11_V2_Redis;
using OneTo11_V2_Redis.Helpers;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_LiveUpdates
{
    public class UpdateLiveMatchTeamPlayers : DefaultBasicConsumer
    {
        private readonly IModel _channel;
        //private LiveGamesDBAccess liveGamesDBAccess;
        //string connection = "mongodb://oneto11admin:Intigate123@104.211.183.244:27017/oneto11live?authSource=admin";
        string connection = ConfigurationManager.AppSettings["LiveMongoConnection"]; //"mongodb+srv://oneto11rootuser:Wve8BYfHtexpmOSG@cluster1.t98az.mongodb.net/test";
        MongoClient dbClient;
        public UpdateLiveMatchTeamPlayers(IModel channel)
        {
            _channel = channel;
            dbClient = new MongoClient(connection);

        }

        public override void HandleBasicDeliver(string consumerTag, ulong deliveryTag, bool redelivered, string exchange, string routingKey, IBasicProperties properties, byte[] body)
        {
            //Library.WriteErrorLog("Live updates receiver started at: " + DateTime.Now);

            JsonLiveUpdates jsonLiveUpdate = new JsonLiveUpdates();

            string jsonData = Encoding.UTF8.GetString(body);
            jsonLiveUpdate = JsonConvert.DeserializeObject<JsonLiveUpdates>(jsonData);

            GetAllLiveMatches(jsonLiveUpdate.api_match_id, jsonLiveUpdate.match_id);
            GetAllContest(jsonLiveUpdate.match_id);

            _channel.BasicAck(deliveryTag, false);
        }

        public bool GetAllLiveMatches(string mchk, long mid)
        {
            try
            {
                using (var cache = new CacheService())
                {
                    string livematchinfo = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchInfo"];
                    string LiveMatchPlayerInfo = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(mid);

                    var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);

                    List<Teamplayer> teamPlayer = new List<Teamplayer>();
                    List<Teamplayer> teamPlayerData = new List<Teamplayer>();

                    var getlivecollection = database.GetCollection<LiveMatchResponse>("APIMatchAndTeamPlayer");
                    var getlivefilter = Builders<LiveMatchResponse>.Filter.Eq(x => x.dbmchid, mid);
                    var getliveupdate = getlivecollection.Find(getlivefilter).ToList();

                    if (getliveupdate.Count > 0)
                    {
                        LiveMatchResponse liveMatchResponse = getliveupdate.FirstOrDefault();
                        var teamPlayers = liveMatchResponse.response.points;
                        if (teamPlayers.teama != null && teamPlayers.teamb != null)
                        {
                            if (teamPlayers.teama.playing11 != null)
                            {
                                foreach (var team in teamPlayers.teama.playing11)
                                {
                                    var player = new Teamplayer
                                    {
                                        _id = (!string.IsNullOrEmpty(team.pid)) ? Convert.ToInt64(team.pid) : 0,
                                        MatchTeamPlayerId = (!string.IsNullOrEmpty(team.pid)) ? Convert.ToInt64(team.pid) : 0,
                                        Name = team.name,
                                        Playing_Role = team.role,
                                        FantacyPoints = (!string.IsNullOrEmpty(team.point)) ? Convert.ToDecimal(team.point) : 0,
                                        Rating = team.rating,
                                        starting11 = team.starting11,
                                        run = team.run,
                                        four = team.four,
                                        six = team.six,
                                        sr = team.sr,
                                        fifty = team.fifty,
                                        duck = team.duck,
                                        wkts = team.wkts,
                                        maidenover = team.maidenover,
                                        er = team.er,
                                        _catch = team.@catch,
                                        runoutstumping = team.runoutstumping,
                                        runoutthrower = team.runoutthrower,
                                        runoutcatcher = team.runoutcatcher,
                                        directrunout = team.directrunout,
                                        stumping = team.stumping,
                                        thirty = team.thirty,
                                        bonus = team.bonus,
                                        IsAnnounced = true,
                                        Credit = (!string.IsNullOrEmpty(team.rating)) ? Convert.ToDecimal(team.rating) : 0
                                    };

                                    teamPlayer.Add(player);
                                }
                            }

                            if (teamPlayers.teama.substitute != null)
                            {
                                foreach (var team in teamPlayers.teama.substitute)
                                {
                                    var player = new Teamplayer
                                    {
                                        _id = (!string.IsNullOrEmpty(team.pid)) ? Convert.ToInt64(team.pid) : 0,
                                        MatchTeamPlayerId = (!string.IsNullOrEmpty(team.pid)) ? Convert.ToInt64(team.pid) : 0,
                                        Name = team.name,
                                        Playing_Role = team.role,
                                        FantacyPoints = (!string.IsNullOrEmpty(team.point)) ? Convert.ToDecimal(team.point) : 0,
                                        Rating = team.rating,
                                        starting11 = team.starting11,
                                        run = team.run,
                                        four = team.four,
                                        six = team.six,
                                        sr = team.sr,
                                        fifty = team.fifty,
                                        duck = team.duck,
                                        wkts = team.wkts,
                                        maidenover = team.maidenover,
                                        er = team.er,
                                        _catch = team.@catch,
                                        runoutstumping = team.runoutstumping,
                                        runoutthrower = team.runoutthrower,
                                        runoutcatcher = team.runoutcatcher,
                                        directrunout = team.directrunout,
                                        stumping = team.stumping,
                                        thirty = team.thirty,
                                        bonus = team.bonus,
                                        IsAnnounced = false,
                                        Credit = (!string.IsNullOrEmpty(team.rating)) ? Convert.ToDecimal(team.rating) : 0
                                    };

                                    teamPlayer.Add(player);
                                }
                            }

                            if (teamPlayers.teamb.playing11 != null)
                            {
                                foreach (var team in teamPlayers.teamb.playing11)
                                {
                                    var player = new Teamplayer
                                    {
                                        _id = (!string.IsNullOrEmpty(team.pid)) ? Convert.ToInt64(team.pid) : 0,
                                        MatchTeamPlayerId = (!string.IsNullOrEmpty(team.pid)) ? Convert.ToInt64(team.pid) : 0,
                                        Name = team.name,
                                        Playing_Role = team.role,
                                        FantacyPoints = (!string.IsNullOrEmpty(team.point)) ? Convert.ToDecimal(team.point) : 0,
                                        Rating = team.rating,
                                        starting11 = team.starting11,
                                        run = team.run,
                                        four = team.four,
                                        six = team.six,
                                        sr = team.sr,
                                        fifty = team.fifty,
                                        duck = team.duck,
                                        wkts = team.wkts,
                                        maidenover = team.maidenover,
                                        er = team.er,
                                        _catch = team.@catch,
                                        runoutstumping = team.runoutstumping,
                                        runoutthrower = team.runoutthrower,
                                        runoutcatcher = team.runoutcatcher,
                                        directrunout = team.directrunout,
                                        stumping = team.stumping,
                                        thirty = team.thirty,
                                        bonus = team.bonus,
                                        IsAnnounced = true,
                                        Credit = (!string.IsNullOrEmpty(team.rating)) ? Convert.ToDecimal(team.rating) : 0
                                    };

                                    teamPlayer.Add(player);
                                }
                            }

                            if (teamPlayers.teamb.substitute != null)
                            {
                                foreach (var team in teamPlayers.teamb.substitute)
                                {
                                    var player = new Teamplayer
                                    {
                                        _id = (!string.IsNullOrEmpty(team.pid)) ? Convert.ToInt64(team.pid) : 0,
                                        MatchTeamPlayerId = (!string.IsNullOrEmpty(team.pid)) ? Convert.ToInt64(team.pid) : 0,
                                        Name = team.name,
                                        Playing_Role = team.role,
                                        FantacyPoints = (!string.IsNullOrEmpty(team.point)) ? Convert.ToDecimal(team.point) : 0,
                                        Rating = team.rating,
                                        starting11 = team.starting11,
                                        run = team.run,
                                        four = team.four,
                                        six = team.six,
                                        sr = team.sr,
                                        fifty = team.fifty,
                                        duck = team.duck,
                                        wkts = team.wkts,
                                        maidenover = team.maidenover,
                                        er = team.er,
                                        _catch = team.@catch,
                                        runoutstumping = team.runoutstumping,
                                        runoutthrower = team.runoutthrower,
                                        runoutcatcher = team.runoutcatcher,
                                        directrunout = team.directrunout,
                                        stumping = team.stumping,
                                        thirty = team.thirty,
                                        bonus = team.bonus,
                                        IsAnnounced = false,
                                        Credit = (!string.IsNullOrEmpty(team.rating)) ? Convert.ToDecimal(team.rating) : 0
                                    };

                                    teamPlayer.Add(player);
                                }
                            }

                            if (teamPlayer.Count > 0)
                            {
                                //var teamplayerdata = GetTeamplayersFromCacheOrMongo(mid);
                                var teamplayerdata = IntigateCache.RetrieveCacheByCacheName<Teamplayer>(LiveMatchPlayerInfo, cache);
                                if (teamplayerdata.Count > 0)
                                {

                                    //var tpObj = from tpd in teamplayerdata
                                    //            join tp in teamPlayer on tpd.MatchTeamPlayerId equals tp.MatchTeamPlayerId
                                    //            select new Teamplayer
                                    //            {
                                    //                _id = tpd._id,
                                    //                MatchTeamPlayerId = tpd.MatchTeamPlayerId,
                                    //                MatchId = tpd.MatchId,
                                    //                TeamId = tpd.TeamId,
                                    //                TeamName = tpd.TeamName,
                                    //                Team_Abbr = tpd.Team_Abbr,
                                    //                Name = tp.Name,
                                    //                Playing_Role = tpd.Playing_Role,
                                    //                SelectedBy = tpd.SelectedBy,
                                    //                CaptainBy = tpd.CaptainBy,
                                    //                ViceCaptainBy = tpd.ViceCaptainBy,
                                    //                PlayerImage = tpd.PlayerImage,
                                    //                Rating = tp.Rating,
                                    //                IsAnnounced = tp.IsAnnounced,
                                    //                FantacyPoints = tp.FantacyPoints,
                                    //                starting11 = tp.starting11,
                                    //                run = tp.run,
                                    //                four = tp.four,
                                    //                six = tp.six,
                                    //                sr = tp.sr,
                                    //                fifty = tp.fifty,
                                    //                duck = tp.duck,
                                    //                wkts = tp.wkts,
                                    //                maidenover = tp.maidenover,
                                    //                er = tp.er,
                                    //                _catch = tp._catch,
                                    //                runoutstumping = tp.runoutstumping,
                                    //                runoutthrower = tp.runoutthrower,
                                    //                runoutcatcher = tp.runoutcatcher,
                                    //                directrunout = tp.directrunout,
                                    //                stumping = tp.stumping,
                                    //                thirty = tp.thirty,
                                    //                bonus = tp.bonus,
                                    //                Credit = tpd.Credit

                                    //            };

                                    var tpObj = from tpd in teamplayerdata
                                                join tp in teamPlayer on tpd.MatchTeamPlayerId equals tp.MatchTeamPlayerId into joinedT
                                                from tp in joinedT.DefaultIfEmpty()
                                                select new Teamplayer
                                                {
                                                    _id = tpd._id,
                                                    MatchTeamPlayerId = tpd.MatchTeamPlayerId,
                                                    MatchId = tpd.MatchId,
                                                    TeamId = tpd.TeamId,
                                                    TeamName = tpd.TeamName,
                                                    Team_Abbr = tpd.Team_Abbr,
                                                    Name = tpd.Name,
                                                    Playing_Role = tpd.Playing_Role,
                                                    SelectedBy = tpd.SelectedBy,
                                                    CaptainBy = tpd.CaptainBy,
                                                    ViceCaptainBy = tpd.ViceCaptainBy,
                                                    PlayerImage = tpd.PlayerImage,
                                                    Rating = tp != null ? tp.Rating : tpd.Rating,
                                                    IsAnnounced = tp != null ? tp.IsAnnounced : tpd.IsAnnounced,
                                                    FantacyPoints = tp != null ? tp.FantacyPoints : tpd.FantacyPoints,
                                                    starting11 = tp != null ? tp.starting11 : tpd.starting11,
                                                    run = tp != null ? tp.run : tpd.run,
                                                    four = tp != null ? tp.four : tpd.four,
                                                    six = tp != null ? tp.six : tpd.six,
                                                    sr = tp != null ? tp.sr : tpd.sr,
                                                    fifty = tp != null ? tp.fifty : tpd.fifty,
                                                    duck = tp != null ? tp.duck : tpd.duck,
                                                    wkts = tp != null ? tp.wkts : tpd.wkts,
                                                    maidenover = tp != null ? tp.maidenover : tpd.maidenover,
                                                    er = tp != null ? tp.er : tpd.er,
                                                    _catch = tp != null ? tp._catch : tpd._catch,
                                                    runoutstumping = tp != null ? tp.runoutstumping : tpd.runoutstumping,
                                                    runoutthrower = tp != null ? tp.runoutthrower : tpd.runoutthrower,
                                                    runoutcatcher = tp != null ? tp.runoutcatcher : tpd.runoutcatcher,
                                                    directrunout = tp != null ? tp.directrunout : tpd.directrunout,
                                                    stumping = tp != null ? tp.stumping : tpd.stumping,
                                                    thirty = tp != null ? tp.thirty : tpd.thirty,
                                                    bonus = tp != null ? tp.bonus : tpd.bonus,
                                                    Credit = tpd.Credit

                                                };


                                    if (tpObj.Count() > 0)
                                    {
                                        //IntigateCache.DeleteCache(LiveMatchPlayerInfo);
                                        IntigateCache.SaveToCache<Teamplayer>(cache, LiveMatchPlayerInfo, tpObj.ToList());

                                        var matchTeamPlayerCollection = database.GetCollection<Teamplayer>("MatchTeamPlayerInfo");
                                        var getmatchTeamPlayerfilter = Builders<Teamplayer>.Filter.Eq(x => x.MatchId, mid);
                                        var getmatchTeamPlayerupdate = matchTeamPlayerCollection.Find(getmatchTeamPlayerfilter).ToList();

                                        if (getmatchTeamPlayerupdate.Count > 0)
                                        {
                                            matchTeamPlayerCollection.DeleteMany(getmatchTeamPlayerfilter);
                                            matchTeamPlayerCollection.InsertMany(tpObj);

                                            //foreach (var mtp in tpObj)
                                            //{
                                            //    var matchTeamPlayerInfo = Builders<Teamplayer>.Update
                                            //        .Set(p => p.IsAnnounced, mtp.IsAnnounced)
                                            //        .Set(p => p.FantacyPoints, mtp.FantacyPoints)
                                            //        .Set(p => p.starting11, mtp.starting11)
                                            //        .Set(p => p.run, mtp.run)
                                            //        .Set(p => p.four, mtp.four)
                                            //        .Set(p => p.six, mtp.six)
                                            //        .Set(p => p.sr, mtp.sr)
                                            //        .Set(p => p.fifty, mtp.fifty)
                                            //        .Set(p => p.duck, mtp.duck)
                                            //        .Set(p => p.wkts, mtp.wkts)
                                            //        .Set(p => p.maidenover, mtp.maidenover)
                                            //        .Set(p => p.er, mtp.er)
                                            //        .Set(p => p._catch, mtp._catch)
                                            //        .Set(p => p.runoutstumping, mtp.runoutstumping)
                                            //        .Set(p => p.runoutthrower, mtp.runoutthrower)
                                            //        .Set(p => p.runoutcatcher, mtp.runoutcatcher)
                                            //        .Set(p => p.directrunout, mtp.directrunout)
                                            //        .Set(p => p.stumping, mtp.stumping)
                                            //        .Set(p => p.thirty, mtp.thirty)
                                            //        .Set(p => p.bonus, mtp.bonus)
                                            //        .Set(p => p.Credit, mtp.Credit);

                                            //    matchTeamPlayerCollection.UpdateOne(getmatchTeamPlayerfilter, matchTeamPlayerInfo);
                                            //}
                                        }
                                    }
                                }
                            }

                            var matchinfo = IntigateCache.RetrieveCacheByCacheName<LiveMatchInfo>(livematchinfo, cache);
                            var matchinfobymatchid = matchinfo.Where(x => x.dbId == mid).ToList();
                            if (matchinfobymatchid.Count > 0)
                            {
                                matchinfo = matchinfo.Where(x => x.dbId != mid).ToList();
                                LiveMatchInfo liveMatchInfo = new LiveMatchInfo();
                                liveMatchInfo = matchinfobymatchid.FirstOrDefault();
                                if (liveMatchResponse.response.status == 2 && liveMatchResponse.response.verified == "true")
                                {
                                    liveMatchInfo.status = 5;
                                    liveMatchInfo.status_str = "Result pending";
                                    //liveMatchInfo.IsVerified = false;
                                }
                                liveMatchInfo.status_note = liveMatchResponse.response.status_note;
                                liveMatchInfo.TeamA_fullscore = liveMatchResponse.response.teama.scores_full != null ? liveMatchResponse.response.teama.scores_full : "";
                                liveMatchInfo.TeamB_fullscore = liveMatchResponse.response.teamb.scores_full != null ? liveMatchResponse.response.teamb.scores_full : "";
                                liveMatchInfo.TeamB_score = liveMatchResponse.response.teamb.scores != null ? liveMatchResponse.response.teamb.scores : "";
                                liveMatchInfo.TeamA_score = liveMatchResponse.response.teama.scores != null ? liveMatchResponse.response.teama.scores : "";
                                liveMatchInfo.TeamB_overs = liveMatchResponse.response.teamb.overs != null ? liveMatchResponse.response.teamb.overs : "";
                                liveMatchInfo.TeamA_overs = liveMatchResponse.response.teama.overs != null ? liveMatchResponse.response.teama.overs : "";
                                matchinfo.Add(liveMatchInfo);

                                IntigateCache.SaveToCache<LiveMatchInfo>(cache, livematchinfo, matchinfo);
                            }

                            var mongoMatchCollection = database.GetCollection<LiveMatchInfo>("Matches");
                            var matchFilter = Builders<LiveMatchInfo>.Filter.Eq(x => x.dbId, mid);
                            var getLiveMatchList = mongoMatchCollection.Find(matchFilter).ToList();
                            if (getLiveMatchList.Count > 0)
                            {
                                LiveMatchInfo liveMatchMongoInfo = new LiveMatchInfo();
                                liveMatchMongoInfo = getLiveMatchList.FirstOrDefault();
                                var matchUpdateFilter = Builders<LiveMatchInfo>.Update
                                        .Set(p => p.status, (liveMatchResponse.response.status == 2 && liveMatchResponse.response.verified == "true") ? 5 : 3)
                                        .Set(p => p.status_str, (liveMatchResponse.response.status == 2 && liveMatchResponse.response.verified == "true") ? "Result pending" : "Live")
                                        .Set(p => p.IsVerified, (liveMatchResponse.response.status == 2 && liveMatchResponse.response.verified == "true") ? true : false)
                                        .Set(p => p.status_note, liveMatchResponse.response.status_note)
                                        .Set(p => p.TeamA_fullscore, liveMatchResponse.response.teama.scores_full != null ? liveMatchResponse.response.teama.scores_full : "")
                                        .Set(p => p.TeamB_fullscore, liveMatchResponse.response.teamb.scores_full != null ? liveMatchResponse.response.teamb.scores_full : "")
                                        .Set(p => p.TeamB_score, liveMatchResponse.response.teamb.scores != null ? liveMatchResponse.response.teamb.scores : "")
                                        .Set(p => p.TeamA_score, liveMatchResponse.response.teama.scores != null ? liveMatchResponse.response.teama.scores : "")
                                        .Set(p => p.TeamB_overs, liveMatchResponse.response.teamb.overs != null ? liveMatchResponse.response.teamb.overs : "")
                                        .Set(p => p.TeamA_overs, liveMatchResponse.response.teama.overs != null ? liveMatchResponse.response.teama.overs : "");

                                mongoMatchCollection.UpdateOne(matchFilter, matchUpdateFilter);

                                if (liveMatchMongoInfo.IsVerified == true || liveMatchResponse.response.status == 4)
                                {
                                    var archiveData = new ArchiveData
                                    {
                                        status_id = (liveMatchResponse.response.status == 2) ? 5 : liveMatchResponse.response.status,
                                        status_str = (liveMatchResponse.response.status == 2) ? "Result pending" : liveMatchResponse.response.status_str,
                                        match_id = mid,
                                        date_start = liveMatchResponse.response.date_start
                                    };

                                    string message = JsonConvert.SerializeObject(archiveData);
                                    MessagingQueue.GetConnectionFactory("oneto11-MoveToArchiveSpark", "oneto11-queue-MoveToArchiveSpark", "oneto11-MoveToArchiveSpark.#", message);
                                }

                            }

                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
                return false;
            }

        }

        public void GetAllContest(long mid)
        {
            var cache = new CacheService();

            var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
            var liveContestCollection = database.GetCollection<MatchContest>("Contest");

            var filter = Builders<MatchContest>.Filter.Eq("MatchId", mid);
            //var _contestList = IntigateCache.RetrieveCacheByCacheName<MatchContest>(livematchcontest, cache);

            var _contestList = liveContestCollection.Find(filter).ToList();

            if (_contestList.Count > 0)
            {
                foreach (var item in _contestList)
                {
                    string livematchcontestCache = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchLeaderBoard"] + Convert.ToString(mid) + ":" + Convert.ToString(item.dbId);
                    //UpdateLiveMatchLeaderBoardInCache(livematchcontestCache, item.dbId);
                    IntigateCache.DeleteCache(livematchcontestCache);
                }
            }
        }

        public List<Teamplayer> GetTeamplayersFromCacheOrMongo(long mid)
        {
            List<Teamplayer> teamplayersList = new List<Teamplayer>();

            try
            {
                var cache = new CacheService();
                string LiveMatchPlayerInfo = ConfigurationManager.AppSettings["CacheStartingAbbr"] + ConfigurationManager.AppSettings["redis:LiveMatchPlayerInfo"] + Convert.ToString(mid);

                teamplayersList = IntigateCache.RetrieveCacheByCacheName<Teamplayer>(LiveMatchPlayerInfo, cache);

                if (teamplayersList.Count == 0)
                {
                    var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                    var matchTeamPlayerCollection = database.GetCollection<Teamplayer>("MatchTeamPlayerInfo");
                    var getmatchTeamPlayerfilter = Builders<Teamplayer>.Filter.Eq(x => x.MatchId, mid);
                    teamplayersList = matchTeamPlayerCollection.Find(getmatchTeamPlayerfilter).ToList();
                }
            }
            catch(Exception ex)
            {
                Library.WriteErrorLog(ex);
            }

            return teamplayersList;
        }


        public void GetContest(long mid)
        {
            //var cache = new CacheService();

            var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
            var liveContestCollection = database.GetCollection<MatchContest>("Contest");


            var filter = Builders<MatchContest>.Filter.Eq("MatchId", mid);
            //var _contestList = IntigateCache.RetrieveCacheByCacheName<MatchContest>(livematchcontest, cache);

            var _contestList = liveContestCollection.Find(filter).ToList();

            if (_contestList.Count > 0)
            {
                foreach (var item in _contestList)
                {
                    string livematchcontestCache = "test_" + ConfigurationManager.AppSettings["redis:LiveMatchLeaderBoard"] + Convert.ToString(mid) + ":" + Convert.ToString(item.dbId);
                    //IntigateCache.DeleteCache(livematchcontestCache);
                    UpdateLiveMatchLeaderBoardInCache(livematchcontestCache, item.dbId);
                }
            }
        }

        public void UpdateLiveMatchLeaderBoardInCache(string cacheName, Int64 ContestId)
        {
            using(var cache = new CacheService())
            {
                var database = dbClient.GetDatabase(ConfigurationManager.AppSettings["mongoLiveDatabase"]);
                var liveContestUserTeamCollection = database.GetCollection<MatchContestUserTeamsMongoLive>("ContestUserTeam");
                var _filter = Builders<MatchContestUserTeamsMongoLive>.Filter.Eq("MatchContestId", ContestId);
                var _contestUserTeamList = liveContestUserTeamCollection.Find(_filter).ToList();
                if(_contestUserTeamList.Count > 0)
                {
                    List<MatchContestUserTeamsMongoLiveCache> mcutmlList = new List<MatchContestUserTeamsMongoLiveCache>();                    
                    foreach (var item in _contestUserTeamList)
                    {
                        MatchContestUserTeamsMongoLiveCache obj = new MatchContestUserTeamsMongoLiveCache
                        {
                            tn = item.TeamAbbr,
                            pts = item.FantacyPoints ?? 0,
                            //un = item.TeamAbbr,
                            tk = item.UserTeamId,
                            rank = item.rank,
                            //image = "https://OneTo11storageaccount.blob.core.windows.net/profile/"+Convert.ToString(item.UserId)+".png",
                            uid = item.UserId,
                            ai = 1,
                            iwj = item.isInWinning_Jone
                        };
                        mcutmlList.Add(obj);
                    }

                    if(mcutmlList.Count > 0)
                    {
                        IntigateCache.SaveToCache(cache, cacheName, mcutmlList);
                    }
                }

            }
        }

        public void Test(JsonLiveUpdates jsonLiveUpdate)
        {
            GetAllLiveMatches(jsonLiveUpdate.api_match_id, jsonLiveUpdate.match_id);
            GetAllContest(jsonLiveUpdate.match_id);
        }
    }
}
