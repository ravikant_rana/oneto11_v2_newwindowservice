﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneTo11_V2_LiveUpdates.Helpers
{
    public class MessagingQueue
    {
        public static void GetConnectionFactory(string exchangeName, string queueName, string routingKey, string message)
        {
            try
            {
                ConnectionFactory factory = new ConnectionFactory
                {

                    HostName = ConfigurationManager.AppSettings["MQHostName"],
                    UserName = ConfigurationManager.AppSettings["MQUserName"],
                    Password = ConfigurationManager.AppSettings["MQPassword"],
                    Port = Convert.ToInt32(ConfigurationManager.AppSettings["MQPort"]),
                    VirtualHost = ConfigurationManager.AppSettings["MQVirtualHost"]
                };

                using (IConnection connection = factory.CreateConnection())
                {
                    using (IModel channel = connection.CreateModel())
                    {
                        channel.ExchangeDeclare(exchangeName, ExchangeType.Topic, true);
                        channel.QueueDeclare(queue: queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                        var body = Encoding.UTF8.GetBytes(message);
                        channel.QueueBind(queueName, exchangeName, routingKey);

                        channel.BasicPublish(exchange: exchangeName,
                                             routingKey: routingKey,
                                             basicProperties: null,
                                             body: body);
                        Console.WriteLine("Message Sent Successfully - {0}", message);

                    }
                }
            }
            catch (Exception ex)
            {
                Library.WriteErrorLog(ex);
            }
        }
    }
}
